@extends('layouts.main')
@section('content')
    <!-- Bottom Strip -->
            <div class="bottom-strip-wrapper">
                <div class="bottom-strip">
                    <p>
                    	<span>Sunati la +40 748 29 29 00  sau cereti o oferta </span><a href="{{URL::to('/contact')}}">Cerere Oferta</a>
                    </p>
                </div>
            </div>
    <!-- Start Container -->
            <div id="container">

                <!-- Content -->
                <div class="content event-listing clearfix">

                    <div class="container-top"></div>

                    <section class="page-head">
                        <h1>
                            <span>Evenimente</span>
                        </h1>
                    </section>

                    <!-- Main -->
                    <div id="main-content">
                        
                        @foreach($events as $event)
                        
                        <article class="event type-event status-publish hentry clearfix">
                            <center>
                            <div class="post-content">

                                <h3 class="post-title">
                                    @if($event->menu_id > 0)
                                        <a href="{{URL::to('menu/'.$event->menu_id)}}">{{$event->title}}</a>
                                    @else
                                        <a href="#">{{$event->title}}</a>
                                    @endif
                                </h3>
				 <div style="position:relative;width:100%" class="post-thumb clearfix event-thumb">
                                    @if($event->image)
                                    <a href="{{asset('img/events/small/'.$event->image)}}" title="{{$event->title}}" class="pretty-photo">
                                        <img src="{{asset('img/events/small/'.$event->image)}}" alt="{{$event->title}}">
                                    </a>
                                    @endif
                                </div>
				{{$event->content}}
                              
                               
                                <!-- end of post thumb -->
                                @if($event->menu_id > 0)
                                    <div style="position:relative;width:100%"><p><a href="{{URL::to('menu/'.$event->menu_id)}}">Vezi Oferta<a></p></div>
                                @else
                                    <div style="position:relative;width:100%"><p><a href="{{URL::to('/contact')}}">Solicita Oferta<a></p></div>                
                                @endif
                                
                            </div>
                            </center>
                        </article>
                        
                        @endforeach
                        <?php echo $events->links(); ?>
                        
                    </div><!-- End Main -->

                    <div class="container-bottom"></div>

                </div> <!-- End Content-->

            </div><!-- End Container -->
@stop

