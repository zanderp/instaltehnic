@extends('layouts.main')
@section('content')
<!-- Bottom Strip -->
            <div class="bottom-strip-wrapper">
                <div class="bottom-strip">
                    <p>
                    	<span>Sunati la +40 748 29 29 00  sau cereti o oferta </span><a href="{{URL::to('/contact')}}">Cerere Oferta</a>
                    </p>
                </div>
            </div>

            <!-- Start Container -->
            <div id="container">

                <!-- Content -->
                <div class="content our-menu clearfix">

                    <div class="container-top"></div>

                    <section class="page-head">
                        <h1>
                            <span>Lista Oferte</span>
                        </h1>
                    </section>

                    <!-- Main -->
                    <div id="main-content">
                        <center>
                        @foreach($offers as $offer)
                            <article class="clearfix">
                                <figure>
                                    <a href="#"> <img src="{{asset('img/offers/small/'.$offer->image)}}" alt="{{$offer->title}}"></a>
                                </figure>
                                
                                <div class="post-content">
                                    <h3 class="post-title"><a href="{{URL::to('offer/'.$offer->id)}}">{{$offer->title}}</a></h3>
                                    <p><a href="{{URL::to('offer/'.$offer->id)}}">Vezi Oferta</a></p>
                                    
                                </div>
                               
                            </article>
                        @endforeach
			{{$page->content}}
                        </center>
                    </div>
                    <div class="container-bottom"></div>

                </div> <!-- End Content-->

            </div><!-- End Container -->
@stop
