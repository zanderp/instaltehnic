@extends('layouts.main')
@section('content')


      <!--LEFT MENU AND SLIDER AREA START-->
    <section style="margin-top:0px" class="menu-and-slider padding-for-menu-slider">
        <div class="container">


            <div class="row">
                <!--Category menu area start -->
                <div class="col-md-4">
                    <div class="left-category-menu hidden-sm hidden-xs">
                        <div class="left-product-cat">
                            <div class="category-heading">
                                <h2>Toate Categoriile</h2>
                            </div>
                            <!-- CATEGORY-MENU-LIST START -->
                            <div class="category-menu-list">
          										<ul>
                                @foreach($categories as $category)
                                  <!--SINGLE MENU START-->
                                  <li class="arrow-plus">
                                    <a  href="{{URL::to('/produse/cat/'.$category->id)}}">{{$category->name}}</a>
                                    <!-- MEGA MENU START -->
									
                                    <div class="cat-left-drop-menu">
                                      @foreach($subcategories as $key => $subcategory)

                                        @if($subcategory->category_id == $category->id)
											@if(isset($subcategory->name))
                                            <div class="cat-left-drop-menu-left">
                                              <a href="{{URL::to('/produse/subcat/'.$subcategory->id)}}" class="menu-item-heading special_link">{{$subcategory->name}}</a>
                                            </div>
											@endif

                                        @endif
                                      @endforeach
									</div>
                                    <!--  MEGA MENU END -->
                                  </li>
                                  <!--SINGLE MENU END-->
                                @endforeach

          										</ul>
          									</div>
                        <!-- CATEGORY-MENU-LIST END -->
                        </div>
                    </div>
                </div>
                <!--CATEGORY MENU AREA END -->
            <!--Category menu area end -->
            <!--  REVOLUTION SLIDER START -->
                <div class="col-md-8">
					<h3>Preturile de pe acest catalog au caracter informativ. Pentru mai multe detalii cereti oferta pentru produsul care va intereseaza sau contactati unul din reprezentantii nostrii de vanzari!</h3>
                    <!-- slider -->
                    <ul class="bxslider">
                      @foreach($slides as $slide)
             		  	    <li><img style="cursor:pointer" onclick="window.open('{{asset('uploads/files/'.$slide->pdf)}}', '_blank')" src="{{asset('img/slides/'.$slide->image)}}" class="img img-responsive" title="{{$slide->name}}" /></li>
             		     @endforeach
				</ul>
                    <!-- slider end-->
                </div>
                <!--  REVOLUTION SLIDER END -->
            </div>
        </div>
    </section>
    <!--LEFT MENU AND SLIDER AREA END-->


	<!--BRANDS AREA START-->
    <section style="margin-top: 23px" class="brands-area">
         <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-area">

                      <h2>Producatori</h2>
                    </div>
                </div>
            </div>
             <div class="row">
                 <div class="col-md-12">
                    <div class="brands-contain">
						@foreach($producatori as $p)
							@if(!empty($p->image))
							<span><a href="#"><img src="{{asset('img/logos/original/'.$p->image)}}" alt="{{$p->title}}" style="width:200px"></a></span>
							@endif
						@endforeach
                     </div>
                 </div>
             </div>
         </div>
     </section>
    <!--BRANDS AREA END-->
   </div>


@stop
