@extends('layouts.main')
@section('content')

<!--BLOG AREA START-->
    <section class="blog">
        <div class="container">
			<div class="row">
                <div class="col-md-12">
                    <div class="bread-crumb">
                        <ul>
                            <li class="breadcrumb-arrow"><a href="{{URL::to('/acasa')}}"><i class="fa fa-home"></i></a></li>
                            <li><a href="">Blog</a></li>
                        </ul>
                    </div>
                </div>
			</div>
			<div class="row col-md-12">
			
               
                	@foreach($posts as $post)
                	 <div class="col-md-6">
                	<center>
                    <div class="blog-video sin-blog">
			@if(!empty($post->image))
                        <div class="post-thumb">
                           <img src="{{asset('img/posts/medium/'.$post->image)}}" class="img-responsive"  alt="" />
                        </div>
			@endif
                        <div class="blog-detail">
                            <h2 class="blog-title">
                               <a href="{{URL::to('blog/posts/'.$post->id)}}">{{$post->title}}</a>
                            </h2>
                            <div class="blog-meta">
                                <span class="published"><i class="fa fa-clock-o"></i> {{date("M j",strtotime($post->created_at))}}</span>
                               
                            </div>
                            <div class="blog-content">
                           	 

						@if(strlen($post->content) > 50)

							{{\Illuminate\Support\Str::words(strip_tags($post->content), 35, '...')}}

						@endif

                           </div>
                            <a class="btn btn-default" href="{{URL::to('blog/posts/'.$post->id)}}">Citeste mai mult</a>
                        </div>
                    </div>
                     	</center>
    
           </div>
               	@endforeach
             
               	<?php echo $posts->links(); ?>
         
        </div>
    </section>
    <!--BLOG AREA END-->
    
@stop


