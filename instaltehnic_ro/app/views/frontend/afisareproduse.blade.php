@extends('layouts.main')
@section('content')

	<div style="margin-bottom:20px" class="right-top-banner-two col-md-12">
		<div class="bread-crumb">
			<ul>
				<li class="breadcrumb-arrow"><a href="{{URL::to('/acasa')}}">
				<i class="fa fa-home"></i></a></li>
				<li><a href="#">Produse</a></li>
			</ul>
		</div>
		<div class="row">
           <div class="col-md-12">
	    		@foreach($products as $product)
	    		<?php

	    		$ctype = null;
	    		$discount = 0;
	    		$normal_proce = 0;
	    		$final_price = 0;
	    		$discountp = 0;
	    		$discounted_price = 0;

	    		if($product->category_id > 0){
	    			$ctype = PCategory::find($product->category_id);
	    			if($ctype->discount > 0){
	    				$discount = $ctype->discount;
				}
			}
			if($product->subcategory_id > 0 && $discount == 0){
				$ctype = PSubcategory::find($product->subcategory_id);
				if($ctype->discount > 0){
					$discount = $ctype->discount;
				}
			}
			if($product->sscat_id > 0 && $discount == 0){
	    			$ctype = Sscat::find($product->sscat_id);
	    			if($ctype->discount > 0){
					$discount = $ctype->discount;
				}
			}


			if($discount > 0){
				$normal_price = explode("lei",$product->price);
				$final_price = tofloat(trim($normal_price[0]));

				$discountp = $discount/100 * $final_price;
				$discounted_price = $final_price - $discountp;
			}

		    ?>

		         <!--SINGLE BANNER-->
		       <div class="col-md-3">
		          <div class="new-pro-two-sin">

		              <div class="new-pro-two-img">
				         <a href="{{URL::to('produs/'.$product->id)}}">
				           <img src="{{asset('img/products/original/'.$product->image)}}" alt="">
				         </a>
		              </div>
		              <div class="new-pro-two-detail">
		                  <div class="sale-star-two">
		                      <br />
		                   </div>
		                   <p><a class="product_title_over" href="{{URL::to('produs/'.$product->id)}}">{{$product->title}}</a></p>
		                   <p>

		                   @if($discount > 0)
			              		<span class="old-price-two">{{$product->price}}</span> <span class="new-price-two">{{$discounted_price}} lei</span>
		              		@else
		              			<span class="new-price-two">{{$product->price}}</span>
		         			@endif
		                   </p>
		              </div>

		              <div class="np-add-cart">
		                <a href="{{URL::to('/contact/'.$product->id)}}"><span>Cere Oferta</span></a>
		             </div>

		          </div>
		      </div>
			@endforeach
		</div>
		</div>
		<div style="margin-bottom:20px" class="right-top-banner-two col-md-12">
			<?php
			echo $products->links();
			?>
		</div>
	</div>
@stop
