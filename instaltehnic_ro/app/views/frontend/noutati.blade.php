@extends('layouts.main')
@section('content')
	
	<div style="margin-bottom:20px;min-height:400px" class="right-top-banner-two col-md-12">
		<div class="bread-crumb">
			<ul>
				<li class="breadcrumb-arrow"><a href="{{URL::to('/acasa')}}">
				<i class="fa fa-home"></i></a></li>
				<li><a href="#">Noutati</a></li>
			</ul>
		</div>
		<div class="row">
		<!--Category menu area start -->
           <div class="col-md-4">
               <div class="left-category-menu hidden-sm hidden-xs">
                   <div class="left-product-cat">
                       <div class="category-heading">
                           <h2>Toate Categoriile</h2>
                       </div>
                       <!-- CATEGORY-MENU-LIST START -->
                       <div class="category-menu-list">
                           <ul>
                           	@foreach($categories as $category)
						<!--SINGLE MENU START-->
						<li class="arrow-plus">
							<a  href="{{URL::to('/produse/cat/'.$category->id)}}"><span class="cat-thumb"><img src="{{asset('frontend/img/cat-accessories.png')}}" alt=""></span>{{$category->name}}</a>
							<!-- MEGA MENU START -->
									<div class="cat-left-drop-menu">
									@foreach($subcategories as $subcategory)
										@if($subcategory->category_id == $category->id)
											<div class="cat-left-drop-menu-left">
												<a href="{{URL::to('/produse/subcat/'.$subcategory->id)}}" class="menu-item-heading">{{$subcategory->name}}</a>
											</div>
										@endif
									@endforeach
									</div>
							<!--  MEGA MENU END -->
						</li>
						<!--SINGLE MENU END-->
                           	@endforeach
                           
                           </ul>
                       </div>
                   <!-- CATEGORY-MENU-LIST END -->
                   </div>
               </div>
           </div>
           <!--CATEGORY MENU AREA END -->
           <div class="col-md-8">
	    		@foreach($produse_adaugate as $produs_adaugat)
		         <!--SINGLE BANNER-->
		       <div class="col-md-6">
		          <div class="new-pro-two-sin">
		             
		              <div class="new-pro-two-img">
		              <a href="{{URL::to('produs/'.$produs_adaugat->id)}}">
		                <img src="{{asset('img/products/original/'.$produs_adaugat->image)}}" alt="">
		              </a>
		              </div>
		              <div class="new-pro-two-detail">
		                  <div class="sale-star-two">
		                      <br />
		                   </div>
		                   <p><a class="product_title_over" href="{{URL::to('produs/'.$produs_adaugat->id)}}">{{$produs_adaugat->title}}</a></p>
		                   <p> 
		                   
		                   <span class="new-price-two">{{$produs_adaugat->price}}</span>
		                   </p>
		              </div>
		              <div class="np-add-link-area">
		                  <div class="single-link"> 
		                     
		                  </div>
		                  <div class="single-link">
		                  	<a href="{{URL::to('produs/'.$produs_adaugat->id)}}" data-toggle="tooltip" title="Vezi Produs" ><i class="fa fa-search"></i></a>
		                  </div>
		                  <div class="single-link">
		                   
		                  </div>
		              </div>
		              <div class="np-add-cart">
		                <a href="{{URL::to('/contact/'.$produs_adaugat->id)}}"><span>Cere Oferta</span></a>
		             </div>
		           
		          </div>
		      </div>
			@endforeach
		</div>
		</div>
		<div style="margin-bottom:20px" class="right-top-banner-two col-md-12">
			
		</div>
	</div>
@stop
