@extends('layouts.main')
@section('content')
   
            <!--End Slider-->
            <!-- Bottom Strip -->
            <div class="bottom-strip-wrapper">
                <div class="bottom-strip">
                    <p>
                    	<span>Sunati la +40 748 29 29 00  sau cereti o oferta </span><a href="{{URL::to('/contact')}}">Cerere Oferta</a>
                    </p>
                </div>
            </div>
    <!-- Start Container -->
    <div id="container">

        <!-- Content -->
        <div class="content full-width clearfix">

            <div class="container-top"></div>

                <div id="main-content">
                
                {{$page->content}}

		@if($images->count() > 0)
				<div class="mygallery">
				<div class="tn3 album">
				    <h4>Fixed Dimensions</h4>
				    <div class="tn3 description">Images with fixed dimensions</div>
				    <div class="tn3 thumb">images/35x35/1.jpg</div>
					<ol>
		                @foreach($images as $image)
		            	
						<li>
						    <h4></h4>
						    <div class="tn3 description"></div>
						    <a href="{{asset('img/slides/'.$image->name)}}">
							<img width="35" height="35" src="{{asset('img/slides/'.$image->name)}}" />
						    </a>
						</li>
	
		                @endforeach
		
				</ol>
				</div>
			    </div>
		@endif
                
                </div><!-- End of Main Content-->

            <div class="container-bottom"></div>

        </div><!-- End Content-->

    </div><!-- End Container -->
    
            
@stop
