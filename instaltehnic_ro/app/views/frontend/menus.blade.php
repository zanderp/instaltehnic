@extends('layouts.main')
@section('content')
<!-- Bottom Strip -->
            <div class="bottom-strip-wrapper">
                <div class="bottom-strip">
                    <p>
                    	<span>Sunati la +40 748 29 29 00  sau cereti o oferta </span><a href="{{URL::to('/contact')}}">Cerere Oferta</a>
                    </p>
                </div>
            </div>

            <!-- Start Container -->
            <div id="container">

                <!-- Content -->
                <div class="content our-menu clearfix">

                    <div class="container-top"></div>

                    <section class="page-head">
                        <h1>
                            <span>Lista Meniuri</span>
                        </h1>
                    </section>

                    <!-- Main -->
                    <div id="main-content">
                        <center>
			<article>
			{{$page->content}}
			</article>

                        @foreach($menus as $menu)
				@if($menu->id != 1)
		                    <article class="clearfix">
					@if($menu->image)
		                        <figure>
		                            <a href="#"> <img src="{{asset('img/menus/small/'.$menu->image)}}" alt="{{$menu->title}}"></a>
		                        </figure>
		                        @endif
		                        <div class="post-content">
		                            <h3 class="post-title"><a href="{{URL::to('menu/'.$menu->id)}}">{{$menu->title}}</a></h3>
		                            <p><a href="{{URL::to('menu/'.$menu->id)}}">Vezi Meniu<a</p>
		                            
		                        </div>
		                       
		                    </article>
				@endif
                        @endforeach
                        </center>
                    </div>
                    <div class="container-bottom"></div>

                </div> <!-- End Content-->

            </div><!-- End Container -->
@stop
