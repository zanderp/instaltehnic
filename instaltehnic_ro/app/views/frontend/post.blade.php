@extends('layouts.main')
@section('content')

 <!--BLOG AREA START-->
    <section class="blog">
        <div class="container">
           <div class="row">
                <div class="col-md-12">
                    <div class="bread-crumb">
                        <ul>
                            <li class="breadcrumb-arrow"><a href="{{URL::to('/acasa')}}"><i class="fa fa-home"></i></a></li>
                            <li><a href="">{{$post->title}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="sin-blog border-none">
                        <div class="post-thumb">
			  @if(!empty($post->image))
                            <div>
                                <img src="{{asset('img/posts/medium/'.$post->image)}}" class="img-responsive"  alt="" />
                            </div>
			  @endif
                        </div>
                        <div style="text-align:center" class="blog-detail">
                        
                            <h2 class="blog-title">
                            <a href="#">
                                {{$post->title}}
                            </a>
                            </h2>
                            <div class="blog-meta">
                                <span class="published"> 
                                    <i class="fa fa-clock-o"></i> 
                                    {{date("M j",strtotime($post->created_at))}}
                                </span>
                               
                            </div>
                            <div class="post-content">
                            		<p>{{$post->content}}</p>  
                            </div>
                      </div>         
                </div>
            </div>
        </div>
    </section>
      <!--BLOG AREA END-->
      
      

@stop


