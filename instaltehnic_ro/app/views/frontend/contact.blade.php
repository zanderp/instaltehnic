@extends('layouts.main')
@section('content')

	<!--BREADCRUMB START -->
    <div class="container">
        <div class="row">
                <div class="col-md-12">
					<div class="bread-crumb">
						 <ul>
							<li class="breadcrumb-arrow"><a href=""><i class="fa fa-home"></i></a></li>
							<li><a href="{{URL::to('/acasa')}}">Contact</a></li>
						 </ul>
					</div>
                </div>
        </div>
    </div>
    <!--BREADCRUMB END -->

    <!--CONTACT START -->
    <section class="container">
        <div class="row">
        		@if(Session::get('message'))
        			<h4>{{Session::get('message')}}</h4>
        		@endif
			<div style="font-size:16px" class="col-md-12 contact-contain " >
				<h1>Contact</h1>
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-3 col-md-3">
								<strong>{{$settings->punct_lucru}}</strong>
							</div>
							<div class="col-sm-3 col-md-3">
								<strong>Telefon</strong><br>{{$settings->mobil}}
							</div>
							<div class="col-sm-3 col-md-3">
								<strong>Fax</strong><br>{{$settings->telefon}}
							</div>
							<div class="col-sm-3 col-md-3">
								<strong>Email</strong><br>{{$settings->email}}
							</div>
						</div>
					</div>
				</div>
				{{Form::open(array('url' => 'contact/post', 'method'=>'post', 'class' => 'form-horizontal' ))}}
					<fieldset>
						<h3>Contact Form</h3>
						<div class="form-group required">
							<label  class="col-sm-2 control-label" for="input-name">Nume</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="input-name" value="" name="name" required>
							</div>
						</div>
						<div class="form-group required">
							<label for="input-email" class="col-sm-2 control-label">E-Mail</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="input-email" value="" name="email" required>
							</div>
						</div>
						<div class="form-group required">
							<label for="input-tel" class="col-sm-2 control-label">Telefon</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="input-tel" value="" name="telefon" required>
							</div>
						</div>
						<div class="form-group required">
							<label for="input-subject" class="col-sm-2 control-label">Subiect</label>
							<div class="col-sm-10">
							@if(isset($product))
								<input type="text" class="form-control" id="input-subject" value="Cerere Oferta: {{$product->title}}" name="subject" required>
							@else
								<input type="text" class="form-control" id="input-subject" value="" name="subject" required>
							@endif
							</div>
						</div>
						<div class="form-group required">
							<label for="input-enquiry" class="col-sm-2 control-label">Mesaj</label>
							<div class="col-sm-10">
								<textarea style="height:100px" class="form-control" id="input-enquiry" rows="20" name="message" required></textarea>
							</div>
						</div>
					</fieldset>
					<div class="buttons">
						<div class="pull-right">
							{{ Form::submit('Trimite Mesaj', array('class' => 'btn btn-primary solid blank')) }}
						</div>
						{{ Form::close() }}
					</div>
				</form>
				<br /><br />
			</div>
        </div>
    </section>
	<div id="map" style='width:100%; height:250px;'></div>

    <script>
      $(document).ready(function() {
      //Map Jquery
      var map = new GMaps({
          el: '#map',
          lat: {{$settings->lat}},
          lng: {{$settings->longi}},
          zoom: 13,
          zoomControl : true,
          zoomControlOpt: {
              style : 'SMALL',
              position: 'TOP_LEFT'
          },
          scrollwheel : false,
          panControl : false,
          streetViewControl : false,
          mapTypeControl: false,
          overviewMapControl: false

        });

          map.addMarker({
            lat: {{$settings->lat}},
            lng: {{$settings->longi}},
            icon: "{{asset('frontend/img/map-marker.png')}}"
          });

        var styles = [
           {
              featureType: "road",
              elementType: "geometry",
              stylers: [
                  { lightness: 100 },
                  { visibility: "simplified" }
            ]
          }, {
              featureType: "road",
              elementType: "labels",
              stylers: [
                  { visibility: "off" }
            ]
          }
        ];

        map.addStyle({
            styledMapName:"Styled Map",
            styles: styles,
            mapTypeId: "map_style"
        });

        map.setStyle("map_style");
      });
  </script>
    <!--CONTACT END -->
@stop
