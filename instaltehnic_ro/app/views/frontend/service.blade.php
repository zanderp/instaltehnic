@extends('layouts.main')
@section('content')
					
	<section class="about-us-info">		
		<div class="container">			
			<div class="row">				
				<div class="col-md-12">					
					<div class="bread-crumb">						
						<ul>
							<li class="breadcrumb-arrow"><a href="/acasa"><i class="fa fa-home"></i></a></li>
							<li><a href="">Service</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-12">					
				{{$page->content}}

				</div>
				@if(count($images))
				<div style="margin-top:20px" class="col-md-12">
					 <div id="myCarousel" class="carousel slide" data-ride="carousel">
				             <!-- Indicators -->
				             <ol class="carousel-indicators">
			             		  @foreach($images as $key=>$image)
			             		  	  @if($key == 0)
						            	<li data-target="#myCarousel" data-slide-to="{{$key}}" class="active"></li>
						            @else
						            	<li data-target="#myCarousel" data-slide-to="{{$key}}"></li>
						            @endif
					            @endforeach
				             </ol>

				               <!-- Wrapper for slides -->
				             <div class="carousel-inner" role="listbox">
				             		@foreach($images as $key=>$image)
				             			@if($key == 0)
										<div class="item active">
									  		<a href=""><img src="{{asset($image->image)}}"></a>
										</div>
									@else
										<div class="item">
											<a href=""><img src="{{asset($image->image)}}"></a>
										</div>
									@endif
								@endforeach
				             </div>
				             <!-- Left and right controls -->
				             <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				                 <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				                 <span class="sr-only">Previous</span>
				             </a>
				             <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				                 <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				                 <span class="sr-only">Next</span>
				             </a>
			        	 </div>
		        	 </div>
		        	 @endif
			</div>
  		</div>
	</section>

@stop
