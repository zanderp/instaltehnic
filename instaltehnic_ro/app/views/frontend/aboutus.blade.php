@extends('layouts.main')
@section('content')

<section class="about-us-info">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="bread-crumb">
						<ul>
							<li class="breadcrumb-arrow"><a href="/acasa"><i class="fa fa-home"></i></a></li>
							<li><a href="">Termeni si Conditii</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-12 col-md-12">
					
					<div class="col-sm-12 col-md-12">
						<div class="about-info">
							<p>Utilizarea in orice fel, vizitarea si comandarea produselor de pe site-ul www.instaltehnic.ro implica acceptarea termenilor si conditiilor detaliate in paragrafele urmatoare.
							S.C. INSTALTEHNIC UTILAJE S.R.L. isi rezerva dreptul de a modifica aceste prevederi, precum si site-ului www.instaltehnic.ro fara o notificare prealabila a utilizatorilor.
							In cazul in care facem referiri la alte site-uri, S.C. Instaltehnic Utilaje S.R.L.  nu garanteaza si nu confirma  sub nici o forma informatiile postate pe acestea. Ramane la aprecierea utilizatorului daca viziteaza sau nu aceste site-uri sau daca ia sau nu in considerare informatia gasita acolo.
							</p>
							<br />
							<h4>Comunicarea cu utilizatorii.</h4>
							<p>Prin accesarea, inregistrarea, comandarea de produse de pe site-ul www.instaltehnic.ro, utilizatorul accepta sa primeasca notificari de la S.C. INSTALTEHNIC UTILAJE S.R.L..
							Modalitatea de comunicare / notificare poate fi electronica (e-mail, mesaje postate pe site-ul www.instaltehnic.ro si vizibile din contul utilizatorului),  telefonica, prin fax sau SMS  fara a se limita insa la acestea.
							S.C. INSTALTEHNIC UTILAJE S.R.L. va considera ca prin vizitarea site-ului www.instaltehnic.ro utilizatorul a acceptat sa primeasca notificarilor de orice fel si prin orice modalitati de la aceasta.
							</p>
							<br />
							<h4>Dreptul de returnare a produselor</h4>
							<p>Produsele achizitionate la distanta se pot returna conform O.G. 130/2000. Acestea se pot returna, in ambalajul original si in aceeasi stare in care au fost receptionate, in termen de 10 zile lucratoare de la primire fara penalitati si fara invocarea vreunui motiv.
							Factura fiscala tine loc de contractul de vanzare-cumparare.
							Returnarea produselor se supune acestei legi in cazul in care acestea au fost livrate clientilor prin firma de curierat rapid. Returnarea produselor se va face pe cheltuiala clientului, folosind acelasi serviciu de transport cu care s-a facut expedierea. S.C. INSTALTEHNIC UTILAJE S.R.L. va inapoia contravaloarea comenzii in cel mult 30 de zile de la data denuntarii in scris a contractului. Suma inapoiata cat si conditiile de returnare nu includ cheltuielile de transport. Nu se accepta pentru returnare produse care prezinta modificari fizice, lovituri, ciobiri, zgarieturi, socuri, produse care prezinta urme de consum sau de uzura, etc.
							In anumite situatii livrarile pot fi facute de catre personalul S.C. INSTALTEHNIC UTILAJE S.R.L.  Verificarea si proba la primirea produsului sunt necesare in prezenta unui reprezentant al acesteia. Acest caz nu poate fi considerat comert la distanta si ca urmare clientul nu beneficiaza la dreptul de denuntare a contractului in termen de 10 zile. In cazul ridicarii produsului de la sediu se aplica aceeasi regula, clientul nu beneficiaza la dreptul de denuntare a contractului in termen de 10 zile. La ridicarea produsului este necesara verificarea acestuia. Clientul nu este obligat sa semneze primirea produsului pana la testarea corespunzatoare a acestuia.
							</p>
							<br />
							<h4>Garantia produselor</h4>
							<p>Toate produsele comercializate de noi, prin intermediul site-ului www.instaltehnic.ro, beneficiaza de garantie in conformitate cu legislatia Romana in vigoare.
							Produsele comercializate sunt noi, in ambalajele originale si provin din surse autorizate de fiecare producator in parte; acestea sunt insotite de Certificatele de Garantie ale producatorilor / importatorilor si beneficiaza de garantie in retelele de service ale acestora. Centrele de service sunt mentionate explicit pe certificatele de garantie.
							In cazul unui produsul reclamat defect in perioada de garantie trebuie prezentat direct la cel mai apropiat centru de service mentionat in certificat. Acest centru autorizat de producator va prelua intreaga responsabilitate a rezolvarii garantiei.
							Conditiile de acordare a garantiei se supun legislatiei in vigoare, insa pot sa fie diferite in functie de politica fiecarui producator/importator.
							</p>
							<br />
							<h4>IMPORTANT:</h4>
							<h5>Lipsa certificatului de garantie al produsului trebuie semnalata in maxim 48 ore de la receptia marfii pe adresa office@instaltehnic.ro. Orice sesizare ulterioara nu va fi luata in considerare.
							</h5>
							<br />
							<h4>Newsletter:</h4>
							<p>Abonare si dezabonarea la newsletterul distribuit de site-ul www.instaltehnic.ro este gratuita si voluntara, si implica acceptarea urmatorilor termeni de utilizare. Mesajele trimise nu pot fi considerate nesolicitate si va puteti dezabona oricand. Mesajele trimise respecta legea Comertului Electronic in ceea ce priveste comunicarea comerciala asa cum este ea stipulata de legislatia romaneasca si internationala.
							</p>
							<br />
							<h4>Protectia datelor cu caracter personal:</h4>
							<p>Datele cu caracter personal ale utilizatorilor vor fi folosite numai în scopul declarat al acestui site. Ne obligam sa nu facem publice sau sa nu furnizam aceste date personale.
							Va aducem la cunostinta ca in conformitate cu Legea nr. 677/2001 pentru protectia persoanelor cu privire la prelucrarea datelor cu caracter personal si libera circulatie a acestor date, modificata si completata si ale Legii nr. 506/2004 privind prelucrarea datelor cu caracter personal si protectia vietii private in sectorul comunicatiilor electronice, aveti dreptul de informare, dreptul de a accesa si/sau rectifica datele cu caracter personal ce va sunt procesate, dreptul de a va opune in orice moment la prelucrarea datelor cu caracter personal, in baza unor motive justificate si temeinice, conform prevederilor legale relevante, dreptul de a nu fi supus unei decizii individuale, dreptul de a va adresa justitiei.
							</p>
							<br />
							<h4>Recomandari:</h4>
							<p>Prin inregistrarea ca membru veti avea un nume de utilizator (adresa de e-mail) si o parola. Sunteti responsabil(a) de pastrarea in siguranta numelui de utilizator si a parolei de acces ca membru al acestui site, orice activitate desfasurata sub numele dumneavoastra de utilizator fiind integral in responsabilitatea dumneavoastra.
							Sunteti de acord sa ne instiintati imediat despre orice suspiciune de activitate neautorizata sub numele dumneavoastra de utilizator.
							</p>
							<br />
							<h4>Drepturi de autor:</h4>
							<p>Elemente din continutul sitului - elemente de grafica Web, scripturi, programe, baze de date - sunt proprietatea S.C. INSTALTEHNIC UTILAJE S.R.L. si sunt aparate de legea pentru protectia drepturilor de autor.
							Folosirea fara acordul scris alS.C. INSTALTEHNIC UTILAJE S.R.L. a oricaror elemente enumerate mai sus se pedepseste conform legilor in vigoare. 
							Declinarea responsabilitatii.
							S.C. INSTALTEHNIC UTILAJE S.R.L.  nu isi asuma responsabilitatea si nu poate fi facuta responsabila pentru pagubele aparute prin folosirea in orice fel a produselor cumparate prin intermediul site-ului www.instaltehnic.ro. Nu raspundem de problemele de compatibilitate cu produsele achizitionate de la alte firme si nu ne asumam nici o raspundere in cazul in care produsele achizitionate de la firma S.C. INSTALTEHNIC UTILAJE S.R.L. sunt folosite in conditii care nu corespund cu performantele si specificatiile tehnice ale produsului respectiv. Datorita volumului mare de produse aflate in baza de date, S.C. INSTALTEHNIC UTILAJE S.R.L.   nu garanteaza ca descrierea acestora (imagini, specificatii tehnice, pret) sau continutul site-ului www.mastertools.ro este exact, complet actualizat sau fara erori. Datorita modificarilor caracteristicilor sau a design-ului de catre producatori (fara o notificare prealabila), produsele livrate pot diferi in orice mod de imaginile afisate pe site. In cazul in care preturile sau alte detalii referitoare la produse au fost afisate gresit, inclusiv din cauza faptului ca au fost operate gresit in baza de date, S.C. INSTALTEHNIC UTILAJE S.R.L.  are dreptul sa anuleze livrarea produsul respectiv si sa anunte clientul despre eroarea aparuta, in cel mai scurt timp, daca livrarea nu a avut loc. S.C. INSTALTEHNIC UTILAJE S.R.L.  nu poate fi facuta in nici un fel responsabila pentru pagubele sau prejudiciile rezultate din neonorarea comenzilor. Valoarea maxima a obligatiilor fata de orice client in cazul nelivrarii sau livrarii necorespunzatoare a produselor se reduce strict la valoarea sumelor incasate de la clientul respectiv.</p>
							<br />
							<h4>Litigiile:</h4>
							<p>Conflictele aparute intre utilizatorii site-ului www.mastertools.ro siS.C. INSTALTEHNIC UTILAJE S.R.L..  vor fi rezolvate pe cale amiabila in termen de 15 zile lucratoare de la data sesizarii lor in scris, de catre utilizator. In cazul in care litigiul nu este solutionat pe cale amiabila, competenta revine Instantelor de Judecata Romane.</p>
						</div>
					</div>
				</div>
				
			</div>  
		</div>
	</section>
    <!--ABOUT US TOP AREA END-->


@stop
