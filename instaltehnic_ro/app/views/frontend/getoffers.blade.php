@extends('layouts.main')
@section('content')
	
	 <!--MIDDLE AREA WRAPPER-->
    <section style="margin-bottom:20px;min-height:400px" class="single-product-area">
        <div class="container">
            <div class="row">
            <!--Category menu area start -->
           <div class="col-md-4">
               <div class="left-category-menu hidden-sm hidden-xs">
                   <div class="left-product-cat">
                       <div class="category-heading">
                           <h2>Toate Categoriile</h2>
                       </div>
                       <!-- CATEGORY-MENU-LIST START -->
                       <div class="category-menu-list">
                           <ul>
                           	@foreach($categories as $category)
						<!--SINGLE MENU START-->
						<li class="arrow-plus">
							<a  href="{{URL::to('/produse/cat/'.$category->id)}}"><span class="cat-thumb"><img src="{{asset('frontend/img/cat-accessories.png')}}" alt=""></span>{{$category->name}}</a>
							<!-- MEGA MENU START -->
									<div class="cat-left-drop-menu">
									@foreach($subcategories as $subcategory)
										@if($subcategory->category_id == $category->id)
											<div class="cat-left-drop-menu-left">
												<a href="{{URL::to('/produse/subcat/'.$subcategory->id)}}" class="menu-item-heading">{{$subcategory->name}}</a>
												
											</div>
										@endif
									@endforeach
									</div>
							<!--  MEGA MENU END -->
						</li>
						<!--SINGLE MENU END-->
                           	@endforeach
                           
                           </ul>
                       </div>
                   <!-- CATEGORY-MENU-LIST END -->
                   </div>
               </div>
           </div>
           <!--CATEGORY MENU AREA END -->
            <!-- LEFT CONTAIN START-->
            <div class="col-md-8">
				<div class="col-sm-12 col-md-12">
					
					<div class="page-product-detail row">
						@foreach($slides as $slide)
						<div style="margin-top:15px" class="single-pro-list-one row ">
							<div class="col-sm-4 col-md-4">
								<div class="sin-pr-list-img">
									<a target="_blank" href="{{asset('uploads/files/'.$slide->pdf)}}"><img src="{{asset('/img/slides/'.$slide->image)}}" alt=""></a>                                                       
								</div>   
							</div>  
							<div style="margin-left:0px" class="product-det-list col-sm-8 col-md-8">
								<div class="name"><a style="font-size:22px;font-weight:bold;font-family:serif" target="_blank" href="{{asset('uploads/files/'.$slide->pdf)}}">{{$slide->name}}</a></div>
								
								
								
								<div class="new-pro-add">
									<div class="new-p-add-cat">
										<span class="add-to-cart"><a target="_blank" href="{{asset('uploads/files/'.$slide->pdf)}}">
										<span style="border-radius:5px">Vezi Oferta</span></a>
										</span>
										
									</div>
								</div>
							 </div> 
						</div>
						@endforeach
					</div>
					
				</div>
            </div>
        </div>
    </section>
    <!--MIDDLE AREA WRAPPER END-->
	
@stop
