@extends('layouts.main')
@section('content')
<!-- Bottom Strip -->
            <div class="bottom-strip-wrapper">
                <div class="bottom-strip">
                    <p>
                    	<span>Sunati la +40 748 29 29 00  sau cereti o oferta </span><a href="{{URL::to('/contact')}}">Cerere Oferta</a>
                    </p>
                </div>
            </div>

            <!-- Start Container -->
            <div id="container">

                <!-- Content -->
                <div class="content our-menu clearfix">

                    <div class="container-top"></div>

                    <section class="page-head">
                        <h1>
                            <span>{{$menu->title}}</span>
                        </h1>
                    </section>
	
                    <!-- Main -->
                    <div id="main-content">

                        
                        <article class="clearfix">
			@if($menu->image)
				<img src="{{asset('img/menus/original/'.$menu->image)}}">
			@endif

                           {{$menu->content}}
			
                        </article>
			@if($images->count() > 0)
				<div class="grid js-masonry">
						<div class="grid-sizer"></div>
		                @foreach($images as $image)
		            	
						<div class="grid-item"><a class="pretty-photo zoom" href="{{asset('img/slides/'.$image->name)}}"><img src="{{asset('img/slides/'.$image->name)}}" alt="{{$menu->title}}"></a></div>
	
		                @endforeach
				</div>
			@endif

			
			</div>
                    </div>
                    <div class="container-bottom"></div>

                </div> <!-- End Content-->

            </div><!-- End Container -->
		<script>
			$(document).ready( function() {
			  var $container = $('#main-content');
			  $container.imagesLoaded( function(){
				  $('.grid').masonry({
				    itemSelector: '.grid-item',
				    columnWidth: '.grid-sizer',
				    percentPosition: true
				  });
			  });
  
			});
		</script>
@stop
