@extends('layouts.main')
@section('content')
	<?php
	$views = $product->views + 1;
	$product->views = $views;
	$product->save();

	$view = new Viewz;
	$view->product = $product->id;
	$view->save();
	?>
	 <!--MIDDLE AREA WRAPPER-->
    <section class="single-product-area">
        <div class="container">
            <div class="row">
            <!--Category menu area start -->
           <div class="col-md-4">
               <div class="left-category-menu hidden-sm hidden-xs">
                   <div class="left-product-cat">
                       <div class="category-heading">
                           <h2>Toate Categoriile</h2>
                       </div>
                       <!-- CATEGORY-MENU-LIST START -->
                       <div class="category-menu-list">
                           <ul>
                           	@foreach($categories as $category)
						<!--SINGLE MENU START-->
						<li class="arrow-plus">
							<a  href="{{URL::to('/produse/cat/'.$category->id)}}"><span class="cat-thumb"><img src="{{asset('frontend/img/cat-accessories.png')}}" alt=""></span>{{$category->name}}</a>
							<!-- MEGA MENU START -->
									<div class="cat-left-drop-menu">
									@foreach($subcategories as $subcategory)
										@if($subcategory->category_id == $category->id)
											<div class="cat-left-drop-menu-left">
												<a href="{{URL::to('/produse/subcat/'.$subcategory->id)}}" class="menu-item-heading special_link">{{$subcategory->name}}</a>

											</div>
										@endif
									@endforeach
									</div>
							<!--  MEGA MENU END -->
						</li>
						<!--SINGLE MENU END-->
                           	@endforeach

                           </ul>
                       </div>
                   <!-- CATEGORY-MENU-LIST END -->
                   </div>
               </div>
           </div>
           <!--CATEGORY MENU AREA END -->
            <!-- LEFT CONTAIN START-->
            <div class="col-md-8">
            <?php
			$discount = "";
			foreach($producatori as $p){
				if($product->producator == $p->id){
					$discount = $p->discount;

					if($discount > 0){
						$normal_price = explode("lei",$product->price);
						$final_price = tofloat(trim($normal_price[0]));

						$discountp = $discount/100 * $final_price;
						$discounted_price = $final_price - $discountp;
					}
				}
			}
			
			

		    ?>
				<div class="col-sm-12 col-md-12">
					<div class="bread-crumb">
					<ul>
						<li class="breadcrumb-arrow"><a href="{{URL::to('/acasa')}}">
						<i class="fa fa-home"></i></a></li>
						<li><a href="#">{{$product->title}}</a></li>
					</ul>
					</div>
					<div class="row padding-left">
						<!--ZOOM START-->
						<div class="col-sm-4 col-md-4 no-padding ">
							<div class="zoomWrapper">
								<div id="img-1" class="zoomWrapper single-zoom">
									<a href="#">
										@if(!empty($product->image))
						                <img src="{{asset('img/products/original/'.$product->image)}}" data-zoom-image="{{asset('img/products/original/'.$product->image)}}" alt="big-6">
										@else
										<img src="{{asset('img/products/original/2016-02-15-17-28-08-ro-default-large_default.jpg')}}" data-zoom-image="{{asset('img/products/original/2016-02-15-17-28-08-ro-default-large_default.jpg')}}" alt="big-6">
										@endif
								</div>
								
							</div>
						</div>
						<!--ZOOM END-->
						<!--PRODUCT DETAIL START-->
						<div class="col-sm-8 col-md-8">
							<div class="product-page-detail">
								<div class="single-pro-list-one">
									<div class="product-det-list">
										<div class="prod-page-name"><h1>{{$product->title}}</h1></div>
											<div class="brand">
												<p>SKU : {{$product->sku}}</p>
											</div>
										<p>
											@if($discount > 0)
										    		<span class="old-price-two">{{$product->price}}</span> <span class="new-price-two">{{$discounted_price}} lei</span>
									    		@else
									    			<span class="new-price-two">{{$product->price}}</span>
								    			@endif
										</p>

										<div class="new-pro-add">
										<div class="new-p-add-cat">
											<span class="add-to-cart">
												<a href="{{URL::to('/contact/'.$product->id)}}">
													<i class="fa fa-shopping-cart" style="font-size:24px"></i>
													<span style="font-size:24px">Cere Oferta</span>
												</a>
											</span>
									</div>
									</div>
								</div>
							</div>
						</div>
						<!--PRODUCT DETAIL END-->
					</div>
					<!--ROW END-->

					<!-- TAB START-->
					<div class="tab-area">
						<div class="row">
							<div class="col-md-12 pro-tab-wrapper">
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#pr-description">Descriere</a></li>
								</ul>
								<div class="tab-content">
									<div id="pr-description" class="tab-pane fade in active">
										{{$product->content}}
										{{$product->details}}
									</div>

								</div>
							</div>
						</div>
					</div>
					<!--TAB  END-->
				</div>
				<!--LEFT CONTAIN END-->

    				<!--FEATURED PRODUCTS AREA START-->
			</div>
    <section class="featured-product-three area-wrapper">
        <div class="container">
           <div class="row">
                <div class="col-md-12 align-left">
         			<h2>Produse Recomandate</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 no-padding">
					<div class="best-seller-three-content">
					@foreach($products as $product)
					<?php

			    		$ctype = null;
			    		$discount = 0;
			    		$normal_proce = 0;
			    		$final_price = 0;
			    		$discountp = 0;
			    		$discounted_price = 0;

			    		if($product->category_id > 0){
			    			$ctype = PCategory::find($product->category_id);
			    			if($ctype->discount > 0){
			    				$discount = $ctype->discount;
						}
					}
					if($product->subcategory_id > 0 && $discount == 0){
						$ctype = PSubcategory::find($product->subcategory_id);
						if($ctype->discount > 0){
							$discount = $ctype->discount;
						}
					}
					if($product->sscat_id > 0 && $discount == 0){
			    			$ctype = Sscat::find($product->sscat_id);
							if(!is_null($ctype)){
			    			if($ctype->discount > 0){
							$discount = $ctype->discount;
						}
					}
					}


					if($discount > 0){
						$normal_price = explode("lei",$product->price);
						$final_price = tofloat(trim($normal_price[0]));

						$discountp = $discount/100 * $final_price;
						$discounted_price = $final_price - $discountp;
					}

				    ?>
					   <!--SINGLE FEATURED PRODUCT-->
						<div class="col-md-3">
							<div class="bs-three-single">
								<div style="height:356px" class="image-sin-three">
									
									<a href="{{URL::to('produs/'.$product->id)}}">
										@if(!empty($product->image))
										<img src="{{asset('img/products/original/'.$product->image)}}" alt="">
										@else
										<img src="{{asset('img/products/original/2016-02-15-17-28-08-ro-default-large_default.jpg')}}" alt="">
										@endif
									</a>
									<div class="cart-link">
									<a style= "border-right:none;width:100%" class="add" href="{{URL::to('/contact/'.$product->id)}}">
									<i class="fa fa-shopping-cart"></i>
									<span>Cere Oferta</span></a>

									</div>
								</div>
								<div class="three-sin-detail">
									<div class="name">
									   <a style="float:none" href="#">{{$product->title}}</a>
									</div>
									<div class="star">

									</div>
									<p>
										@if($discount > 0)
									    		<span class="old-price-two">{{$product->price}}</span> <span class="new-price-two">{{$discounted_price}} lei</span>
								    		@else
								    			<span class="new-price-two">{{$product->price}}</span>
							    			@endif
									</p>
								</div>
							</div>
						</div>
						@endforeach


					</div>
                </div>
            </div>
        </div>
    </section>
     <!--FEATURED PRODUCTS AREA END-->

            </div>
        </div>
    </section>
    <!--MIDDLE AREA WRAPPER END-->

@stop
