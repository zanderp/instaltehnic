@extends('layouts.main')
@section('content')
	
	<div style="margin-bottom:20px;min-height:400px" class="right-top-banner-two col-md-12">
		<div class="bread-crumb">
			<ul>
				<li class="breadcrumb-arrow"><a href="{{URL::to('/acasa')}}">
				<i class="fa fa-home"></i></a></li>
				<li><a href="#">Cautare</a></li>
			</ul>
		</div>
		<div class="row">
		<!--Category menu area start -->
           <div class="col-md-4">
               <div class="left-category-menu hidden-sm hidden-xs">
                   <div class="left-product-cat">
                       <div class="category-heading">
                           <h2>Toate Categoriile</h2>
                       </div>
                       <!-- CATEGORY-MENU-LIST START -->
                       <div class="category-menu-list">
                           <ul>
                           	@foreach($categories as $category)
						<!--SINGLE MENU START-->
						<li class="arrow-plus">
							<a  href="{{URL::to('/produse/cat/'.$category->id)}}"><span class="cat-thumb"><img src="{{asset('frontend/img/cat-accessories.png')}}" alt=""></span>{{$category->name}}</a>
							<!-- MEGA MENU START -->
									<div class="cat-left-drop-menu">
									@foreach($subcategories as $subcategory)
										@if($subcategory->category_id == $category->id)
											<div class="cat-left-drop-menu-left">
												<a href="{{URL::to('/produse/subcat/'.$subcategory->id)}}" class="menu-item-heading">{{$subcategory->name}}</a>
											</div>
										@endif
									@endforeach
									</div>
							<!--  MEGA MENU END -->
						</li>
						<!--SINGLE MENU END-->
                           	@endforeach
                           
                           </ul>
                       </div>
                   <!-- CATEGORY-MENU-LIST END -->
                   </div>
               </div>
           </div>
           <!--CATEGORY MENU AREA END -->
           <div class="col-md-8" style="min-height:450px">
           
           	
	    		@foreach($products as $product)
		         <!--SINGLE BANNER-->
		       <div class="col-md-6">
		          <div class="new-pro-two-sin">
		             
		              <div class="new-pro-two-img">
		              <a href="{{URL::to('produs/'.$product->id)}}">
						@if(!empty($product->image))
		                <img src="{{asset('img/products/original/'.$product->image)}}" alt="">
						@else
						<img src="{{asset('img/products/original/2016-02-15-17-28-08-ro-default-large_default.jpg')}}" alt="">
						@endif
		              </a>
		              </div>
		              <div class="new-pro-two-detail">
		                  <div class="sale-star-two">
		                      <br />
		                   </div>
		                   <p><a class="product_title_over" href="{{URL::to('produs/'.$product->id)}}">{{$product->title}}</a></p>
		                   <p> 
		                   <?php
    		
				    		$ctype = null;
				    		$discount = 0;
				    		$normal_proce = 0;
				    		$final_price = 0;
				    		$discountp = 0;
				    		$discounted_price = 0;
					    		
				    		if($product->category_id > 0){
				    			$ctype = PCategory::find($product->category_id);
				    			if($ctype->discount > 0){
				    				$discount = $ctype->discount;
							}
						}
						if($product->subcategory_id > 0 && $discount == 0){
							$ctype = PSubcategory::find($product->subcategory_id);
							if($ctype->discount > 0){
								$discount = $ctype->discount;
							}
						}
						if($product->sscat_id > 0 && $discount == 0){
				    			$ctype = Sscat::find($product->sscat_id);
								if(!is_null($ctype)){
					    			if($ctype->discount > 0){
										$discount = $ctype->discount;
									}
								}
				    			
						}
						if($product->sscat_id = 0 && $discount == 0){
				    			$ctype = Sscat::find($product->sscat_id);
				    			if($ctype->discount > 0){
								$discount = $ctype->discount;
							}
						}
		
		
						if($discount > 0){
							$normal_price = explode("lei",$product->price);
							$final_price = tofloat(trim($normal_price[0]));
		
							$discountp = $discount/100 * $final_price;
							$discounted_price = $final_price - $discountp;
						}
		
					    ?>
		                   
		                   @if($discount > 0)
		                   		<span class="old-price-two">{{$product->price}}</span> <span class="new-price-two">{{$discounted_price}} lei</span>
	                   		@else
	                   			<span class="new-price-two">{{$product->price}}</span>
                   			@endif
		                   </p>
		              </div>
		              <div class="np-add-link-area">
		                  <div class="single-link"> 
		                     
		                  </div>
		                  <div class="single-link">
		                  	<a href="{{URL::to('produs/'.$product->id)}}" data-toggle="tooltip" title="Vezi Produs" ><i class="fa fa-search"></i></a>
		                  </div>
		                  <div class="single-link">
		                   
		                  </div>
		              </div>
		              <div class="np-add-cart">
		                <a href="{{URL::to('/contact/'.$product->id)}}"><span>Cere Oferta</span></a>
		             </div>
		           
		          </div>
		      </div>
			@endforeach
		</div>
		</div>
		<div style="margin-bottom:20px" class="right-top-banner-two col-md-12">
			
		</div>
	</div>
@stop
