<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>{{$subject}}</h2>

		<div>
			<p>{{$messageBody}}</p>
			<br />
			<p>Nume: {{$contactName}}</p>
			<p>Telefon: {{$telefon}}</p>
			<p>Email: {{$email}}</p>
		</div>
	</body>
</html>
