@extends('layouts.admin')
@section('content')
<style>
	.form-control{
		margin-bottom:0px !important;
	}
	.input-group{
		margin-bottom:0px !important;
	}
	.form-group{
		margin-bottom:0px !important;
	}
	.oferta{
		display:none;
	}
	.media-plus{display:none;}

</style>
<style>
@media print {
  body * {
    visibility: hidden;
  }
  #content * {
     visibility: visible;
   }
  #content{
	 position: absolute;
	 font-size:10px;
	 left: 0;
	 top: 0;
  }
	#offcanvas-body{
		display:none;
	}
	#offcanvas-tools{
		display:none;
	}
  .btn{
	  display:none;
  }
  .fotorama__nav__shaft{
  	  display:none;
  }
  .galerie{
	  display:none;
  }
  .vizualizeaza{
	  display:none;
  }
  .oferta{display:block;}
  .media-plus{display:block;}
  img {
     display:block;
  }
 .footer {
	 border-top: 1px solid;
 }
 .f-20 {
	 font-size:20px;
 }
 .f-17{
	 font-size: 17px;
 }
}
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>
<script>
function myFunction() {
    window.print();
}
</script>
			<div id="content">
				<section>
				<img class="media-plus" src="{{asset('img/installogo.png')}}">
				<h2 class="oferta">Oferta Comerciala</h2>
				<h2 class="vizualizeaza">Vizualizare Oferta</h2>
					<div class="col-md-12">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<div class="card">
							<div class="card-body">
								<a href="{{URL::to('/admin/goleste')}}"><button class="btn btn-danger pull-left">Goleste Lista</button></a>
								<button class="btn btn-success pull-right" onclick="myFunction()">Listeaza Oferta</button>
								<br></br>
							@foreach($items as $i)
								@foreach($products as $im)
									@if($i->product_id == $im->id)
					   						<h3>Detalii produs</h3>
											{{$im->title}}
										</br>
											{{$im->price}}
										</br>
											{{$im->content}}
										</br>
											{{$im->details}}
											<img src="{{URL::to('img/products/original/'.$im->image)}}">

									@endif
					   			@endforeach
							@endforeach

							</div><!--end .card-body -->
            </div>
						<div class='footer'>
								<p class='f-20'>Ing. Ene Valentin</p>
								<p class='f-17'>office@instaltehnic.ro</p>
								<p class='f-17'>0763772695</p>
						</div>
          </div>
					</section>
				</div>
@stop
