@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Creeaza Minicategorie</h2>
					<div class="col-lg-9">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<div class="card">
							<div class="card-body">
								{{ Form::open(array('url' => 'admin/sscat/store','class' => 'form')) }}
									<div class="form-group">
										{{ Form::text('name', Input::old('name'), array('placeholder' => 'Nume minicategorie', 'class' => 'form-control', 'id' => 'title')) }}
										<label for="title">Nume</label>
									</div>
									<div class="form-group">
										<select class="form-control" name="cat_id">
											@foreach($categories as $c)
												<option value="{{$c->id}}">{{$c->name}}</option>
											@endforeach
										</select>
										<label for="title">Subcategorie Parinte</label>
									</div>
									<div class="form-group">
										{{ Form::text('discount', Input::old('discount'), array('placeholder' => 'Discount', 'class' => 'form-control', 'id' => 'title')) }}
										<label for="title">Discount %</label>
									</div>
									<div class="col-xs-12 text-right">
										{{ Form::submit('Save', array('class' => 'btn btn-primary btn-raised')) }}
									</div><!--end .col -->	
									{{ Form::close() }}						
							</div><!--end .card-body -->
						</div><!--end .card -->
					</div>
											
				</section>
			</div>

@stop
