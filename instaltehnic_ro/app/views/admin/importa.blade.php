@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Importa Produse</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    <b>{{ Session::get('message') }}</b>
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-lg-12">
                             <div class="card">
                             	
                                <div class="card-body">
                                	@if(isset($products))
                                		Hello
                                	@else
                                   	{{ Form::open(array('url' => 'admin/importa/', 'method' => 'POST','class' => 'form', 'enctype' => 'multipart/form-data')) }}
	                                 <div class="form-group">
	                                    {{Form::file('fisier')}}
	                                 </div>
		                              <div class="col-xs-12 text-right">
		                                      {{ Form::submit('Importa', array('class' => 'btn btn-primary btn-raised')) }}
		                              </div><!--end .col -->
		                              {{ Form::close() }}
	                              @endif
                                </div>
                             </div>
                     </div>
		</section>
	</div>

@stop
