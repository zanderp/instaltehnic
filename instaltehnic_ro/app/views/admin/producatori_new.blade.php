@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Adauga Producator</h2>
					<div class="col-lg-9">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<div class="card">
							<div class="card-body">
								{{ Form::open(array('url' => 'admin/producatori/store','class' => 'form', 'files' => true)) }}
									<div class="form-group">
										{{ Form::text('title', Input::old('title'), array('placeholder' => 'Nume Producator', 'class' => 'form-control', 'id' => 'title')) }}
										<label for="title">Nume Producator</label>
									</div>
									<div class="form-group">
										{{ Form::file('image', Input::old('image'), array('placeholder' => 'Selecteaza Imagine', 'class' => 'form-control', 'id' => 'img')) }}
										<label for="title">Imagine</label>
									</div>
									
									<div class="col-xs-12 text-right">
										{{ Form::submit('Save', array('class' => 'btn btn-primary btn-raised')) }}
									</div><!--end .col -->	
									{{ Form::close() }}						
							</div><!--end .card-body -->
						</div><!--end .card -->
					</div>
											
				</section>
			</div>

@stop
