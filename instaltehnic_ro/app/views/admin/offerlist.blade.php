@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Listare Oferte</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
                                        @if (Session::get('messagedenger'))
					<div class="alert alert-danger">
					    {{ Session::get('messagedenger') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-xs-12 text-right">
						<a href="{{URL::to('admin/offers/new/')}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Adauga Oferta</button></a>
					</br>
					</div>
					<div class="col-lg-12">
							
                                            <div class="card">
                                                    <div class="card-body">
                                                            <div class="table-responsive">
                                                                    <table class="table no-margin">
                                                                            <thead>
                                                                                    <tr>
                                                                                            <th>ID</th>
                                                                                            <th>Nume</th>
                                                                                            <th>Meniu</th>
																							<th>Data</th>
																							<th>Ora</th>
                                                                                            <th>Promovat</th>
                                                                                            <th>Editeaza</th>
                                                                                            <th>Sterge</th>
                                                                                    </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                    @foreach($offers as $offer)
                                                                                    <tr>
                                                                                            <td>{{$offer->id}}</td>
                                                                                            <td><a href="{{URL::to('admin/offers/update/'.$offer->id)}}">{{$offer->title}}</a></td>
                                                                                            <td>@foreach($menus as $menu)
                                                                                                            @if($menu->id == $offer->menu_id)
                                                                                                                {{$menu->title}}
                                                                                                            @endif
                                                                                                    @endforeach</td>
																									<td>{{$offer->date}}</td>
		                                                                                            <td>{{$offer->hour}}</td>
																									<td>
		                                                                                                {{Form::open(array('url' => array('admin/offers/updatestatus', $offer->id )))}}
		                                                                                                {{Form::select('status',['yes'=>'Da','no'=>'Nu'],$offer->featured,['style'=>'margin-bottom:0','onchange'=>'submit()'])}}
		                                                                                                {{Form::close()}}
		                                                                                            </td>
                                                                                            
                                                                                            <td><a href="{{URL::to('admin/offers/update/'.$offer->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Editeaza</button></a></td>
                                                                                            <td><a href="{{URL::to('admin/offers/destroy/'.$offer->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger">Sterge</button></a></td>
                                                                                    </tr>
                                                                                    @endforeach
                                                                            </tbody>
                                                                    </table>
                                                            </div><!--end .table-responsive -->
															<?php echo $offers->links(); ?>
                                                    </div><!--end .card-body -->
                                            </div><!--end .card -->
                                    </div>
				</section>
			</div>

@stop
