@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Creaza Oferta</h2>
					<div class="col-lg-9">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
                                                <div class="card">
                                                    <div class="card-body">
                                                            {{ Form::open(array('url' => 'admin/offers/store','class' => 'form','files'=> true)) }}
                                                                    <div class="form-group">
                                                                            {{ Form::text('title', Input::old('title'), array('placeholder' => 'Titlu Aici', 'class' => 'form-control', 'id' => 'title')) }}
                                                                            <label for="title">Titlu</label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                            {{ Form::text('date', Input::old('date'), array('placeholder' => 'Data', 'class' => 'form-control', 'id' => 'date')) }}
                                                                            <label for="title">Data</label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                            {{ Form::text('hour', Input::old('hour'), array('placeholder' => 'Ora', 'class' => 'form-control', 'id' => 'hour')) }}
                                                                            <label for="hour">Ora</label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                            <label>Continut</label>
                                                                            {{ Form::textarea('content', Input::old('content'), array('class' => 'form-control', 'id' => 'summernote')) }}
                                                                    </div>						
                                                    </div><!--end .card-body -->
                                            </div><!--end .card -->
                                    </div>
                                    <div class="col-lg-3">
                                            <div class="card">
                                                    <div class="card-body">
                                                            <div class="form-group">
                                                                    <h3>Selecteaza meniu</h3>
                                                                    <select name="menu_id" class="form-control">
                                                                            
                                                                            <option>Fara Meniu</option>
                                                                            @foreach($menus as $menu)
                                                                            <option value='{{$menu->id}}'>{{$menu->title}}</option>
                                                                            @endforeach
                                                                    </select>
                                                            </div>
                                                            <div class="form-group">
                                                                    <h3>Imagina Principala</h3>
                                                                    {{ Form::file('image') }}
                                                            </div>
                                                            <div class="col-xs-12 text-right">
                                                                    {{ Form::submit('Salveaza', array('class' => 'btn btn-primary btn-raised')) }}
                                                                    {{ Form::close() }}
                                                            </div><!--end .col -->
                                                    </div>
                                            </div>
                                    </div>
				</section>
			</div>

@stop
