@extends('layouts.admin')
@section('content')
    <div id="content">
            <section>
                    <h2>Evenimente</h2>
                    @if (Session::get('message'))
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    @if (Session::get('messagedenger'))
                    <div class="alert alert-danger">
                        {{ Session::get('messagedenger') }}
                    </div>
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="col-lg-6">
                        <a href="{{URL::to('admin/nav/new/left')}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Adauga Meniu Stanga</button></a>	
                        <div class="card">
                                <div class="card-body">
                                        <div class="table-responsive">
                                                <table class="table no-margin">
                                                        <thead>
                                                                <tr>
                                                                    <th>ID</th>
                                                                    <th>Pagina</th>
                                                                    <th>Delete</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($nav_left as $nav)
                                                                <tr>
                                                                    <td>{{$nav->id}}</td>
                                                                    <td>
                                                                       @foreach($pages as $page)
                                                                            @if($page->id == $nav->page)
                                                                                <a href="{{URL::to('admin/events/update/'.$nav->id)}}">{{$page->title}}</a>
                                                                            @endif
                                                                        @endforeach 
                                                                    </td>
                                                                    
                                                                    <td><a href="{{URL::to('admin/nav/destroy/'.$nav->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger">Delete</button></a></td>
                                                                </tr>
                                                            @endforeach    
                                                        </tbody>
                                                </table>
                                        </div><!--end .table-responsive -->
                                </div><!--end .card-body -->
                        </div><!--end .card -->
                </div>
                <div class="col-lg-6">
                        <a href="{{URL::to('admin/nav/new/right')}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Adauga Meniu Dreapta</button></a>	
                        <div class="card">
                                <div class="card-body">
                                        <div class="table-responsive">
                                                <table class="table no-margin">
                                                        <thead>
                                                                <tr>
                                                                    <th>ID</th>
                                                                    <th>Pagina</th>
                                                                    <th>Delete</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($nav_right as $nav)
                                                                <tr>
                                                                    <td>{{$nav->id}}</td>
                                                                    <td>
                                                                       @foreach($pages as $page)
                                                                            @if($page->id == $nav->page)
                                                                                <a href="{{URL::to('admin/events/update/'.$nav->id)}}">{{$page->title}}</a>
                                                                            @endif
                                                                        @endforeach 
                                                                    </td>
                                                                    
                                                                    <td><a href="{{URL::to('admin/nav/destroy/'.$nav->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger">Delete</button></a></td>
                                                                </tr>
                                                            @endforeach        
                                                        </tbody>
                                                </table>
                                        </div><!--end .table-responsive -->
                                </div><!--end .card-body -->
                        </div><!--end .card -->
                </div>
            </section>
    </div>

@stop
