@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Listare Producatori</h2>
					@if (Session::get('message'))
						<div class="alert alert-success">
						    {{ Session::get('message') }}
						</div>
					@endif
                         @if (Session::get('messagedenger'))
						<div class="alert alert-danger">
						    {{ Session::get('messagedenger') }}
						</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-xs-12 text-right">
						<a href="{{URL::to('admin/producatori/new/')}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Adauga Producator</button></a>
					</br>
					</div>
					<div class="col-lg-12">

                                  <div class="card">
                                          <div class="card-body">
                                                  <div class="table-responsive">
                                                          <table class="table no-margin">
                                                                  <thead>
                                                                     <tr>
                                                                        <th>ID</th>
                                                                        <th>Producator</th>
                                                                        <th>Discount</th>
                                                                        <th>Editeaza</th>
                                                                     </tr>
                                                                  </thead>
                                                                  <tbody>
                                                                     @foreach($producatori as $producator)
                                                                     <tr>
                                                                        <td>{{$producator->id}}</td>
                                                                        <td><a href="{{URL::to('admin/producatori/update/'.$producator->id)}}">{{$producator->title}}</a></td>
                                                                  	  <td>{{$producator->discount}} %</td>
                                                                        <td><a href="{{URL::to('admin/producatori/update/'.$producator->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Editeaza</button></a></td>
                                                                     </tr>
                                                                     @endforeach
                                                                  </tbody>
                                                          </table>
                                                  </div><!--end .table-responsive -->

                                          </div><!--end .card-body -->
                                  </div><!--end .card -->
                          </div>
				</section>
			</div>

@stop
