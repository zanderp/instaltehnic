@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Editare Categorie</h2>
					<div class="col-lg-9">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
                                 <div class="card">
                                         <div class="card-body">
                                             {{ Form::model($category, array('url' => array('admin/pcategories/update', $category->id), 'method' => 'POST','class' => 'form')) }}
                                                     <div class="form-group">
                                                             {{ Form::text('name', Input::old('title'), array('placeholder' => 'Title Here', 'class' => 'form-control', 'id' => 'title')) }}
                                                             <label for="title">Nume Categorie</label>
                                                     </div>
                                                     <div class="form-group">
											  {{ Form::text('discount', Input::old('discount'), array('placeholder' => 'Discount', 'class' => 'form-control', 'id' => 'title')) }}
											  <label for="title">Discount %</label>
										   </div>
                                                     <div class="col-xs-12 text-right">
                                                             {{ Form::submit('Update', array('class' => 'btn btn-primary btn-raised')) }}
                                                     </div><!--end .col -->	
                                             {{ Form::close() }}
                                         </div><!--end .card-body -->
                                 </div><!--end .card -->
                         </div>
												
				</section>
			</div>

@stop
