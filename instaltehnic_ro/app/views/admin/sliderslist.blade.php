@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Listare Slidere</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
                         @if (Session::get('messagedenger'))
					<div class="alert alert-danger">
					    {{ Session::get('messagedenger') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-xs-12 text-right">
						<a href="{{URL::to('admin/sliders/new/')}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Adauga Slider</button></a>
					</br>
					</div>
					<div class="col-lg-12">
							
                                            <div class="card">
                                                    <div class="card-body">
                                                            <div class="table-responsive">
                                                                    <table class="table no-margin">
                                                                            <thead>
                                                                                    <tr>
                                                                                       <th>ID</th>
                                                                                       <th>Nume</th>
                                                                                       <th>Editeaza</th>
                                                                                       <th>Sterge</th>
                                                                                    </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                    @foreach($sliders as $slider)
                                                                                    <tr>
                                                                                            <td>{{$slider->id}}</td>
                                                                                            <td><a href="{{URL::to('admin/sliders/update/'.$slider->id)}}">{{$slider->name}}</a></td>
                                                                                            <td><a href="{{URL::to('admin/sliders/update/'.$slider->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Editeaza</button></a></td>
                                                                                            <td><a href="{{URL::to('admin/sliders/destroy/'.$slider->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger">Sterge</button></a></td>
                                                                                    </tr>
                                                                                    @endforeach
                                                                            </tbody>
                                                                    </table>
                                                            </div><!--end .table-responsive -->
															<?php echo $sliders->links(); ?>
                                                    </div><!--end .card-body -->
                                            </div><!--end .card -->
                                    </div>
				</section>
			</div>

@stop
