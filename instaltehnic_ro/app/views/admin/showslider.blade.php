@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Edit Slider</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-lg-9">
                             <div class="card">
                                     <div class="card-body">
                                             {{ Form::model($slider, array('url' => array('admin/sliders/update', $slider->id ), 'method' => 'POST','class' => 'form', 'enctype' => 'multipart/form-data')) }}
                                                     <div class="form-group">
                                                             {{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'id' => 'title')) }}
                                                             <label for="title">Title</label>
                                                     </div>
                                     </div><!--end .card-body -->
                             </div><!--end .card -->
                          </div>
                          <div class="col-lg-3">
                                  <div class="card">
                                          <div class="card-body">
                                                  
                                                  <div class="form-group" id="menajme">
                                                          <h3>Imagine Slider</h3>
                                                          @if(!empty($slider->image))
                                                          {{ HTML::image("/img/slides/$slider->image", "$slider->title" , array('class' => 'img img-responsive')) }}
                                                          <span class="btn btn-default btn-file">
                                                                 Inlocuieste Imagine{{ Form::file('image') }}
                                                          </span>
                                                          @else
                                                          {{Form::file('image')}}
                                                          @endif
                                                  </div>
                                                  @if($slider->pdf)
										<div class="form-group">
											<h4><a target="_blank" href="{{asset('uploads/files/'.$slider->pdf.'')}}">Vezi PDF</a></h4>
										</div>
										<div class="form-group">
											<h3>Inlocuieste PDF</h3>
											{{ Form::file('file') }}
						                    </div>
										@else
										<div class="form-group">
											<h3>Fisier PDF</h3>
											{{ Form::file('file') }}
						                    </div>
						                    @endif
                                                  <div class="col-xs-12 text-right">
                                                          {{ Form::submit('Update', array('class' => 'btn btn-primary btn-raised')) }}
                                                  </div><!--end .col -->
                                                  {{ Form::close() }}
                                          </div>
                                  </div>
                          </div>
				</section>
			</div>

@stop
