@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Edit Page</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-lg-9">
						
						
                                  <div class="card">
                                          <div class="card-body">
                                                  {{ Form::model($page, array('url' => array('admin/pages/update', $page->id ), 'method' => 'POST','class' => 'form', 'enctype' => 'multipart/form-data')) }}
                                                          <div class="form-group">
                                                                  {{ Form::text('title', Input::old('title'), array('placeholder' => 'Title Here', 'class' => 'form-control', 'id' => 'title')) }}
                                                                  <label for="title">Title</label>
                                                          </div>
                                                          <div class="form-group">
                                                                  <label>Content</label>
                                                                  {{ Form::textarea('content', Input::old('content'), array('class' => 'form-control', 'id' => 'summernote')) }}
                                                          </div>

                                          </div><!--end .card-body -->
                                  </div><!--end .card -->
                                    </div>
                                    <div class="col-lg-3">
                                            <div class="card">
                                                    <div class="card-body">
                                                            <div class="form-group">
                                                                    <h3>Parent Page</h3>
                                                                    
                                                                    <select name="parent_id" class="form-control">
                                                                            <option>No Parent</option>
                                                                            @foreach($categories as $category)
                                                                                    @if($category->id == $page->parent_id)
                                                                               	    		<option value="{{$category->id}}" selected="selected"> {{$category->title}}</option>																							{{$category->name}}</option>
                                                                                    @else
                                                                                    		<option value="{{$category->id}}">{{$category->title}}</option>
                                                                                    @endif
                                                                            @endforeach
                                                                    </select>

                                                            </div>
                                                            <div class="form-group" id="menajme">
                                                                    <h3>Main Image</h3>
                                                                    @if(!empty($page->image))
                                                                    {{ HTML::image("/img/pages/small/$page->image", "$page->title" , array('class' => 'img img-responsive')) }}
                                                                    <span class="btn btn-default btn-file">
                                                                            Replace{{ Form::file('image') }}
                                                                    </span>
                                                                    @else
                                                                    {{Form::file('image')}}
                                                                    @endif

                                                            </div>
                                                            <div class="col-xs-12 text-right">
                                                                    {{ Form::submit('Update', array('class' => 'btn btn-primary btn-raised')) }}
                                                            </div><!--end .col -->
                                                            {{ Form::close() }}
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="col-lg-9">
                                            <div class="card">
                                                            <div class="card-head style-primary">
                                                                    <header>Article SEO</header>
                                                            </div>
                                                            <div class="card-body no-padding">
                                                                    @if(!empty($seo))
                                                                    {{ Form::open(array('url'=>'admin/seos/update/'.$seo->id, 'files'=>true, 'class'=>'form')) }}

                                                                                                               @else
                                                                                                       {{ Form::open(array('url'=>'admin/seos/new', 'files'=>true, 'class'=>'form')) }}
                                                                                                                @endif

                                                                                    <div class="form-group">
                                                                                            <input type="text" name="title" class="form-control"
                                                                                                                                               @if(!empty($seo))
                                                                                                                                               value="{{ $seo->title }}"
                                                                                                                                               @endif
                                                                                                                                            placeholder="Your Meta Title Here" />
                                                                                            <label for="title">Meta Title</label>
                                                            </div>
                                                                                    <div class="form-group">
                                                                                            <input type="text" name="description" class="form-control"
                                                                                                                                               @if(!empty($seo))
                                                                                                                                               value="{{ $seo->description }}"
                                                                                                                                               @endif
                                                                                                                                            placeholder="Your Meta Description Here" />
                                                                                            <label for="title">Meta Description</label>
                                                            </div>
                                                                                    <div class="form-group">
                                                                                            <input type="text" name="keywords" class="form-control"
                                                                                                                                               @if(!empty($seo))
                                                                                                                                               value="{{ $seo->keywords }}"
                                                                                                                                               @endif
                                                                                                                                            placeholder="Your Meta Keywords Here" />
                                                                                            <label for="title">Meta Keywords</label>
                                                            </div>
                                                                                    <div class="form-group">
                                                                                            <input type="text" name="author" class="form-control"
                                                                                                                                               @if(!empty($seo))
                                                                                                                                               value="{{ $seo->author }}"
                                                                                                                                               @endif
                                                                                                                                        placeholder="Your Name Here" />
                                                                                            <label for="title">Meta Author</label>
                                                            </div>
                                                            <div class="col-xs-12 text-right">
								                         <input type="hidden" name="post_id" value="0">
													<input type="hidden" name="offer_id" value="0">
													<input type="hidden" name="menu_id" value="0">
													<input type="hidden" name="event_id" value="0">
													<input type="hidden" name="gallery_id" value="0">
													<input type="hidden" name="product_id" value="0">
		                                                        <input type="hidden" name="page_id" value="{{$page->id}}">
                                                                    @if(!empty($seo))
                                                                    {{ Form::submit('Update', array('class' => 'btn btn-primary btn-raised')) }}
                                                                     @else
                                                                     {{ Form::submit('Save', array('class' => 'btn btn-primary btn-raised')) }}
                                                                     @endif
                                                            </div><!--end .col -->
                                                            {{ Form::close() }}
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="col-lg-9">
                                            <div class="card">
                                                <div class="card-head style-primary">
                                                        <header>Drag and drop uploader</header>
                                                </div>
                                                <div class="card-body no-padding">

                                                        {{ Form::open(array('url' => 'admin/imgposts/create','class' => 'dropzone dz-clickable','id'=>'my-awesome-dropzone','files'=> true)) }}
                                                                <div class="dz-message">
                                                                        <h3>Drop files here or click to upload.</h3>
                                                                </div>
                                                                <input type="hidden" name="post_id" value="0">
													<input type="hidden" name="offer_id" value="0">
													<input type="hidden" name="menu_id" value="0">
													<input type="hidden" name="event_id" value="0">
													<input type="hidden" name="gallery_id" value="0">
													<input type="hidden" name="product_id" value="0">
                                                                <input type="hidden" name="page_id" value="{{$page->id}}">
                                                                @foreach($imgpages as $imgpage)
                                                                   @if($imgpage->page_id == $page->id)
                                                                <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">  															
                                                                <div class="dz-image"><img data-dz-thumbnail="" alt="download.jpeg" src="{{asset($imgpage->image)}}"></div>  <div class="dz-details">
                                                            		<a href="{{URL::to('admin/imgposts/destroy/'.$imgpage->id.'/'.$page->id.'/page')}}"><button type="button" class="btn ink-reaction btn-floating-action btn-primary"><i class="md md-delete"></i></button></a></div>
                                              			   </div>
                                                       		 @endif
                                                       	 @endforeach
                                                        {{ Form::close() }}
                                                </div><!--end .card-body -->
                                        </div>
                                    </div>
				</section>
			</div>

@stop
