@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Posts Categories</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-xs-12 text-right">
						<a href="{{URL::to('admin/categories/new/')}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Add Category</button></a>
					</br>
					</div>
					<div class="col-lg-12">
							
													<div class="card">
														<div class="card-body">
															<div class="table-responsive">
																<table class="table no-margin">
																	<thead>
																		<tr>
																			<th>ID</th>
																			<th>Name</th>
																			<th>Date</th>
																			<th>Edit</th>
																			<th>Delete</th>
																		</tr>
																	</thead>
																	<tbody>
																		@foreach($categories as $c)
																		<tr>
																			<td>{{$c->id}}</td>
																			<td><a href="{{URL::to('admin/categories/update/'.$c->id)}}">{{$c->name}}</a></td>
																			<td>{{$c->created_at}}</td>
																			<td><a href="{{URL::to('admin/categories/update/'.$c->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Edit</button></a></td>
																			<td><a href="{{URL::to('admin/categories/destroy/'.$c->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger">Delete</button></a></td>
																		</tr>
																		@endforeach
																	</tbody>
																</table>
															</div><!--end .table-responsive -->
														</div><!--end .card-body -->
													</div><!--end .card -->
												</div>
				</section>
			</div>

@stop
