@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Editare Oferta</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-lg-9">
						
						
                                            <div class="card">
                                                    <div class="card-body">
                                                            {{ Form::model($offer, array('url' => array('admin/offers/update', $offer->id ), 'method' => 'POST','class' => 'form', 'enctype' => 'multipart/form-data')) }}
                                                                    <div class="form-group">
                                                                            {{ Form::text('title', Input::old('title'), array('placeholder' => 'Titlu Aici', 'class' => 'form-control', 'id' => 'title')) }}
                                                                            <label for="title">Titlu</label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                            {{ Form::text('date', Input::old('date'), array('placeholder' => 'Data', 'class' => 'form-control', 'id' => 'date')) }}
                                                                            <label for="title">Data</label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                            {{ Form::text('hour', Input::old('hour'), array('placeholder' => 'Ora', 'class' => 'form-control', 'id' => 'hour')) }}
                                                                            <label for="hour">Ora</label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                            <label>Continut</label>
                                                                            {{ Form::textarea('content', Input::old('content'), array('class' => 'form-control', 'id' => 'summernote')) }}
                                                                    </div>

                                                    </div><!--end .card-body -->
                                            </div><!--end .card -->
                                    </div>
                                    <div class="col-lg-3">
                                            <div class="card">
                                                    <div class="card-body">
                                                            <div class="form-group">
                                                                    <h3>Selecteaza meniu</h3>
                                                                    
                                                                    <select name="menu_id" class="form-control">
                                                                            <option>Fara meniu</option>
                                                                            @foreach($menus as $menu)
                                                                                    @if($menu->id == $offer->menu_id)
                                                                                        <option value="{{$menu->id}}" selected="selected"> {{$menu->title}}</option>
                                                                                    @else
                                                                                        <option value="{{$menu->id}}">{{$menu->title}}</option>
                                                                                    @endif
                                                                            @endforeach
                                                                    </select>

                                                            </div>
                                                            <div class="form-group" id="menajme">
                                                                    <h3>Imagine Principala</h3>
                                                                    @if(!empty($offer->image))
                                                                    {{ HTML::image("/img/offers/small/$offer->image", "$offer->title" , array('class' => 'img img-responsive')) }}
                                                                    <span class="btn btn-default btn-file">
                                                                            Inlocuieste{{ Form::file('image') }}
                                                                    </span>
                                                                    @else
                                                                    {{Form::file('image')}}
                                                                    @endif

                                                            </div>
                                                            <div class="col-xs-12 text-right">
                                                                    {{ Form::submit('Modifica', array('class' => 'btn btn-primary btn-raised')) }}
                                                            </div><!--end .col -->
                                                            {{ Form::close() }}
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="col-lg-9">
                                            <div class="card">
                                                            <div class="card-head style-primary">
                                                                    <header>SEO Oferta</header>
                                                            </div>
                                                            <div class="card-body no-padding">
                                                                    @if(!empty($seo))
                                                                    {{ Form::open(array('url'=>'admin/seos/update/'.$seo->id, 'files'=>true, 'class'=>'form')) }}

                                                                                                               @else
                                                                                                       {{ Form::open(array('url'=>'admin/seos/new', 'files'=>true, 'class'=>'form')) }}
                                                                                                                @endif

                                                                                    <div class="form-group">
                                                                                            <input type="text" name="title" class="form-control"
                                                                                                                                               @if(!empty($seo))
                                                                                                                                               value="{{ $seo->title }}"
                                                                                                                                               @endif
                                                                                                                                            placeholder="Aici Meta Titlu" />
                                                                                            <label for="title">Titlul Paginii</label>
                                                            </div>
                                                                                    <div class="form-group">
                                                                                            <input type="text" name="description" class="form-control"
                                                                                                                                               @if(!empty($seo))
                                                                                                                                               value="{{ $seo->description }}"
                                                                                                                                               @endif
                                                                                                                                            placeholder="Aici Meta Descriere" />
                                                                                            <label for="title">Meta Descriere</label>
                                                            </div>
                                                                                    <div class="form-group">
                                                                                            <input type="text" name="keywords" class="form-control"
                                                                                                                                               @if(!empty($seo))
                                                                                                                                               value="{{ $seo->keywords }}"
                                                                                                                                               @endif
                                                                                                                                            placeholder="Aici Cuvinte Cheie" />
                                                                                            <label for="title">Cuvinte Cheie</label>
                                                            </div>
                                                                                    <div class="form-group">
                                                                                            <input type="text" name="author" class="form-control"
                                                                                                                                               @if(!empty($seo))
                                                                                                                                               value="{{ $seo->author }}"
                                                                                                                                               @endif
                                                                                                                                        placeholder="Numele Tau Aici" />
                                                                                            <label for="title">Autor</label>
                                                            </div>
                                                            <div class="col-xs-12 text-right">
                                                                    <input type="hidden" name="post_id" value="0">
                                                                    <input type="hidden" name="page_id" value="0">
                                                                    <input type="hidden" name="offer_id" value="{{$offer->id}}">
                                                                    @if(!empty($seo))
                                                                    {{ Form::submit('Modifica', array('class' => 'btn btn-primary btn-raised')) }}
                                                                     @else
                                                                     {{ Form::submit('Salveaza', array('class' => 'btn btn-primary btn-raised')) }}
                                                                     @endif
                                                            </div><!--end .col -->
                                                            {{ Form::close() }}
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="col-lg-9">
                                            <div class="card">
                                                <div class="card-head style-primary">
                                                        <header>Galerie</header>
                                                </div>
                                                <div class="card-body no-padding">

                                                        {{ Form::open(array('url' => 'admin/imgposts/create','class' => 'dropzone dz-clickable','id'=>'my-awesome-dropzone','files'=> true)) }}
                                                                <div class="dz-message">
                                                                        <h3>Trage fisiere aici sau fa click pentru incarcare.</h3>
                                                                </div>
                                                                <input type="hidden" name="post_id" value="0">
                                                                <input type="hidden" name="page_id" value="0">
                                                                <input type="hidden" name="offer_id" value="{{$offer->id}}">
                                                                @foreach($imgoffers as $imgoffer)
                                                                                                                                                                                                                                        @if($imgoffer->offer_id == $offer->id)
                                                                <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">  															
                                                                        <div class="dz-image"><img data-dz-thumbnail="" alt="download.jpeg" src="{{asset($imgoffer->image)}}"></div>  <div class="dz-details">
                                                                                <a href="{{URL::to('admin/imgposts/destroy/'.$imgoffer->id.'/'.$offer->id)}}"><button type="button" class="btn ink-reaction btn-floating-action btn-primary"><i class="md md-delete"></i></button></a><button type="button" class="btn ink-reaction btn-floating-action btn-primary"><i class="md md-mode-edit"></i></button>  </div>

                                                        </div>
                                                        @endif
                                                        @endforeach
                                                        {{ Form::close() }}
                                                </div><!--end .card-body -->
                                        </div>
                                    </div>
				</section>
			</div>

@stop
