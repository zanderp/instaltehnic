@extends('layouts.admin')
@section('content')
    <div id="content">
       
            <section>
                    @if($type == "left")
                        <h2>Meniu Stanga</h2>
                    @else
                        <h2>Meniu Dreapta</h2>
                    @endif
                    <div class="col-lg-9">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="card">
                                    <div class="card-body">
                                        {{ Form::open(array('url' => 'admin/nav/store','class' => 'form')) }}
                                        <input type="hidden" name="type" value="{{$type}}">
                                            <div class="form-group">
                                                <select name="page" class="form-control">
                                                    <option>Alege o pagina</option>
                                                    @foreach($pages as $page)
                                                    <option value='{{$page->id}}'>{{$page->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-xs-12 text-right">
                                                {{ Form::submit('Save', array('class' => 'btn btn-primary btn-raised')) }}
                                            </div><!--end .col -->
                                        {{ Form::close() }}						
                                    </div><!--end .card-body -->
                            </div><!--end .card -->
                    </div>

            </section>
    </div>

@stop
