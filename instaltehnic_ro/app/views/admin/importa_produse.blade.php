@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Importa Produse</h2>
					@if(count($products) > 0)
						<div class="alert alert-success">
						    <b>Produse Importate!</b>
						</div>
					@endif
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-lg-12">
                             <div class="card">
                             	
                                <div class="card-body">
                                	<table class="table table-striped">
                                		{{ Form::open(array('url' => array('admin/importafinal',$csv), 'method' => 'POST','class' => 'form')) }}
                           			@foreach($products as $key=>$product)
                           			<tr>
                           				<td>{{$product}}</td>
                           				<td>
		                      				<div class="form-group">
				                 				<select class="form-control" name="col_type[]">
				                 				  <option value="ignore-{{$key}}">Ignora</option>
											  <option value="sku-{{$key}}">SKU</option>
											  <option value="price-{{$key}}">Pret</option>
											</select>
										</div>
									</td>
                      				</tr>
                      				@endforeach
                                	
                                	</table>
									<div class="form-group">
										<select class="form-control" name="bifa">
											<option>None</option>
											<option value="1">Pentru Bosh original</option>
										</select>
                  				<div class="form-group">
	                 				<select class="form-control" name="producator">
										<option>Selecteaza Producator</option>
										@foreach($producatori as $p)
	                 				  	<option value="{{$p->id}}">{{$p->title}}</option>
										@endforeach
									</select>
								</div>
                  				<div class="form-group">
	                 				<select class="form-control" name="categorie">
										<option>Selecteaza Categorie</option>
										@foreach($categorii as $p)
	                 				  	<option value="{{$p->id}}">{{$p->name}}</option>
										@endforeach
									</select>
								</div>
                  				<div class="form-group">
	                 				<select class="form-control" name="subcategorie">
										<option>Selecteaza Subcategorie</option>
										@foreach($subcategorii as $p)
	                 				  	<option value="{{$p->id}}">{{$p->name}}</option>
										@endforeach
									</select>
								</div>
                  				<div class="form-group">
	                 				<select class="form-control" name="sscat">
										<option>Selecteaza Sub-subcategorie</option>
										@foreach($sscat as $p)
	                 				  	<option value="{{$p->id}}">{{$p->name}}</option>
										@endforeach
									</select>
								</div>	
							<div class="col-md-6 text-right">
								 {{ Form::submit('Importa', array('class' => 'btn btn-primary btn-raised')) }}
							</div>
							{{Form::close()}}
                                </div>
                             </div>
                     </div>
		</section>
	</div>

@stop
