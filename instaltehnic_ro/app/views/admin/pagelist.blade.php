@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Listare Pagini</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
                                        @if (Session::get('messagedenger'))
					<div class="alert alert-danger">
					    {{ Session::get('messagedenger') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-xs-12 text-right">
						<a href="{{URL::to('admin/pages/new/')}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Adauga Pagina</button></a>
					</br>
					</div>
					<div class="col-lg-12">
							
                                            <div class="card">
                                                    <div class="card-body">
                                                            <div class="table-responsive">
                                                                    <table class="table no-margin">
                                                                            <thead>
                                                                                    <tr>
                                                                                            <th>ID</th>
                                                                                            <th>Nume</th>
                                                                                            <th>Parinte</th>
                                                                                            <th>Data</th>
                                                                                            <th>Editeaza</th>
                                                                                            <th>Sterge</th>
                                                                                    </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                    @foreach($pages as $page)
                                                                                    <tr>
                                                                                            <td>{{$page->id}}</td>
                                                                                            <td><a href="{{URL::to('admin/pages/update/'.$page->id)}}">{{$page->title}}</a></td>
                                                                                            <td>@foreach($categories as $category)
                                                                                                            @if($category->id == $page->parent_id)
                                                                                                                    <a href="{{URL::to('admin/categories/update/'.$page->parent_id)}}">{{$category->title}}</a>
                                                                                                            @endif
                                                                                                    @endforeach
                                                                                            </td>
                                                                                            <td>{{$page->created_at}}</td>
                                                                                            <td><a href="{{URL::to('admin/pages/update/'.$page->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Editeaza</button></a></td>
                                                                                            <td><a href="{{URL::to('admin/pages/destroy/'.$page->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger">Sterge</button></a></td>
                                                                                    </tr>
                                                                                    @endforeach
                                                                            </tbody>
                                                                    </table>
                                                            </div><!--end .table-responsive -->
															<?php echo $pages->links(); ?>
                                                    </div><!--end .card-body -->
                                            </div><!--end .card -->
                                    </div>
				</section>
			</div>

@stop
