@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Website Contact</h2>
					<div class="col-lg-9">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
                            <div class="card">
                                    <div class="card-body">
                                        @if($setting)

                                            {{ Form::model($setting, array('url' => array('admin/settings/update', $setting->id ), 'method' => 'POST','class' => 'form', 'enctype' => 'multipart/form-data')) }}


                                                    <div class="form-group">
                                                            {{ Form::text('punct_lucru', Input::old('punct_lucru'), array('placeholder' => 'Punct de Lucru', 'class' => 'form-control', 'id' => 'punct_lucru')) }}
                                                            <label for="punct_lucru">Punct de Lucru</label>
                                                    </div>
                                                    <div class="form-group">
                                                            {{ Form::text('telefon', Input::old('telefon'), array('placeholder' => 'Telefon / Fax', 'class' => 'form-control', 'id' => 'telefon')) }}
                                                            <label for="telefon">Telefon / Fax</label>
                                                    </div>
                                                    <div class="form-group">
                                                            {{ Form::text('mobil', Input::old('mobil'), array('placeholder' => 'Telefon Mobil', 'class' => 'form-control', 'id' => 'mobil')) }}
                                                            <label for="mobil">Telefon Mobil</label>
                                                    </div>
                                                    <div class="form-group">
                                                            {{ Form::text('email', Input::old('email'), array('placeholder' => 'Email', 'class' => 'form-control', 'id' => 'email')) }}
                                                            <label for="email">Email</label>
                                                    </div>
																										<div class="form-group">
																														{{ Form::text('lat', Input::old('lat'), array('placeholder' => 'Latitudine', 'class' => 'form-control', 'id' => 'lat')) }}
																														<label for="lat">Latitudine</label>
																										</div>
																										<div class="form-group">
																														{{ Form::text('longi', Input::old('longi'), array('placeholder' => 'Longitudine', 'class' => 'form-control', 'id' => 'longi')) }}
																														<label for="longi">Longitudine</label>
																										</div>
                                                    <div class="col-xs-12 text-right">
                                                            {{ Form::submit('Save', array('class' => 'btn btn-primary btn-raised')) }}
                                                    </div><!--end .col -->
                                                {{ Form::close() }}
                                        @else

                                                {{ Form::open(array('url' => 'admin/settings/store','class' => 'form')) }}

                                                    <div class="form-group">
                                                            {{ Form::text('punct_lucru', Input::old('punct_lucru'), array('placeholder' => 'Punct de Lucru', 'class' => 'form-control', 'id' => 'punct_lucru')) }}
                                                            <label for="punct_lucru">Punct de Lucru</label>
                                                    </div>
                                                    <div class="form-group">
                                                            {{ Form::text('telefon', Input::old('telefon'), array('placeholder' => 'Telefon / Fax', 'class' => 'form-control', 'id' => 'telefon')) }}
                                                            <label for="telefon">Telefon / Fax</label>
                                                    </div>
                                                    <div class="form-group">
                                                            {{ Form::text('mobil', Input::old('mobil'), array('placeholder' => 'Telefon Mobil', 'class' => 'form-control', 'id' => 'mobil')) }}
                                                            <label for="mobil">Telefon Mobil</label>
                                                    </div>
                                                    <div class="form-group">
                                                            {{ Form::text('email', Input::old('email'), array('placeholder' => 'Email', 'class' => 'form-control', 'id' => 'email')) }}
                                                            <label for="email">Email</label>
                                                    </div>
																										<div class="form-group">
																														{{ Form::text('lat', Input::old('lat'), array('placeholder' => 'Latitudine', 'class' => 'form-control', 'id' => 'lat')) }}
																														<label for="lat">Latitudine</label>
																										</div>
																										<div class="form-group">
																														{{ Form::text('longi', Input::old('longi'), array('placeholder' => 'Longitudine', 'class' => 'form-control', 'id' => 'longi')) }}
																														<label for="longi">Longitudine</label>
																										</div>
                                                    <div class="col-xs-12 text-right">
                                                            {{ Form::submit('Save', array('class' => 'btn btn-primary btn-raised')) }}
                                                    </div><!--end .col -->
                                                {{ Form::close() }}
                                        @endif

                                    </div><!--end .card-body -->
                            </div><!--end .card -->
                    </div>

				</section>
			</div>

@stop
