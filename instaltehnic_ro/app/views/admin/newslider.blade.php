@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Adauga Slider</h2>
					<div class="col-lg-9">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<div class="card">
							<div class="card-body">
								{{ Form::open(array('url' => 'admin/sliders/store','class' => 'form','files'=> true)) }}
									<div class="form-group">
										{{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'id' => 'title')) }}
										<label for="title">Nume Slider</label>
									</div>
								
							</div><!--end .card-body -->
						</div><!--end .card -->
					</div>
					<div class="col-lg-3">
						<div class="card">
							<div class="card-body">
							
								<div class="form-group">
									<h3>Imagine Slider</h3>
									{{ Form::file('image') }}
								</div>
								<div class="form-group">
									<h3>Fisier PDF</h3>
									{{ Form::file('file') }}
		                              </div>
								<div class="col-xs-12 text-right">
									{{ Form::submit('Save', array('class' => 'btn btn-primary btn-raised')) }}
									{{ Form::close() }}
								</div><!--end .col -->
							</div>
						</div>
					</div>
				</section>
			</div>

@stop
