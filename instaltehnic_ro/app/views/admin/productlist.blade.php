@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Listare Produse</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
                                        @if (Session::get('messagedenger'))
					<div class="alert alert-danger">
					    {{ Session::get('messagedenger') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif

					<div class="col-lg-12">
						<div style="margin-top:28px" class="col-lg-6">
							{{ Form::open(array('url' => 'admin/products/search','class' => 'form','files'=> true)) }}
							<div style="margin-bottom:0px" class="form-group floating-label">
								{{ Form::text('search', Input::old('search'), array('class' => 'form-control', 'id' => 'search')) }}
								<label for="search">Cauta dupa Produs</label>
							</div>

						</div>
						<div style="margin-top:45px" class="col-lg-2">
								{{ Form::submit('Cauta', array('class' => 'btn btn-primary btn-raised')) }}
								{{ Form::close() }}
							</div>
						<div style="margin-top:57px" class="col-lg-4 text-right">
							<a href="{{URL::to('admin/products/new/')}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Adauga Produs</button></a>
						</div>
					</div>
					<div class="col-lg-12">

                                            <div class="card">
                                                    <div class="card-body">
                                                            <div class="table-responsive">
                                                                    <table class="table no-margin">
                                                                            <thead>
                                                                                    <tr>
				                                                                        <th>ID</th>
				                                                                        <th>Nume</th>
				                                                                        <th>Producator</th>
																		  <th>Promovat</th>
																		  <th>Pret</th>
				                                                                        <th>Editeaza</th>
				                                                                        <th>Sterge</th>
                                                                                    </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                    @foreach($products as $product)
                                                                                    <tr>
                                                                                            <td>{{$product->id}}</td>
                                                                                            <td><a href="{{URL::to('admin/products/update/'.$product->id)}}">{{$product->title}}</a></td>
                                                                                      	  <td>
                                                                                      	  	@foreach($producatori as $producator)
                                                                                      	  		@if($producator->id == $product->producator)
                                                                                      	  			{{$producator->title}}
                                                                                      	  		@endif
                                                                                      	  	@endforeach
                                                                                      	  </td>
																		  <td>
                                                                                                {{Form::open(array('url' => array('admin/products/status', $product->id )))}}
                                                                                                {{Form::select('status',['1'=>'Da','0'=>'Nu'],$product->featured,['style'=>'margin-bottom:0','onchange'=>'submit()'])}}
                                                                                                {{Form::close()}}
                                                                                            </td>

                                                                                            <td>{{$product->price}}</td>
                                                                                            <td><a href="{{URL::to('admin/products/update/'.$product->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Editeaza</button></a></td>
                                                                                            <td><a href="{{URL::to('admin/products/destroy/'.$product->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger">Sterge</button></a></td>
											<td>
<form action="{{URL::to('/admin/cart')}}" method="POST">
								<input type="hidden" name="id" value="{{$product->id}}">

										<button "btn btn-success pull-right">Adauga la oferta</button>


								</form>
 </td>
</tr>
                                                                                    @endforeach
                                                                            </tbody>
                                                                    </table>
                                                            </div><!--end .table-responsive -->
																														@if (!isset($search))
																														<?php echo $products->links(); ?>
																														@endif
                                                    </div><!--end .card-body -->
                                            </div><!--end .card -->
                                    </div>
				</section>
			</div>

@stop
