@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Adauga Produs</h2>
					<div class="col-lg-9">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<div class="card">
							<div class="card-body">
								{{ Form::open(array('url' => 'admin/products/store','class' => 'form','files'=> true)) }}
									<div class="form-group">
										{{ Form::text('title', Input::old('title'), array('placeholder' => 'Title Here', 'class' => 'form-control', 'id' => 'title')) }}
										<label for="title">Title</label>
									</div>
									<div class="form-group">
										<label>Content</label>
										{{ Form::textarea('content', Input::old('content'), array('class' => 'form-control', 'id' => 'summernote')) }}
									</div>
									<div class="form-group">
										<label>Detalii Produs</label>
										{{ Form::textarea('details', Input::old('details'), array('class' => 'form-control', 'id' => 'summernote2')) }}
									</div>						
							</div><!--end .card-body -->
						</div><!--end .card -->
					</div>
					<div class="col-lg-3">
						<div class="card">
							<div class="card-body">
								<div class="form-group">
									<h3>Categorie</h3>
									<select name="category_id" class="form-control">
										<option>Select Category</option>
										@foreach($categories as $category)
										<option value='{{$category->id}}'>{{$category->name}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<h3>Subcategorie</h3>
									<select name="subcategory_id" class="form-control">
										<option>Selecteaza</option>
										@foreach($subcategories as $category)
										<option value='{{$category->id}}'>{{$category->name}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<h3>Sub-subcategorie</h3>
									<select name="sscat_id" class="form-control">
										<option>Selecteaza</option>
										@foreach($sscats as $category)
										<option value='{{$category->id}}'>{{$category->name}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<h3>Producatori</h3>
									<select name="producator_id" class="form-control">
										<option>Selecteaza</option>
										@foreach($producatori as $producator)
										<option value='{{$producator->id}}'>{{$producator->title}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									{{ Form::text('price', Input::old('price'), array('placeholder' => 'Pret', 'class' => 'form-control', 'id' => 'price')) }}
								</div>
								<div class="form-group">
									<h3>Main Image</h3>
									{{ Form::file('image') }}
								</div>
								<div class="col-xs-12 text-right">
									{{ Form::submit('Save', array('class' => 'btn btn-primary btn-raised')) }}
									{{ Form::close() }}
								</div><!--end .col -->
							</div>
						</div>
					</div>
				</section>
			</div>

@stop
