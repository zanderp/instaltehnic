@extends('layouts.admin')
@section('content')
	<div id="content">
		<section>
			<div class="section-body no-margin">
				<div class="row">
				
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
					
					<div class="col-md-8">
						<h2>Ultimele Produse Vizualizate</h2>
						<!-- BEGIN MESSAGE ACTIVITY -->
						<div class="tab-pane" id="activity">
							@if($vizualizate->count())
								{{ Form::open(array('url' => 'admin/deleteviews','class' => 'form')) }}
									<button type="submit" style="margin-left:40px" class="btn btn-raised btn-default-light ink-reaction">Sterge</button>
								{{ Form::close() }}
							@endif
							<ul class="timeline collapse-lg timeline-hairline">
								@foreach($vizualizate as $vizualizat)
								<li class="timeline-inverted">
									<div class="timeline-circ circ-xl style-primary"><i class="md md-event"></i></div>
									<div class="timeline-entry">
										<div class="card style-default-light">
											<div class="card-body small-padding">
												<span class="text-medium">{{$vizualizat->title}}</span><br/>
												<span class="opacity-50">
													{{$vizualizat->created_at}}
												</span>
												<span style="font-weight:bold" class="opacity-50 pull-right">
													Vizualizari: {{$vizualizat->views}}
												</span>
											</div>
										</div>
									</div><!--end .timeline-entry -->
								</li>
								@endforeach
							</ul>
							<?php echo $vizualizate->links(); ?>
						</div><!--end #activity -->
					</div><!--end .col -->
					<!-- END MESSAGE ACTIVITY -->
				</div>
			</div>
		</div>
	</section>
</div>

@stop
