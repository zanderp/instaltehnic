@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>	Editeaza Discount</h2>
					<div class="col-lg-12">
						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
						<div class="card">
							
							<div class="card-body">
								<h2>Producator: {{$producator->title}}</h2>
								{{ Form::open(array('url' => array('admin/producatori/update',$producator->id), 'class' => 'form','files'=> true)) }}
									<div class="form-group">
										<input type="text" class="form-control" name="discount" id="discount" value="{{$producator->discount}}" />
										<label for="discount">Discount %</label>
									</div>
									<img src="{{asset('img/logos/original/'.$producator->image)}}" style="width:200px">
									<div class="form-group">
										{{ Form::file('image', Input::old('image'), array('placeholder' => 'Inlocuieste Imagine', 'class' => 'form-control', 'id' => 'img')) }}
										<label for="title">Imagine</label>
									</div>
								{{ Form::submit('Update', array('class' => 'btn btn-primary btn-raised')) }}
								{{ Form::close() }}
							</div><!--end .card-body -->
						</div><!--end .card -->
					</div>
				</section>
			</div>

@stop
