@extends('layouts.admin')
@section('content')
			<div id="content">
				<section>
					<h2>Listare Galerii</h2>
					@if (Session::get('message'))
					<div class="alert alert-success">
					    {{ Session::get('message') }}
					</div>
					@endif
                                        @if (Session::get('messagedenger'))
					<div class="alert alert-danger">
					    {{ Session::get('messagedenger') }}
					</div>
					@endif
					@if (count($errors) > 0)
					    <div class="alert alert-danger">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
					<div class="col-xs-12 text-right">
						<a href="{{URL::to('admin/galleries/new/')}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Adauga Galerie</button></a>
					</br>
					</div>
					<div class="col-lg-12">
							
                                            <div class="card">
                                                    <div class="card-body">
                                                            <div class="table-responsive">
                                                                    <table class="table no-margin">
                                                                            <thead>
                                                                                    <tr>
                                                                                            <th>ID</th>
                                                                                            <th>Nume</th>
                                                                                            <th>Editeaza</th>
                                                                                            <th>Sterge</th>
                                                                                    </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                    @foreach($galleries as $g)
                                                                                    <tr>
                                                                                            <td>{{$g->id}}</td>
                                                                                            <td><a href="{{URL::to('admin/galleries/update/'.$g->id)}}">{{$g->name}}</a></td>
                                                                                           
                                                                                            <td><a href="{{URL::to('admin/galleries/update/'.$g->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-primary">Editeaza</button></a></td>
                                                                                            <td><a href="{{URL::to('admin/galleries/destroy/'.$g->id)}}"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger">Sterge</button></a></td>
                                                                                    </tr>
                                                                                    @endforeach
                                                                            </tbody>
                                                                    </table>
                                                            </div><!--end .table-responsive -->
															<?php echo $galleries->links(); ?>
                                                    </div><!--end .card-body -->
                                            </div><!--end .card -->
                                    </div>
				</section>
			</div>

@stop
