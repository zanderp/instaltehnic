<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Basic Page Needs
	================================================== -->
 	<meta charset="utf-8">
   	<meta http-equiv="X-UA-Compatible" content="IE=edge">
   	<meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Instaltehnic</title>
     <meta name="author" content="Developer Ace Com">
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
		<link rel="shortcut icon" type="image/x-icon" href="{{asset('frontend/img/fevicon.png')}}">

		<!-- Google Fonts
		============================================ -->
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,400italic' rel='stylesheet' type='text/css'>

        <!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">

        <!-- font-awesome.min CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('frontend/css/font-awesome.min.css')}}">

        <!--Responsive Mobile Menu
        ============================================ -->
        <link rel="stylesheet" href="{{asset('frontend/css/slicknav.css')}}" />

        <!-- animate CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('frontend/css/animate.css')}}">

        <!-- Preloader css
		============================================ -->
        <link rel="stylesheet" href="{{asset('frontend/css/introLoader.min.css')}}">

        <!--Carousel Slider
        ============================================ -->
        <link href="{{asset('frontend/css/owl.carousel.css')}}" rel="stylesheet">
        <link href="{{asset('frontend/css/owl.theme.css')}}" rel="stylesheet">

        <!-- nivo slider CSS
		============================================ -->
		<link rel="stylesheet" href="{{asset('frontend/custom-slider/css/nivo-slider.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{asset('frontend/custom-slider/css/preview.css')}}" type="text/css" media="screen" />

        <!-- Template CSS
		============================================ -->
        <link href="{{asset('frontend/style.css')}}" rel="stylesheet">

        <!--Responsive CSS
        ============================================ -->
        <link href="{{asset('frontend/css/responsive.css')}}" rel="stylesheet">

        <link href="{{asset('frontend/js/jquery_boxslider/jquery.bxslider.css')}}" rel="stylesheet">
        <script src="{{asset('frontend/js/jquery.js')}}"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="{{asset('frontend/js/gmaps.js')}}"></script>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->


</head>

<body class="home-two single-product">
			<div id="introloader" class="introLoading"></div>
    <div class="container no-padding white-bg">
    <!--HEADER AREA START-->

    <header id="header">

        <!--HEADER MIDDLE START-->
        <div class="header-middle-area ">
            <div class="container">
                <div class="row">

                    <!--MAIN LOGO-->
                    <div class="col-xs-12 col-sm-12 col-md-3 ">
                        <div class="main-logo">
                            <a href="{{URL::to('/acasa')}}"><img src="{{asset('img/installogo.png')}}" alt=""></a>
							<div style="width:100%; text-align:center; margin-top:-34px; font-size:16px;margin-bottom: 20px;">Vanzari - Service - Inchirieri</div>
                        </div>
                    </div>
                    <!--MAIN LOGO END-->
                    <!--PROMOTIONAL AREA START-->
                    <div class="col-xs-12 col-sm-9 col-md-9">
                        <!--SINGLE PROMO-->
                        <div class="header_menu_top">
                        		<ul class="top_headmenu">
                        			<li><a href="{{URL::to('/noutati')}}">Noutati</a></li>
                        			<li><a href="{{URL::to('/ofertespeciale')}}">Oferte Speciale</a></li>
                        		</ul>
                        </div>
                    </div>
                  <!--PROMOTIONAL AREA END-->
                </div>
            </div>
        </div>
        <!--HEADER MIDDLE END-->
        <!--HEADER BOTTOM AREA START-->
        <div class="header-bottom-area ">
            <div class="container">
                <div class="top-menu-area">
                    <div class="row ">
                        <!--MAIN MENU START-->
                        <div class="col-md-8 hidden-sm hidden-xs">
                            <nav class="top-menu">
                                <ul >
                                    <li class="home-icon active"><a href="{{URL::to('/acasa')}}"><i class="fa fa-home"></i></a></li>
                                    <li><a href="{{URL::to('/afisareproduse')}}">Produse</a></li>
                                    <li><a href="{{URL::to('/service')}}">Service</a></li>
                                    <li><a href="{{URL::to('/inchirieri')}}">Inchirieri</a></li>
                                    <li><a href="{{URL::to('/blog')}}">blog</a></li>
							 <li><a href="{{URL::to('/despre')}}">despre noi</a></li>
							 <li><a href="{{URL::to('/contact')}}">contact</a></li>
                                </ul>
                            </nav>
                        </div>
                        <!--MAIN MENU END-->
                        <!--SEARCH AND MOBILE MENU START-->
                        <div class="col-md-4 ">
                           <!--SEARCH START-->
                           <div class="search">

                           {{ Form::open(array('url' => 'search','class' => 'form')) }}
		                      <input type="text" name="search_value" class="search-input" placeholder="Cauta">
		                      <button onClick='submit()' class="btn btn-default btn-lg" type="button">
		                      	<i class="fa fa-search"></i>
                          	 </button>
	                      {{ Form::close() }}

                           </div>
                           <!--SEARCH END-->
                           <!--MOBILE MENU START-->
                            <div class="mobile-menu ">
                                <nav>
                                 <ul id="mobile-menu">
                                 	<a href="{{URL::to('/acasa')}}">Acasa</a>
                                 	<a href="{{URL::to('/afisareproduse')}}">Produse</a>
                            		<a href="{{URL::to('/service')}}">Service</a>
                                 	<a href="{{URL::to('/blog')}}">Blog</a>
                                 	<a href="{{URL::to('/despre')}}">Despre</a>
                                 	<a href="{{URL::to('/contact')}}">Contact</a>
                            		@foreach($categories as $category)

							<!--SINGLE MENU START-->
							<li>
								<a href="/produse/cat/{{$category->id}}">{{$category->name}}</a>
								<!-- MEGA MENU START -->
									@foreach($subcategories as $keym=>$subcategory)

										@if($keym == 0)
											<ul>
										@endif

										@if($subcategory->category_id == $category->id)
												<li><a href="/produse/subcat/{{$subcategory->id}}">{{$subcategory->name}}</a>

													@if(!empty($minicategories))

														@foreach($minicategories as $key=>$minicategory)
															@if($key == 0)
																<ul>
															@endif
															@if($minicategory->subcategory_id == $subcategory->id)
																<li><a href="">{{$minicategory->name}}</a></li>
															@endif
															@if(count($minicategories)-1 == $key)
																</ul>
															@endif
														@endforeach

													@endif

												</li>
										@endif

										@if(count($subcategories)-1 == $keym)
											</ul>
										@endif

									@endforeach
								<!--  MEGA MENU END -->
							</li>
							<!--SINGLE MENU END-->
                                	@endforeach

                                </ul>
                                </nav>
                            </div>
                            <!--MOBILE MENU END-->
                        </div>
                        <!--SEARCH AND MOBILE MENU END-->
                    </div>
                </div>
            </div>
        </div>
        <!--HEADER BOTTOM AREA END-->
    </header>
    <!--HEADER AREA END-->





            @yield('content')




     </div>
    <!--NEWS-LETTER AREA START-->
    <section class="news-letter-area">
        <div class="container">
            <div class="row">
                 <div class="col-sm-6 col-md-6 over-hidden">
			 	<a href="{{URL::to('/service')}}"><img class="img-responsive" src="{{asset('img/1.jpg')}}" style="height:200px"></a>
			 </div>


			 <div class="col-sm-6 col-md-6 over-hidden">
			 	<a href="{{URL::to('/inchirieri')}}"><img class="img-responsive" src="{{asset('img/2.jpg')}}" style="height:200px"></a>
			  </div>
            </div>
        </div>
    </section>
      <!--NEWS-LETTER AREA END-->

      <!--FOOTER AREA START-->
    <footer class="footer-area">
		<div class="footer-top-area">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-md-3 over-hidden">
						<div class="footer-title">
						  <h3>Produse adaugate recent</h3>
						</div>
						<div class="left-footer2">
							<ul>
								@foreach($produse_adaugate as $produs_adaugat)
									<li><a href="{{URL::to('produs/'.$produs_adaugat->id)}}">{{$produs_adaugat->title}}</a></li>
								@endforeach
							</ul>
						</div>
					</div>



					<div class="col-sm-3 col-md-3 over-hidden">
						<div class="footer-title">
						  <h3>Cele mai vizualizate produse</h3>
						</div>
						<div class="left-footer2">
							<ul>
								@foreach($produse_vizualizate as $produs_vizualizat)
									<li><a href="{{URL::to('produs/'.$produs_vizualizat->id)}}">{{$produs_vizualizat->title}}</a></li>
								@endforeach
							</ul>
						</div>
					</div>


					<div class="col-sm-3 col-md-3 over-hidden">
						<div class="footer-title">
						  <h3>De pe blog</h3>
						</div>
						<div class="left-footer2">
							<ul>
								@foreach($postari_blog as $postare_blog)
									<li><a href="{{URL::to('blog/posts/'.$postare_blog->id)}}">{{$postare_blog->title}}</a></li>
								@endforeach
							</ul>
						</div>
					</div>


					<div class="col-sm-3 col-md-3 over-hidden">
						<div class="single-footer">
							<div class="footer-title">
							  <h3>Contacteaza-ne</h3>
							</div>
							<ul class="contact-footer">
								<li>
								<span><i class="fa fa-map-marker"></i></span>
								<div class="footer-contact-detail-top">
								<p>Galati, str. Tudor Vladimirescu nr.158</p>
								</div>
								</li>
								<li>
								<span><i class="fa fa-mobile"></i></span>
								<div class="footer-contact-detail">
									<p>0763772695 / 0749624961</p>
								</div>
								</li>
								<li>
								<span><i class="fa fa-envelope-o"></i></span>
								<div class="footer-contact-detail">
								<p>office@instaltehnic.ro / contabilitate@instaltehnic.ro</p>
								</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
            </div>
        </div>
        <div class="footer-bottom-area">
			<div class="container">
				<div class="row">
          <div class="col-xs-12 col-sm-12 col-md-5">
              <p class="copy">Copyright © 2016 <a href="http://developerace.com">DEVELOPER ACE COM SRL-D </a>All rights reserved.</p>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-7">
						<div class="footer-menu">
							<nav>
								<ul>
									<li><a href="{{URL::to('/acasa')}}">ACASA</a></li>
									<li><a href="#">PRODUSE</a></li>
									<li><a href="{{URL::to('/despre')}}">DESPRE NOI</a></li>
									<li><a href="{{URL::to('/blog')}}">BLOG</a></li>
									<li><a href="{{URL::to('/termeni')}}">TERMENI SI CONDITII</a></li>
									<li><a href="{{URL::to('/contact')}}">CONTACT</a></li>
								</ul>
							</nav>
						</div>
					</div>
        </div>
			</div>
        </div>
    </footer>
    <!--FOOTER  AREA END-->
	</div>
    <!--CONTAINER END-->


	 <!-- JS -->

 		<!-- jquery-1.11.3.min js
		============================================ -->
        <script src="{{asset('frontend/js/vendor/jquery-1.11.3.min.js')}}"></script>

 		<!-- bootstrap js
		============================================ -->
        <script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>

        <!-- wow JS
        ============================================ -->
        <script src="{{asset('frontend/js/wow.min.js')}}"></script>

        <!-- Nivo slider js
        ============================================ -->
        <script src="{{asset('frontend/custom-slider/js/jquery.nivo.slider.js')}}" type="text/javascript"></script>
        <script src="{{asset('frontend/custom-slider/home.js')}}" type="text/javascript"></script>

        <!-- count down js
        ============================================ -->
        <script src="{{asset('frontend/js/jquery.countdown.min.js')}}" type="text/javascript"></script>

        <!-- Preloader js
		============================================ -->
        <script src="{{asset('frontend/js/jquery.introLoader.pack.min.js')}}"></script>

        <!--Mobile Menu Js
        ============================================ -->
        <script src="{{asset('frontend/js/jquery.slicknav.min.js')}}"></script>

        <!-- jquery.scrollUp js -->
        <script src="{{asset('frontend/js/jquery.scrollUp.min.js')}}"></script>

        <!-- carousel owl  js
        ============================================ -->
        <script src="{{asset('frontend/js/owl.carousel.js')}}"></script>

        <!--zoom plugin
        ============================================ -->
        <script src='{{asset('frontend/js/jquery.elevatezoom.js')}}'></script>

        <script src="{{asset('frontend/js/jquery_boxslider/jquery.bxslider.min.js')}}"></script>

         <!--main js
        ============================================ -->
        <script src="{{asset('frontend/js/main.js')}}"></script>
			 <script>
			 $(".cat-left-drop-menu").each(function(){
				 if(!$.trim($(this).html())){
					 $(this).remove();
				 }
			 });
			 </script>

</body>
</html>
