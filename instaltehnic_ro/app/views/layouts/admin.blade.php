<?php
if(Auth::user()!== null){
            $email = Auth::user()->email;
        }
$default = "http://holisticmarketingmanagement.ro/wp-content/uploads/2015/03/Generic_Avatar.png";
$size = 40;

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Admin - Dashboard</title>

		<!-- BEGIN META -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="your,keywords">
		<meta name="description" content="Short explanation about this website">
		<!-- END META -->

		<!-- BEGIN STYLESHEETS -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
		<link type="text/css" rel="stylesheet" href="{{asset('admin/css/theme-default/bootstrap.css?1422792965')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('admin/css/theme-default/materialadmin.css?1425466319')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('admin/css/theme-default/font-awesome.min.css?1422529194')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('admin/css/theme-default/material-design-iconic-font.min.css?1421434286')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('admin/css/theme-default/libs/rickshaw/rickshaw.css?1422792967')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('admin/css/theme-default/libs/morris/morris.core.css?1420463396')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('admin/css/theme-default/libs/summernote/summernote.css?1425218701')}}" />
		<link type="text/css" rel="stylesheet" href="{{asset('admin/css/theme-default/libs/dropzone/dropzone-theme.css?1424887864')}}" />
                <link type="text/css" rel="stylesheet" href="{{asset('admin/css/jquery-ui-theme/jquery-ui.min.css')}}" />
                <link type="text/css" rel="stylesheet" href="{{asset('admin/css/jquery-ui-theme/jquery-ui.structure.min.css')}}" />
                <link type="text/css" rel="stylesheet" href="{{asset('admin/css/jquery-ui-theme/jquery-ui.theme.min.css')}}" />
                <link type="text/css" rel="stylesheet" href="{{asset('admin/css/timepicker/timepicker.css')}}" />
		<!-- END STYLESHEETS -->

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script type="text/javascript" src="{{asset('admin/js/libs/utils/html5shiv.js?1403934957')}}"></script>
		<script type="text/javascript" src="{{asset('admin/js/libs/utils/respond.min.js?1403934956')}}"></script>
		<![endif]-->
	</head>
	<body class="menubar-hoverable header-fixed ">

		<!-- BEGIN HEADER-->
		<header id="header" >
			<div class="headerbar">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="headerbar-left">
					<ul class="header-nav header-nav-options">
						<li class="header-nav-brand" >
							<div class="brand-holder">
								<a href="{{URL::to('admin/dashboard')}}">
									<span class="text-lg text-bold text-primary">WEBSITE ADMIN</span>
								</a>
							</div>
						</li>
						<li>
							<a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
								<i class="fa fa-bars"></i>
							</a>
						</li>
					</ul>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="headerbar-right">
					<ul class="header-nav header-nav-options">
<li>
							<?php
							
							$id = Auth::user()->id;
							$cart = Cart::where('user_id', $id)->first();
							if(isset($cart)){
								$cid = $cart->id;
								$count = CartItem::where('cart_id',$cid)->count();?>
							
								<a href="{{URL::to('/admin/cart/view')}}"><button class="btn btn-icon-toggle ink-reaction"><i class="md-add-shopping-cart"></i>{{$count}}</button></a>
								<?php
									
								}
							
								?>
						</li>						
<li>
							<!-- Search form -->
							<form class="navbar-search" role="search">
								<div class="form-group">
									<input type="text" class="form-control" name="headerSearch" placeholder="Enter your keyword">
								</div>
								<button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
							</form>
						</li>
						
					</ul><!--end .header-nav-options -->
					<ul class="header-nav header-nav-profile">
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
								<img src="<?php $grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size; echo$grav_url ;?>" alt="{{Auth::user()->name}}}}" />
								<span class="profile-info">
									{{Auth::user()->name}}
									<small>Administrator</small>
								</span>
							</a>
							<ul class="dropdown-menu animation-dock">
								<li class="dropdown-header">Config</li>
								<li><a href="../../html/pages/profile.html">My profile</a></li>
								<li class="divider"></li>
								<li><a href="{{URL::to('/admin/settings')}}"><i class="fa fa-fw fa-gear"></i> Settings</a></li>
								<li><a href="{{ URL::to('logout') }}"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
							</ul><!--end .dropdown-menu -->
						</li><!--end .dropdown -->
					</ul><!--end .header-nav-profile -->
					<ul class="header-nav header-nav-toggle">
						<li>
							<a class="btn btn-icon-toggle btn-default" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
								<i class="fa fa-ellipsis-v"></i>
							</a>
						</li>
					</ul><!--end .header-nav-toggle -->
				</div><!--end #header-navbar-collapse -->
			</div>
		</header>
		<!-- END HEADER-->

		<!-- BEGIN BASE-->
		<div id="base">

			<!-- BEGIN OFFCANVAS LEFT -->
			<div class="offcanvas">
			</div><!--end .offcanvas-->
			<!-- END OFFCANVAS LEFT -->
			
			
			@yield('content')
			

			<!-- BEGIN MENUBAR-->
			<div id="menubar" class="menubar-inverse ">
				<div class="menubar-fixed-panel">
					<div>
						<a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
							<i class="fa fa-bars"></i>
						</a>
					</div>
					<div class="expanded">
						<a href="{{URL::to('admin/dashboard')}}">
							<span class="text-lg text-bold text-primary ">WEBSITE ADMIN</span>
						</a>
					</div>
				</div>
				<div class="menubar-scroll-panel">

					<!-- BEGIN MAIN MENU -->
					<ul id="main-menu" class="gui-controls">

						<!-- BEGIN DASHBOARD -->
						<li>
							<a href="{{URL::to('admin/dashboard')}}" class="active">
								<div class="gui-icon"><i class="md md-home"></i></div>
								<span class="title">Dashboard</span>
							</a>
						</li><!--end /menu-li -->
						<!-- END DASHBOARD -->
						 <li class="gui-folder">
                                    <a>
								<div class="gui-icon"><i class="glyphicon glyphicon-film"></i></div>
								<span class="title">Slider</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="{{URL::to('admin/sliders')}}" ><span class="title">Listeaza Elemente</span></a></li>
								<li><a href="{{URL::to('admin/sliders/new')}}" ><span class="title">Element Nou</span></a></li>
							</ul><!--end /submenu -->
                                 </li>
						<li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-web"></i></div>
								<span class="title">Produse</span>
							</a>
							
							<ul>
								<li><a href="{{URL::to('admin/products')}}" ><span class="title">Listeaza Produse</span></a></li>
								<li><a href="{{URL::to('admin/products/new')}}" ><span class="title">Produs Nou</span></a></li>
								<li><a href="{{URL::to('admin/pcategories')}}" ><span class="title">Categorii Produse</span></a></li>
								<li><a href="{{URL::to('admin/psubcategories')}}" ><span class="title">Subcategorii Produse</span></a></li>
								<li><a href="{{URL::to('admin/sscat')}}" ><span class="title">Minicategorii Produse</span></a></li>
								<li><a href="{{URL::to('admin/producatori')}}" ><span class="title">Producatori</span></a></li>
							</ul>
						</li>
						<li class="gui-folder">
							<a>
								<div onclick="location.href = '{{URL::to('/admin/importa')}}';" class="gui-icon"><i class="md md-save"></i></div>
								<span onclick="location.href = '{{URL::to('/admin/importa')}}';" class="title">Importa Produse</span>
							</a>
						</li><!--end /menu-li -->
                                 <li class="gui-folder">
                                    <a>
								<div class="gui-icon"><i class="glyphicon glyphicon-list-alt"></i></div>
								<span class="title">CMS</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="{{URL::to('admin/pages')}}" ><span class="title">List Pages</span></a></li>
								<li><a href="{{URL::to('admin/pages/new')}}" ><span class="title">New Page</span></a></li>
							</ul><!--end /submenu -->
                                 </li>
                                 <li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-subject"></i></div>
								<span class="title">Blog</span>
							</a>
							<!--start submenu -->
							<ul>
								<li><a href="{{URL::to('admin/posts')}}" ><span class="title">List Post</span></a></li>
								<li><a href="{{URL::to('admin/posts/new')}}" ><span class="title">New Post</span></a></li>
								<li><a href="{{URL::to('admin/categories')}}" ><span class="title">Categories</span></a></li>
							</ul><!--end /submenu -->
						</li><!--end /menu-li -->
						
						
						
                                             <!--    <li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-restaurant-menu"></i></div>
								<span class="title">Meniuri</span>
							</a>
							
							<ul>
								<li><a href="{{URL::to('admin/menus')}}" ><span class="title">Listeaza Meniuri</span></a></li>
								<li><a href="{{URL::to('admin/menus/new')}}" ><span class="title">Meniu nou</span></a></li>
							</ul>
						</li>
                                                <li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-assessment"></i></div>
								<span class="title">Evenimente</span>
							</a>
							
							<ul>
								<li><a href="{{URL::to('admin/events')}}" ><span class="title">Listeaza Evenimente</span></a></li>
								<li><a href="{{URL::to('admin/events/new')}}" ><span class="title">Eveniment nou</span></a></li>
							</ul>
						</li>
                                                <li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-menu"></i></div>
								<span class="title">Meniu Navigare</span>
							</a>
							
							<ul>
								<li><a href="{{URL::to('admin/nav')}}" ><span class="title">Meniu</span></a></li>
							</ul>
						</li>
                                                <li class="gui-folder">
							<a>
								<div class="gui-icon"><i class="md md-menu"></i></div>
								<span class="title">Galerie</span>
							</a>
							
							<ul>
								<li><a href="{{URL::to('admin/gallery')}}" ><span class="title">Listare Galerii</span></a></li>
							</ul>
						</li><!--end /menu-li -->
						<!-- END EMAIL -->
					</ul><!--end .main-menu -->
					<!-- END MAIN MENU -->

					<div class="menubar-foot-panel">
						<small class="no-linebreak hidden-folded">
							<span class="opacity-75">Copyright &copy; 2015</span> <strong>Developer Ace COM</strong>
						</small>
					</div>
				</div><!--end .menubar-scroll-panel-->
			</div><!--end #menubar-->
			<!-- END MENUBAR -->

			<!-- BEGIN OFFCANVAS RIGHT -->
			<div class="offcanvas">

				<!-- BEGIN OFFCANVAS SEARCH -->
				<div id="offcanvas-search" class="offcanvas-pane width-8">
					<div class="offcanvas-head">
						<header class="text-primary">Search</header>
						<div class="offcanvas-tools">
							<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
								<i class="md md-close"></i>
							</a>
						</div>
					</div>
					<div class="offcanvas-body no-padding">
						<ul class="list ">
							<li class="tile divider-full-bleed">
								<div class="tile-content">
									<div class="tile-text"><strong>A</strong></div>
								</div>
							</li>
							<li class="tile">
								<a class="tile-content ink-reaction" href="#offcanvas-chat" data-toggle="offcanvas" data-backdrop="false">
									<div class="tile-icon">
										<img src="#" alt="" />
									</div>
									<div class="tile-text">
										Alex Nelson
										<small>123-123-3210</small>
									</div>
								</a>
							</li>
						</ul>
					</div><!--end .offcanvas-body -->
				</div><!--end .offcanvas-pane -->
				<!-- END OFFCANVAS SEARCH -->

				<!-- BEGIN OFFCANVAS CHAT -->
				<div id="offcanvas-chat" class="offcanvas-pane style-default-light width-12">
					<div class="offcanvas-head style-default-bright">
						<header class="text-primary">Chat with Ann Laurens</header>
						<div class="offcanvas-tools">
							<a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
								<i class="md md-close"></i>
							</a>
							<a class="btn btn-icon-toggle btn-default-light pull-right" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false">
								<i class="md md-arrow-back"></i>
							</a>
						</div>
						<form class="form">
							<div class="form-group floating-label">
								<textarea name="sidebarChatMessage" id="sidebarChatMessage" class="form-control autosize" rows="1"></textarea>
								<label for="sidebarChatMessage">Leave a message</label>
							</div>
						</form>
					</div>
					<div class="offcanvas-body">
						<ul class="list-chats">
							<li>
								<div class="chat">
									<div class="chat-avatar"><img class="img-circle" src="#" alt="" /></div>
									<div class="chat-body">
										Yes, it is indeed very beautiful.
										<small>10:03 pm</small>
									</div>
								</div><!--end .chat -->
							</li>
						</ul>
					</div><!--end .offcanvas-body -->
				</div><!--end .offcanvas-pane -->
				<!-- END OFFCANVAS CHAT -->

			</div><!--end .offcanvas-->
			<!-- END OFFCANVAS RIGHT -->

		</div><!--end #base-->
		<!-- END BASE -->

		<!-- BEGIN JAVASCRIPT -->
		<script src="{{asset('admin/js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
                <script src="{{asset('admin/js/libs/jquery-ui/jquery-ui.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/bootstrap/bootstrap.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/spin.js/spin.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/autosize/jquery.autosize.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/moment/moment.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/flot/jquery.flot.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/flot/jquery.flot.time.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/flot/jquery.flot.resize.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/flot/jquery.flot.orderBars.js')}}"></script>
		<script src="{{asset('admin/js/libs/flot/jquery.flot.pie.js')}}"></script>
		<script src="{{asset('admin/js/libs/flot/curvedLines.js')}}"></script>
		<script src="{{asset('admin/js/libs/jquery-knob/jquery.knob.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/sparkline/jquery.sparkline.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/dropzone/dropzone.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/d3/d3.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/d3/d3.v3.js')}}"></script>
		<script src="{{asset('admin/js/libs/rickshaw/rickshaw.min.js')}}"></script>
		<script src="{{asset('admin/js/libs/summernote/summernote.min.js')}}"></script>
		<script src="{{asset('admin/js/core/source/App.js')}}"></script>
		<script src="{{asset('admin/js/core/source/AppNavigation.js')}}"></script>
		<script src="{{asset('admin/js/core/source/AppOffcanvas.js')}}"></script>
		<script src="{{asset('admin/js/core/source/AppCard.js')}}"></script>
		<script src="{{asset('admin/js/core/source/AppForm.js')}}"></script>
		<script src="{{asset('admin/js/core/source/AppNavSearch.js')}}"></script>
		<script src="{{asset('admin/js/core/source/AppVendor.js')}}"></script>
		<script src="{{asset('admin/js/core/demo/Demo.js')}}"></script>
		<!--<script src="{{asset('admin/js/core/demo/DemoDashboard.js')}}"></script>-->
		<script src="{{asset('admin/js/core/demo/DemoFormEditors.js')}}"></script>
		<script src="{{asset('admin/js/core/demo/DemoFormComponents.js')}}"></script>
                <script src="{{asset('admin/js/libs/timepicker/timepicker.js')}}"></script>
		<!-- END JAVASCRIPT -->
                <script>

                      $( "#date" ).datepicker();
                      $('#hour').timepicker();
                      
                      $(".remove_row").click(function(){
                   		
                      	var product_row = $(this).attr("id");
                      	
                      	$("."+product_row).fadeOut("slow",function(){remove();});
                      });
                      
                </script>
	</body>
</html>
