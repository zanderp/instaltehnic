<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
App::error(function(\Symfony\Component\HttpKernel\Exception\NotFoundHttpException $exception, $code)
{
   // handle the exception and show view or redirect to a diff route
    return Redirect::to('/');
});
// route to show the login form
Route::get('login', array('uses' => 'HomeController@showLogin'));

// route to process the form
Route::post('login', array('uses' => 'HomeController@doLogin'));
Route::group(array('before' => 'auth'), function()
{
// logout
Route::get('logout', array('uses' => 'HomeController@doLogout'));
// Dash
Route::get('admin/dashboard', array('uses'=> 'AdminController@getIndex'));
Route::post('admin/deleteviews', array('uses'=> 'AdminController@delViews'));
Route::get('admin/importa', array('uses'=> 'AdminController@importa'));
Route::post('admin/importa', array('uses'=> 'AdminController@importaProduse'));
Route::post('admin/importafinal/{csv}', array('uses'=> 'AdminController@importaProduseFinal'));
//cart
Route::post('admin/cart', array('uses'=>'AdminController@postCart'));
Route::get('admin/cart/view', array('uses'=>'AdminController@getCart'));
Route::get('admin/goleste', array('uses'=>'AdminController@golesteCart'));
//contact settings
Route::get('admin/settings', array('uses'=> 'AdminController@getSettings'));
Route::post('admin/settings/store', array('uses'=> 'AdminController@store'));
Route::post('admin/settings/update/{id}', array('uses'=> 'AdminController@edit'));
// Posts
Route::get('admin/posts', array('uses'=> 'PostController@index'));
Route::get('admin/posts/new',array('uses'=> 'PostController@create'));
Route::post('admin/posts/store',array('uses'=> 'PostController@store'));
Route::get('admin/posts/update/{id}', array('uses' => 'PostController@show'));
Route::post('admin/posts/update/{id}',array('uses' => 'PostController@edit'));
Route::get('admin/posts/destroy/{id}',array('uses'=>'PostController@destroy'));
// Post Categories
Route::get('admin/categories', array('uses'=>'CategoryController@index'));
Route::get('admin/categories/new',array('uses'=> 'CategoryController@create'));
Route::post('admin/categories/store',array('uses'=> 'CategoryController@store'));
Route::get('admin/categories/update/{id}', array('uses' => 'CategoryController@show'));
Route::post('admin/categories/update/{id}',array('uses' => 'CategoryController@edit'));
Route::get('admin/categories/destroy/{id}',array('uses'=>'CategoryController@destroy'));
// imgposts
Route::get('admin/imgposts/destroy/{id}/{post_id}/{type}', array('uses'=>'ImgpostsController@getDestroy'));
Route::post('admin/imgposts/create', array('uses'=>'ImgpostsController@postCreate'));
// Seo
Route::post('admin/seos/new', array('uses'=>'SeoController@store'));
Route::post('admin/seos/update/{id}', array('uses'=>'SeoController@edit'));

// Pages
Route::get('admin/pages', array('uses'=> 'PageController@index'));
Route::get('admin/pages/new',array('uses'=> 'PageController@create'));
Route::post('admin/pages/store',array('uses'=> 'PageController@store'));
Route::get('admin/pages/update/{id}', array('uses' => 'PageController@show'));
Route::post('admin/pages/update/{id}',array('uses' => 'PageController@edit'));
Route::get('admin/pages/destroy/{id}',array('uses'=>'PageController@destroy'));

//Galerie
Route::get('admin/gallery', array('uses' => 'GalleryController@index'));
Route::post('admin/gallery/create', array('uses'=> 'GalleryController@store'));
Route::get('admin/galleries/update/{id}', array('uses' => 'GalleryController@show'));

//Produse
Route::get('admin/products', array('uses'=> 'ProductsController@get'));
Route::get('admin/products/new', array('uses' => 'ProductsController@newProduct'));
Route::post('admin/products/store', array('uses' => 'ProductsController@saveProduct'));
Route::get('admin/products/update/{id}', array('uses' => 'ProductsController@editProduct'));
Route::post('admin/products/update/{id}', array('uses' => 'ProductsController@updateProduct'));
Route::post('admin/products/status/{id}', array('uses' => 'ProductsController@editstatus'));
Route::get('admin/products/destroy/{id}', array('uses' => 'ProductsController@destroyProduct'));
Route::post('admin/products/search', array('uses' => 'ProductsController@search'));

//producatori
Route::get('admin/producatori', array('uses'=> 'ProductsController@getProducatori'));
Route::post('admin/producatori/store', array('uses'=>'ProductsController@storeProducator'));
Route::get('admin/producatori/new', array('uses'=>'ProductsController@newProducator'));
Route::get('admin/producatori/update/{id}', array('uses' => 'ProductsController@editProducator'));
Route::post('admin/producatori/update/{id}', array('uses' => 'ProductsController@updateProducator'));

//Slidere
Route::get('admin/sliders', array('uses'=> 'SlidersController@index'));
Route::get('admin/sliders/new', array('uses' => 'SlidersController@create'));
Route::post('admin/sliders/store', array('uses' => 'SlidersController@store'));
Route::get('admin/sliders/update/{id}', array('uses' => 'SlidersController@edit'));
Route::post('admin/sliders/update/{id}', array('uses' => 'SlidersController@update'));
Route::get('admin/sliders/destroy/{id}', array('uses' => 'SlidersController@destroy'));

//categorii
Route::get('admin/pcategories', array('uses'=> 'ProductsController@getPcats'));
Route::get('admin/pcategories/new', array('uses'=> 'ProductsController@newPcats'));
Route::post('admin/pcategories/store', array('uses' => 'ProductsController@savePcats'));
Route::get('admin/pcategories/update/{id}', array('uses' => 'ProductsController@editPcats'));
Route::post('admin/pcategories/update/{id}', array('uses' => 'ProductsController@updatePcats'));
Route::get('admin/pcategories/destroy/{id}', array('uses' => 'ProductsController@destroyPcats'));

//Subcategorii
Route::get('admin/psubcategories', array('uses'=> 'ProductsController@getPscat'));
Route::get('admin/psubcategories/new', array('uses'=> 'ProductsController@newPscat'));
Route::post('admin/psubcategories/store', array('uses' => 'ProductsController@savePscat'));
Route::get('admin/psubcategories/update/{id}', array('uses' => 'ProductsController@editPscat'));
Route::post('admin/psubcategories/update/{id}', array('uses' => 'ProductsController@updatePscat'));
Route::get('admin/psubcategories/destroy/{id}', array('uses' => 'ProductsController@destroyPscat'));

//sub sub categorii
Route::get('admin/sscat', array('uses'=> 'ProductsController@getSscat'));
Route::get('admin/sscat/new', array('uses'=> 'ProductsController@newSscat'));
Route::post('admin/sscat/store', array('uses' => 'ProductsController@saveSscat'));
Route::get('admin/sscat/update/{id}', array('uses' => 'ProductsController@editSscat'));
Route::post('admin/sscat/update/{id}', array('uses' => 'ProductsController@updateSscat'));
Route::get('admin/sscat/destroy/{id}', array('uses' => 'ProductsController@destroySscat'));


//Navigare
Route::get('admin/nav', array('uses'=> 'NavController@index'));
Route::get('admin/nav/new/{type}',array('uses'=> 'NavController@create'));
Route::post('admin/nav/store',array('uses'=> 'NavController@store'));
Route::get('admin/nav/destroy/{id}',array('uses'=>'NavController@destroy'));

//Scraper
Route::get('admin/scraper', array('uses'=>'ProductsController@getWeb'));


});

//frontend
Route::get('/', array('uses' => 'HomeController@getHome'));
Route::get('/acasa', array('uses' => 'HomeController@getHome'));
Route::get('/contact', array('uses' => 'HomeController@getContact'));
Route::get('/contact/{id}', array('uses' => 'HomeController@getContactWith'));
Route::get('/producatori', array('uses' => 'HomeController@getColaboratori'));
Route::get('/produs/{id}', array('uses' => 'HomeController@getProdus'));
Route::get('/produse/{type}/{id}', array('uses' => 'HomeController@listProduse'));
Route::get('/afisareproduse', array('uses' => 'HomeController@afisareProduse'));
Route::get('/service', array('uses' => 'HomeController@getService'));
Route::get('/ofertespeciale', array('uses' => 'HomeController@getOferte'));
Route::get('/noutati', array('uses' => 'HomeController@getNoutati'));
Route::get('/inchirieri', array('uses' => 'HomeController@getRent'));
Route::post('/search', array('uses' => 'HomeController@search'));
//blog
Route::get('/blog', array('uses'=> 'BlogController@index'));
Route::get('blog/posts/{id}', array('uses'=> 'BlogController@getPost'));
//oferte
Route::get('/oferte', array('uses'=> 'FrontOffersController@index'));
Route::get('offer/{id}', array('uses'=> 'FrontOffersController@getOffer'));
//pagini
Route::get('pages/{id}', array('uses'=> 'HomeController@getPage'));
//contact
Route::post('contact/post', array('uses' => 'AboutController@store'));
//galerie
Route::get('/gallery', array('uses' => 'HomeController@getOnegallery'));
Route::get('/gallery/{id}', array('uses' => 'HomeController@getGallery'));
//despre
Route::get('/despre', array('uses' => 'HomeController@getDespre'));
//termeni
Route::get('/termeni', array('uses' => 'HomeController@getTerms'));
//detalii livrare
Route::get('/livrare',array('uses' => 'HomeController@getLivrare'));
