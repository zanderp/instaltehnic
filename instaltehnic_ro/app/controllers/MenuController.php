<?php

class MenuController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$menus = Menu::paginate(10);
		return View::make('admin.menulist')->with('menus', $menus);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.menucreate');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Menu::$rules);
		if($validator->passes()){
				$menu = new Menu;
				$menu->title = Input::get('title');
				$menu->content = Input::get('content');
				$folders = array(
				                    'original' => '735-425',
				                    'small'   => '268-140',
				                    'medium'  => '460-320',
				                    'large'   => '735-425',
				            );

				$image = Input::file('image');
				if (isset($image)){
				foreach($folders as $key => $data) {
				            $sizes = explode('-', $data);
				            $filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
				       	 	$save_img = $this->makeImage($filename, 'img/menus/' . $key .'/', $sizes[0], $sizes[1], $image);
							$menu->image = $filename;
				           }
					   }
				$menu->save();
				return Redirect::to("admin/menus")
								->with('message',"The menu: <b> {$menu->title} </b>,  was created.");
			}
			return Redirect::to('admin/menus/new')
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.menushow')
		            ->with('menu', Menu::find($id))
                            ->with('seo', Seo::where('menu_id',$id)->first())
                            ->with('imgmenus', Imgpost::all());
	} 


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$validator = Validator::make(Input::all(), Menu::$rulesupdate);
		if($validator->passes()){
            	$menu = Menu::find($id);
				$menu->title = Input::get('title');
				$menu->content = Input::get('content');
				//$offer->parent_id = Input::get('parent_id');
				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '735-425',
				            );

				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$menu->image = $filename;
					foreach($folders as $key => $data) {
						$sizes = explode('-', $data);
				    	$this->makeImage($filename, 'img/menus/' . $key .'/', $sizes[0], $sizes[1], $image);
					}
				}
				$menu->save();
				return Redirect::to("admin/menus")
								->with('message',"The menu: <b> {$menu->title} </b>,  was updated.");
			}
			return Redirect::to('admin/menus/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
                
		$menu = Menu::find($id);

		        if($menu){
		            $menu->delete();
		            return Redirect::to('admin/menus')
		                ->with('message',"The menu: <b>$menu->title</b> was deleted");
		        }
		        return Redirect::to('admin/menu')
		            ->with('messagedenger','Something went wrong');
            
	}
	private function makeImage($a, $b ,$c, $d, $e){
	            $image = $e;
	            $path = public_path($b . $a);
				if($b =="img/menus/original/"){
					Image::make($image->getRealPath())->save($path);
				}else{
					Image::make($image->getRealPath())->resize($c, $d)->save($path);
				}
	            
	    }


}
