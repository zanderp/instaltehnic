<?php

class SeoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$idp = Input::get('post_id');
                $idpage = Input::get('page_id');
                $idoffer = Input::get('offer_id');
                $idmenu = Input::get('menu_id');
                $idevent = Input::get('event_id');
		$validator = Validator::make(Input::all(), Seo::$rules);
		if($validator->passes()){
				$seo = new Seo;
				$seo->title = Input::get('title');
				$seo->description = Input::get('description');
				$seo->keywords = Input::get('keywords');
				$seo->author = Input::get('author');
				$seo->post_id = Input::get('post_id');
                                $seo->page_id = Input::get('page_id');
                                $seo->offer_id = Input::get('offer_id');
                                $seo->menu_id = Input::get('menu_id');
                                $seo->event_id = Input::get('event_id');
                                $seo->product_id = Input::get('product_id');
				$seo->save();
                                if($idp != 0){
                                   return Redirect::to("admin/posts/update/".$idp)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was created."); 
                                }
                                elseif($idoffer != 0){
                                   return Redirect::to("admin/offers/update/".$idoffer)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was created."); 
                                }
                                elseif($idmenu != 0){
                                   return Redirect::to("admin/menus/update/".$idmenu)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was created."); 
                                }
                                elseif($idevent != 0){
                                   return Redirect::to("admin/events/update/".$idevent)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was created."); 
                                }
                                else {
                                    return Redirect::to("admin/pages/update/".$idpage)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was created.");
                                }
				
			}
                        
                        if($idp != 0){
			return Redirect::to('admin/posts/update/'.$idp)
			           ->withErrors($validator)
			           ->withInput();
                        }
                        elseif($idoffer != 0){
			return Redirect::to('admin/offers/update/'.$idoffer)
			           ->withErrors($validator)
			           ->withInput();
                        }
                        elseif($idevent != 0){
			return Redirect::to('admin/events/update/'.$idevent)
			           ->withErrors($validator)
			           ->withInput();
                        }
                        elseif($idmenu != 0){
			return Redirect::to('admin/menus/update/'.$idmenu)
			           ->withErrors($validator)
			           ->withInput();
                        }
                        else{
                            return Redirect::to('admin/posts/update/'.$idpage)
			           ->withErrors($validator)
			           ->withInput();
                        }
                        
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$idp = Input::get('post_id');
                $idpage = Input::get('page_id');
                $idoffer = Input::get('offer_id');
                $idmenu = Input::get('menu_id');
                $idevent = Input::get('event_id');
		$validator = Validator::make(Input::all(), Seo::$rules);
		if($validator->passes()){
				$seo = Seo::find($id);
				$seo->title = Input::get('title');
				$seo->description = Input::get('description');
				$seo->keywords = Input::get('keywords');
				$seo->author = Input::get('author');
				$seo->post_id = Input::get('post_id');
                                $seo->page_id = Input::get('page_id');
                                $seo->offer_id = Input::get('offer_id');
                                $seo->menu_id = Input::get('menu_id');
                                $seo->event_id = Input::get('event_id');
				$seo->save();
                                if($idp != 0){
				return Redirect::to("admin/posts/update/".$idp)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was updated.");
                                }
                                elseif($idoffer != 0){
                                   return Redirect::to("admin/offers/update/".$idoffer)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was created."); 
                                } elseif($idmenu != 0){
                                   return Redirect::to("admin/menus/update/".$idmenu)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was created."); 
                                }elseif($idevent != 0){
                                   return Redirect::to("admin/events/update/".$idevent)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was created."); 
                                }else{
                                    return Redirect::to("admin/pages/update/".$idpage)
								->with('message',"The SEO: <b> {$seo->title} </b>,  was updated.");
                                }
			}
                        if($idp != 0){
			return Redirect::to('admin/posts/update/'.$idp)
			           ->withErrors($validator)
			           ->withInput();
                        }elseif($idoffer != 0){
			return Redirect::to('admin/offers/update/'.$idoffer)
			           ->withErrors($validator)
			           ->withInput();
                        }elseif($idmenu != 0){
			return Redirect::to('admin/menus/update/'.$idmenu)
			           ->withErrors($validator)
			           ->withInput();
                        }elseif($idevent != 0){
			return Redirect::to('admin/events/update/'.$idevent)
			           ->withErrors($validator)
			           ->withInput();
                        }else{
                            return Redirect::to('admin/posts/update/'.$idp)
			           ->withErrors($validator)
			           ->withInput();
                        }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
