<?php

class EventController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$events = Events::paginate(10);
		return View::make('admin.eventlist')
                        ->with('events', $events)
                        ->with('menus',Menu::all());
                
//                return View::make('admin.eventlist')
//                        ->with('events', Event::paginate(10));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.eventcreate')->with('menus',Menu::all());
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Events::$rules);
		if($validator->passes()){
				$event = new Events;
				$event->title = Input::get('title');
				$event->content = Input::get('content');
                               
                                $event->featured = 'no';
                                $event->menu_id = Input::get('menu_id');
				//$page->parent_id = Input::get('parent_id');
				$folders = array(
				                    'original' => '735-425',
				                    'small'   => '268-140',
				                    'medium'  => '460-320',
				                    'large'   => '728-210',
				            );

				$image = Input::file('image');
				if (isset($image)){
				foreach($folders as $key => $data) {
				            $sizes = explode('-', $data);
				            $filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
				       	 	$save_img = $this->makeImage($filename, 'img/events/' . $key .'/', $sizes[0], $sizes[1], $image);
							$event->image = $filename;
				           }
                                }
				$event->save();
				return Redirect::to("admin/events")
								->with('message',"The event: <b> {$event->title} </b>,  was created.");
			}
			return Redirect::to('admin/events/new')
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.eventshow')
		            ->with('event', Events::find($id))
                            ->with('seo', Seo::where('event_id',$id)->first())
                            ->with('imgevents', Imgpost::all())
                            ->with('menus',Menu::all());
	} 


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$validator = Validator::make(Input::all(), Events::$rulesupdate);
		if($validator->passes()){
            	$event = Events::find($id);
				$event->title = Input::get('title');
				$event->content = Input::get('content');
                                
                                $event->menu_id = Input::get('menu_id');
                                
				//$offer->parent_id = Input::get('parent_id');
				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '728-210',
				            );

				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$event->image = $filename;
					foreach($folders as $key => $data) {
						$sizes = explode('-', $data);
				    	$this->makeImage($filename, 'img/events/' . $key .'/', $sizes[0], $sizes[1], $image);
					}
				}
				$event->save();
				return Redirect::to("admin/events")
								->with('message',"The event: <b> {$event->title} </b>,  was updated.");
			}
			return Redirect::to('admin/events/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
	}

        public function editstatus($id)
	{
            	$event = Events::find($id);
                $event->featured = Input::get('status');
                $event->save();
                
                return Redirect::to("admin/events")->with('message',"The event: <b> {$event->title} </b>,  was updated.");
        }
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
                
		$event = Events::find($id);

		        if($event){
		            $event->delete();
		            return Redirect::to('admin/events')
		                ->with('message',"The event: <b>$event->title</b> was deleted");
		        }
		        return Redirect::to('admin/event')
		            ->with('messagedenger','Something went wrong');
            
	}
	private function makeImage($a, $b ,$c, $d, $e){
	            $image = $e;
	            $path = public_path($b . $a);
				if($b =="img/events/original/"){
					Image::make($image->getRealPath())->save($path);
				}else{
					Image::make($image->getRealPath())->resize($c, $d)->save($path);
				}
	            
	    }


}
