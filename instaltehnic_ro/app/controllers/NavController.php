<?php

class NavController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$offers = Offer::paginate(10);
		return View::make('admin.navlist')
                        ->with('nav_left', Nav::Where('type','left')->get())
                        ->with('nav_right', Nav::Where('type','right')->get())
                        ->with('pages', Page::all());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($type)
	{
        
		return View::make('admin.navcreate')
                        ->with('pages', Page::all())
                        ->with('type',$type);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Nav::$rules);
		if($validator->passes()){
				$nav = new Nav;
                                $nav->type = Input::get('type');
				$nav->page = Input::get('page');
                                
				
				$nav->save();
				return Redirect::to("admin/nav")
								->with('message',"Navigare schimbata.");
			}
			return Redirect::to('admin/nav/new/'.Input::get('type'))
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.offershow')
		            ->with('offer', Offer::find($id))
                            ->with('seo', Seo::where('offer_id',$id)->first())
                            ->with('imgoffers', Imgpost::all());
	} 


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$validator = Validator::make(Input::all(), Offer::$rulesupdate);
		if($validator->passes()){
            	$offer = Offer::find($id);
				$offer->title = Input::get('title');
				$offer->content = Input::get('content');
				//$offer->parent_id = Input::get('parent_id');
				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '735-425',
				            );

				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$offer->image = $filename;
					foreach($folders as $key => $data) {
						$sizes = explode('-', $data);
				    	$this->makeImage($filename, 'img/offers/' . $key .'/', $sizes[0], $sizes[1], $image);
					}
				}
				$offer->save();
				return Redirect::to("admin/offers")
								->with('message',"The offer: <b> {$offer->title} </b>,  was updated.");
			}
			return Redirect::to('admin/offers/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
                
		$nav = Nav::find($id);

		        if($nav){
		            $nav->delete();
		            return Redirect::to('admin/nav')
		                ->with('message',"Optiunea de navigarea a fost stearsa.");
		        }
		        return Redirect::to('admin/nav')
		            ->with('messagedenger','Something went wrong');
            
	}
	private function makeImage($a, $b ,$c, $d, $e){
	            $image = $e;
	            $path = public_path($b . $a);
				if($b =="img/offers/original/"){
					Image::make($image->getRealPath())->save($path);
				}else{
					Image::make($image->getRealPath())->resize($c, $d)->save($path);
				}
	            
	    }


}
