<?php

class BlogController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('frontend.blog')
                        ->with('posts',Post::paginate(10))
                        ->with('categories',Category::all())
                        ->with('pages', Page::all())
 ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
							
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get()); 
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getPost($id)
	{
		return View::make('frontend.post')
                        ->with('post', Post::find($id))
                        ->with('categories',Category::all())
                        ->with('pages', Page::all())
					->with('galleries', Gallery::all())
 					->with('categories',PCategory::all())
			      ->with('subcategories',PSubcategory::all())
			      ->with('minicategories',Sscat::all())
			    
				->with('images',  Imgpost::where('post_id',$id)->get())
				->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
		  		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
		  		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get()); 
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
