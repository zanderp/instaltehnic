<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = Post::paginate(10);
		return View::make('admin.plist')->with('posts', $posts)
			->with('categories', Category::all());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.pcreate')
					->with('categories', Category::all());
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Post::$rules);
		if($validator->passes()){
				$post = new Post;
				$post->title = Input::get('title');
				$post->content = Input::get('content');
				$post->category_id = Input::get('category_id');
				$folders = array(
				                    'original' => '735-425',
				                    'small'   => '268-140',
				                    'medium'  => '460-320',
				                    'large'   => '735-425',
				            );

				$image = Input::file('image');
				if (isset($image)){
				foreach($folders as $key => $data) {
				            $sizes = explode('-', $data);
				            $filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
				       	 	$save_img = $this->makeImage($filename, 'img/posts/' . $key .'/', $sizes[0], $sizes[1], $image);
							$post->image = $filename;
				           }
					   }
				$post->save();
				return Redirect::to("admin/posts")
								->with('message',"The post: <b> {$post->title} </b>,  was created.");
			}
			return Redirect::to('admin/posts/new')
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.pshow')
		            ->with('post', Post::find($id))
					->with('seo', Seo::where('post_id',$id)->first())
					->with('categories', Category::all())
					->with('imgposts', Imgpost::all());
	} 


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$validator = Validator::make(Input::all(), Post::$rulesupdate);
		if($validator->passes()){
            	$post = Post::find($id);
				$post->title = Input::get('title');
				$post->content = Input::get('content');
				$post->category_id = Input::get('category_id');
				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '735-425',
				            );

				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$post->image = $filename;
					foreach($folders as $key => $data) {
						$sizes = explode('-', $data);
				    	$this->makeImage($filename, 'img/posts/' . $key .'/', $sizes[0], $sizes[1], $image);
					}
				}
				$post->save();
				return Redirect::to("admin/posts")
								->with('message',"The post: <b> {$post->title} </b>,  was updated.");
			}
			return Redirect::to('admin/posts/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Post::find($id);

		        if($post){
		            $post->delete();
		            return Redirect::to('admin/posts')
		                ->with('message',"The post: <b>$post->title</b> was deleted");
		        }
		        return Redirect::to('admin/posts')
		            ->with('messagedenger','Something went wrong');
	}
	private function makeImage($a, $b ,$c, $d, $e){
	            $image = $e;
	            $path = public_path($b . $a);
				if($b =="img/posts/original/"){
					Image::make($image->getRealPath())->save($path);
				}else{
					Image::make($image->getRealPath())->resize($c, $d)->save($path);
				}
	            
	}


}
