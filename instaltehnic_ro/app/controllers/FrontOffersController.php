<?php

class FrontOffersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	$id = 6;
		return View::make('frontend.offers')
                        ->with('offers', Offer::all())
                        ->with('pages', Page::all())
							->with('evs', Events::all() )
							->with('galleries', Gallery::all())
                ->with('events', Events::where('featured','1')->get())
                ->with('menus', Menu::all())
                ->with('pages_left',Nav::where('type','left')->get())
                ->with('pages_right',Nav::where('type','right')->get())
		->with('page', Page::find($id));
	}
        
        public function getOffer($id)
	{
		return View::make('frontend.offer')
                        ->with('offer', Offer::find($id))
							->with('evs', Events::all() )
							->with('galleries', Gallery::all())
								->with('pages', Page::all())
				                ->with('menus', Menu::all())
				                ->with('pages_left',Nav::where('type','left')->get())
				                ->with('pages_right',Nav::where('type','right')->get()) 
						->with('images',  Imgpost::where('page_id',$id)->get());
							
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
