<?php

class CategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cat = Category::paginate(10);
		return View::make('admin.pcat')->with('categories', $cat);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.pcatn');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Category::$rules);
		if($validator->passes()){
				$cat = new Category;
				$cat->name = Input::get('name');
				$cat->save();
				return Redirect::to("admin/categories")
								->with('message',"The category: <b> {$cat->name} </b>,  was created.");
			}
			return Redirect::to('admin/categories/new')
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.pcate')
		            ->with('category',Category::find($id));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$validator = Validator::make(Input::all(), Category::$rulesupdate);
		if($validator->passes()){
            	$post = Category::find($id);
				$post->name = Input::get('name');
				$post->save();
				return Redirect::to("admin/categories")
								->with('message',"The category: <b> {$post->name} </b>,  was updated.");
			}
			return Redirect::to('admin/categories/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Category::find($id);

		        if($post){
		            $post->delete();
		            return Redirect::to('admin/categories')
		                ->with('message',"The category: <b>$post->name</b> was deleted");
		        }
		        return Redirect::to('admin/categories')
		            ->with('messagedenger','Something went wrong');
	}


}
