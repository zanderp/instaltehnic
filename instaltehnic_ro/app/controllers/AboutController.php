<?php

class AboutController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function store()
	{
	$contactEmail = Input::get('email');
	$contactSubject = Input::get('subject');
	$contactMessage = Input::get('message');
	$contactName = Input::get('name');
	$contactPhone = Input::get('telefon');
	
	$data = array('subject'=>$contactSubject, 'email'=>$contactEmail, 'messageBody'=>$contactMessage, 'contactName'=>$contactName, 'telefon'=>$contactPhone);
	//dd($data);
	Mail::send('emails.contact', $data, function($message) use ($contactEmail, $contactSubject, $contactName, $contactPhone)
	{
		$message->from('office@instaltehnic.ro', 'Contact Website');
		$message->to('office@instaltehnic.ro', 'InstalTehnic')->subject($contactSubject);
	});
	//dd(Mail::failures());
	return Redirect::to('/contact')
		->with('settings', Settings::first())
		->with('message', 'Va multumim pentru mesaj!')
		->with('categories',PCategory::all())
		->with('subcategories',PSubcategory::all())
		->with('minicategories',Sscat::all());

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
