<?php

class OfferController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$offers = Offer::paginate(10);
		return View::make('admin.offerlist')
			->with('offers', $offers)
			->with('menus',Menu::all());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.offercreate')->with('menus',Menu::all());
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Offer::$rules);
		if($validator->passes()){
				$offer = new Offer;
				$offer->title = Input::get('title');
				$offer->content = Input::get('content');
                $offer->date = date("Y:m:d", strtotime(Input::get('date')));
                $offer->hour = Input::get('hour');
				//$page->parent_id = Input::get('parent_id');
				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '728-210',
				            );

				$image = Input::file('image');
				if (isset($image)){
				foreach($folders as $key => $data) {
				            $sizes = explode('-', $data);
				            $filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
				       	 	$save_img = $this->makeImage($filename, 'img/offers/' . $key .'/', $sizes[0], $sizes[1], $image);
							$offer->image = $filename;
				           }
					   }
                $offer->featured = 'no';
                $offer->menu_id = Input::get('menu_id');
				$offer->save();
				return Redirect::to("admin/offers")
								->with('message',"The offer: <b> {$offer->title} </b>,  was created.");
			}
			return Redirect::to('admin/offers/new')
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.offershow')
		            ->with('offer', Offer::find($id))
                            ->with('seo', Seo::where('offer_id',$id)->first())
                            ->with('imgoffers', Imgpost::all())
							->with('menus',Menu::all());
	} 


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$validator = Validator::make(Input::all(), Offer::$rulesupdate);
		if($validator->passes()){
            	$offer = Offer::find($id);
				$offer->title = Input::get('title');
				$offer->content = Input::get('content');
                $offer->date = date("Y:m:d", strtotime(Input::get('date')));
                $offer->hour = Input::get('hour');
				//$offer->parent_id = Input::get('parent_id');
				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '728-210',
				            );

				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$offer->image = $filename;
					foreach($folders as $key => $data) {
						$sizes = explode('-', $data);
				    	$this->makeImage($filename, 'img/offers/' . $key .'/', $sizes[0], $sizes[1], $image);
					}
				}
				$offer->menu_id = Input::get('menu_id');
				$offer->save();
				return Redirect::to("admin/offers")
								->with('message',"The offer: <b> {$offer->title} </b>,  was updated.");
			}
			return Redirect::to('admin/offers/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}
    public function editstatus($id)
	{
        	$offer = Offer::find($id);
            $offer->featured = Input::get('status');
            $offer->save();
            
            return Redirect::to("admin/offers")->with('message',"The event: <b> {$offer->title} </b>,  was updated.");
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
                
		$offer = Offer::find($id);

		        if($offer){
		            $offer->delete();
		            return Redirect::to('admin/offers')
		                ->with('message',"The offer: <b>$offer->title</b> was deleted");
		        }
		        return Redirect::to('admin/offers')
		            ->with('messagedenger','Something went wrong');
            
	}
	private function makeImage($a, $b ,$c, $d, $e){
	            $image = $e;
	            $path = public_path($b . $a);
				if($b =="img/offers/original/"){
					Image::make($image->getRealPath())->save($path);
				}else{
					Image::make($image->getRealPath())->resize($c, $d)->save($path);
				}
	            
	    }


}
