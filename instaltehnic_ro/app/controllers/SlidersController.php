<?php

class SlidersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.sliderslist')->with('sliders',Slider::paginate(10));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.newslider');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Slider::$rules);
		if($validator->passes()){
				$slider = new Slider;
				$slider->name = Input::get('name');
				
				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$slider->image = $filename;
					
				    	$this->makeImage($filename, 'img/slides/', $image);
					
				}
				if (Input::hasFile('file')) {
					   $file            = Input::file('file');
					   $destinationPath = public_path().'/uploads/files/';
					   $filename        = date('Y-m-d-H-i-s') . "-" . $file->getClientOriginalName();
					   $uploadSuccess   = $file->move($destinationPath, $filename);
					   $slider->pdf = $filename;
			    	}
				$slider->save();
				return Redirect::to("admin/sliders")
								->with('message',"Slider adaugat.");
			}
			return Redirect::to('admin/sliders/new')
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return View::make('admin.showslider')
			->with('slider', Slider::find($id));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validator = Validator::make(Input::all(), Slider::$rules);
		if($validator->passes()){
				$slider = Slider::find($id);
				$slider->name = Input::get('name');
				
				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$slider->image = $filename;
					
				    	$this->makeImage($filename, 'img/slides/', $image);
				}
				if (Input::hasFile('file')) {
				   	
						File::delete(public_path().'/uploads/files/'.$slider->pdf);
					
						$file            = Input::file('file');
						$destinationPath = public_path().'/uploads/files/';
						$filename        = date('Y-m-d-H-i-s') . "-" . $file->getClientOriginalName();
						$uploadSuccess   = $file->move($destinationPath, $filename);
						$slider->pdf = $filename;
			    }
				    
				$slider->save();
				return Redirect::to("admin/sliders")
								->with('message',"Slider adaugat.");
			}
			return Redirect::to('admin/sliders/new')
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$slider = Slider::find($id);

		        if($slider){
	        		  File::delete(public_path().'/uploads/files/'.$slider->pdf);
	        		  File::delete(public_path().'/img/slides/'.$slider->image);
		            $slider->delete();
		            return Redirect::to('admin/sliders')
		                ->with('message',"Sliderul a fost sters.");
		        }
		        return Redirect::to('admin/sliders')
		            ->with('messagedenger','Ceva nu a functionat corect.');
	}
	
	private function makeImage($a, $b ,$e){
	
       $image = $e;
       $path = public_path($b . $a);
		Image::make($image->getRealPath())->save($path);
		      
	}


}
