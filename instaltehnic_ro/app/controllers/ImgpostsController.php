<?php

class ImgpostsController extends BaseController {

    public function __construct(){
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->beforeFilter('admin');
    }

    public function postCreate(){

        $validator = Validator::make(Input::all(), Imgpost::$rules);

         if($validator->passes())
        {
            $imgpost = new Imgpost();
            $image = Input::file('file');
            $filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
            $path = public_path('img/slides/' . $filename);
            $upload_success = Image::make($image->getRealPath())->save($path);
            $imgpost->post_id = Input::get('post_id');
            $imgpost->page_id = Input::get('page_id');
            $imgpost->offer_id = Input::get('offer_id');
            $imgpost->menu_id = Input::get('menu_id');
            $imgpost->event_id = Input::get('event_id');
            $imgpost->product_id = Input::get('product_id');
            $image = 'img/slides/' . $filename;
            $imgpost->name = $filename;
            $imgpost->image = $image;
            $imgpost->save();
            if( $upload_success ) {
                return Response::json('success', 200);
            } else {
                return Response::json('error', 400);
            }
        }
         else
         {
             return Response::make($validator->errors->first(), 400);
         }
    }

    public function getDestroy($id,$post_id,$type){
        $imgpost = Imgpost::find($id);

        if($imgpost){
            unlink($imgpost->image);
            $imgpost->delete();
            if($type == "page")
            return Redirect::to("admin/pages/update/$post_id")
                ->with('message', "The picture $imgpost->name was deleted");
        }
    	   if($type == "page")
        return Redirect::to("admin/pages/update/$post_id")
            ->with('messagedenger','Something went wrong');

    }
}
