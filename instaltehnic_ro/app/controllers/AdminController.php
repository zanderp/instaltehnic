<?php

class AdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{

		return View::make('admin.dashboard')->with('vizualizate',DB::Table('vizualizate')->join('products','vizualizate.product', '=', 'products.id')
		->select('vizualizate.product','vizualizate.created_at','products.title','products.views')
		->orderBy('vizualizate.created_at','DESC')
		->paginate(20));
	}

        public function getSettings()
	{
		return View::make('admin.settings')
                        ->with('setting',Settings::first());
	}

	public function delViews(){

		Viewz::truncate();

		return Redirect::to('admin/dashboard')
                        ->with('message','Continutul a fost sters!');

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Settings::$rules);
		if($validator->passes()){
				$settings = new Settings;
				$settings->sediu_social = Input::get('sediu_social');
                                $settings->punct_lucru = Input::get('punct_lucru');
                                $settings->mobil = Input::get('mobil');
				$settings->telefon = Input::get('telefon');
                                $settings->email = Input::get('email');
                                $settings->lat = Input::get('lat');
                                $settings->longi = Input::get('longi');

				$settings->save();
				return Redirect::to("admin/settings")
								->with('message',"Setari salvate.");
			}
			return Redirect::to('admin/settings')
			           ->withErrors($validator)
			           ->withInput();
	}

        public function edit($id)
	{
		$validator = Validator::make(Input::all(), Settings::$rules);
                $settings = Settings::find($id);
		if($validator->passes()){

				$settings->sediu_social = Input::get('sediu_social');
                                $settings->punct_lucru = Input::get('punct_lucru');
                                $settings->mobil = Input::get('mobil');
				$settings->telefon = Input::get('telefon');
                                $settings->email = Input::get('email');
                                $settings->lat = Input::get('lat');
                                $settings->longi = Input::get('longi');

				$settings->save();
				return Redirect::to("admin/settings")
								->with('message',"Setari salvate.");
			}
			return Redirect::to('admin/settings')
			           ->withErrors($validator)
			           ->withInput();
	}

	public function importa(){

		return View::make('admin.importa');

	}

	public function importaProduse(){

		$products = array();
		$i = 0;

		if (Input::hasFile('fisier')) {
		   $file            = Input::file('fisier');
		   $destinationPath = public_path().'/uploads/csv/';
		   $filename        = date('Y-m-d-H-i-s') . "-" . $file->getClientOriginalName();
		   $uploadSuccess   = $file->move($destinationPath, $filename);

		   $file_new = fopen(public_path().'/uploads/csv/'.$filename,"r");

		   //loop through each row of the csv and store them in the array
		   /*
		   while(!feof($file_new))
		  {
		  	$products[$i] = fgetcsv($file_new);
		  	$i++;
		  }
		  */
		  $products = fgetcsv($file_new);
		  fclose($file_new);
	    }

		return View::make('admin.importa_produse')->with('products',array_filter($products))
										->with('csv',$filename)
										->with('producatori', Producator::all())
									    ->with('categorii', PCategory::all())
										->with('sscat', Sscat::all())
										->with('subcategorii', PSubcategory::all());
	}

	public function importaProduseFinal($csv){

		$col_type = Input::get('col_type');
		$ignore 	= Input::get('ignore');
		$producator = Input::get('producator');
		$categorie  = Input::get('categorie');
		$subcategorie = Input::get('subcategorie');
		$bifa = Input::get('bifa');
		$sscat = Input::get('sscat');
		foreach($col_type as $c){
			$col = explode("-",$c);
			if($col[0]== 'ignore'){
				continue 1;
			}elseif($col[0] == 'sku'){
				$sku = $col[1];
			}elseif($col[0]== 'price'){
				$price= $col[1];
			}
		}
		$products = array();
		/*
		if($ignore == "yes")
			$i = 1;
		else $i = 0;
		*/
		$i  = 0;
		$i2 = 0;

		$file_new = fopen(public_path().'/uploads/csv/'.$csv,"r");

		//loop through each row of the csv and store them in the array

		while(!feof($file_new))
		{

				$products[$i] = fgetcsv($file_new);
				$psku = $products[$i][$sku];
				if($bifa == 1){
					$psku = substr_replace($psku, ' ', 1, 0);
					$psku = substr_replace($psku, ' ', 5, 0);
					$psku = substr_replace($psku, ' ', 9, 0);
				}

				$pprice = $products[$i][$price];
				$product = Product::where('sku','=', $psku)->first();

			if($product){
				$i2++;
				$pp = $pprice." Lei fara TVA";
				$product->price = $pp;
				if(!empty($producator)){
					$product->producator = $producator;

				}
				if(!empty($categorie)){
					$product->category_id = $categorie;

				}
				if(!empty($subcategorie)){
					$product->subcategory_id = $subcategorie;
				}
				if(!empty($sscat)){
					$product->sscat_id = $sscat;
				}
				$product->save();
			}
		$i++;
		}
		fclose($file_new);
	    	File::delete(public_path().'/uploads/csv/'.$csv);


		$i -= 1;

		$message = $i2.' din '.$i.' produse au fost importate!';

		return Redirect::to('admin/importa')->with('message',$message);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}




	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function postCart(){
				$cart = Cart::where('user_id',Auth::user()->id)->first();

		        if(!$cart){
		            $cart =  new Cart();
		            $cart->user_id=Auth::user()->id;
		            $cart->save();
		        }

		        $cartItem  = new CartItem();
		        $cartItem->product_id= Input::get('id');
		        $cartItem->cart_id= $cart->id;
		        $cartItem->save();
			return Redirect::to('admin/products');
	}
	public function golesteCart(){
			$cart = Cart::where('user_id',Auth::user()->id)->first();
			$cart->delete();
			return Redirect::to("admin/dashboard");
	}
	public function getCart(){
			$cart = Cart::where('user_id',Auth::user()->id)->first();
			return View::make('admin.cart')->with('items',CartItem::where('cart_id', $cart->id)->get())
->with('products', Product::all())
                        ->with('categories', PCategory::all())
                        ->with('subcategories', PSubcategory::all())
                        ->with('sscats', Sscat::all())
                        ->with('imgposts', Imgpost::all())
                        ->with('producatori', Producator::all());

	}
}
