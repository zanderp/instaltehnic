<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showLogin()
    {
        // show the form
        return View::make('login');
    }

    public function doLogin()
    {
            $rules = array(
        'email'    => 'required|email', // make sure the email is an actual email
        'password' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
        return Redirect::to('login')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
    } else {

        // create our user data for the authentication
        $userdata = array(
            'email'     => Input::get('email'),
            'password'  => Input::get('password')
        );

        // attempt to do the login
        if (Auth::attempt($userdata)) {

            // validation successful!
            // redirect them to the secure section or whatever
            // return Redirect::to('secure');
            // for now we'll just echo success (even though echoing in a controller is bad)
           return  Redirect::to('admin/dashboard');

        } else {

            // validation not successful, send back to form
            return Redirect::to('login');

        }

    	}
    }

    public function doLogout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::to('login'); // redirect the user to the login screen
    }

    public function getHome(){
        $id = 1;
        return View::make('frontend.index')
                ->with('page', Page::where('id',$id)->first())
                ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('products',Product::where('featured','1')->limit(9)->get())
					->with('producatori', Producator::all())
       		 ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('slides',Slider::all());
    }

    public function search(){

		$search_val = Input::get('search_value');
		$words = explode(" ", $search_val);
		 $query = DB::table('products')->select('*');
		 if(isset($words[0])){
			 $query->where('title','LIKE','%'.$words[0].'%');
		 }
		 if(isset($words[1])){
			 $query->where('title','LIKE','%'.$words[1].'%');
		 }
		 if(isset($words[2])){
			 $query->where('title','LIKE','%'.$words[2].'%');
		 }
		 $products = $query->get();
		return View::make('frontend.search')->with('products',$products)
			 ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
       		 ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());

    }

     public function getDespre(){
        $id = 20;
        return View::make('frontend.offer')
                ->with('page', Page::where('id',$id)->first())
                ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
    }

    public function getTerms(){
        $id = 22;
        return View::make('frontend.offer')
                ->with('page', Page::where('id',$id)->first())
                ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
    }
    public function getLivrare(){
        $id = 25;
        return View::make('frontend.offer')
                ->with('page', Page::where('id',$id)->first())
                ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
    }
    public function getOferte(){
        return View::make('frontend.getoffers')
                ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('slides',Slider::all());
    }

    public function getProduse(){
        $id = 21;
        return View::make('frontend.offer')
                ->with('page', Page::where('id',$id)->first())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
    }

     public function getService(){
        $id = 23;
        return View::make('frontend.service')
                ->with('page', Page::where('id',$id)->first())
                ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('images',  Imgpost::where('page_id',$id)->get())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
    }

    public function getNoutati(){
        return View::make('frontend.noutati')
                ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
    }

    public function getRent(){
        $id = 24;
        return View::make('frontend.offer')
                ->with('page', Page::where('id',$id)->first())
                ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
    }

    public function listProduse($type,$id){

    		$discount = "";

		if($type == "cat"){
			$produse = Product::where('category_id',$id)->paginate(9);
			$ctype = PCategory::find($id);
		}
		elseif($type == "subcat"){
			$produse = Product::where('subcategory_id',$id)->paginate(9);
			$ctype = PSubcategory::find($id);
		}
		else{
			$produse = Product::where('sscat_id',$id)->paginate(9);
			$ctype = Sscat::find($id);
		}


		return View::make('frontend.produse')->with('products', $produse)
									  ->with('categories',PCategory::all())
									  ->with('subcategories',PSubcategory::all())
									  ->with('minicategories',Sscat::orderBy('name','asc')->get())
									  ->with('ctype',$ctype)
									  ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
							  		  ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
							  		  ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get())
							  		  ->with('type',$type)
							  		  ->with('discount',$ctype->discount);
    }


    public function afisareProduse(){
		return View::make('frontend.afisareproduse')->with('products', Product::paginate(16))
											->with('settings', Settings::first())
									  		 ->with('categories',PCategory::all())
											 ->with('subcategories',PSubcategory::all())
											 ->with('minicategories',Sscat::all())
											 ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
									  		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
									  		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
    }

    public function getContact(){
        return View::make('frontend.contact')
                ->with('settings', Settings::first())
       		 ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
    }

    public function getContactWith($id){
	   $product = Product::find($id);

        return View::make('frontend.contact')
                ->with('settings', Settings::first())
       		 ->with('categories',PCategory::all())
                ->with('subcategories',PSubcategory::all())
                ->with('minicategories',Sscat::all())
                ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get())
       		 ->with('product',$product);
    }


    public function getPage($id)
	{
		return View::make('frontend.offer')
                     ->with('offer', Page::find($id))
	                ->with('pages', Page::all())
				 ->with('evs', Events::all() )
				 ->with('galleries', Gallery::all())
	                ->with('events', Offer::where('featured','yes')->get())
	                ->with('menus', Menu::all())
	                ->with('pages_left',Nav::where('type','left')->get())
	                ->with('pages_right',Nav::where('type','right')->get())
				 ->with('images',  Imgpost::where('page_id',$id)->get())
				 ->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
  		 		 ->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       			 ->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
	}
    public function getGallery($id)
	{
		return View::make('frontend.gallery')
                     ->with('gallery', Gallery::find($id))
	                ->with('pages', Page::all())
				 ->with('evs', Events::all())
				 ->with('galleries', Gallery::all())
	                ->with('events', Offer::where('featured','yes')->get())
	                ->with('menus', Menu::all())
	                ->with('pages_left',Nav::where('type','left')->get())
	                ->with('pages_right',Nav::where('type','right')->get())
				 ->with('images',  Imgpost::where('gallery_id',$id)->get());
	}


	public function getProdus($id){

		$product = Product::find($id);

		/*
		if($product->category_id > 0)
			$products = Product::where('category_id',$product->category_id)->where('title','<>',$product->title)->take(4)->get();
		elseif($product->subcategory_id > 0)
			$products = Product::where('subcategory_id',$product->subcategory_id)->where('title','<>',$product->title)->take(4)->get();
		elseif($product->sscat_id > 0)
			$products = Product::where('sscat_id',$product->sscat_id)->where('title','<>',$product->title)->take(4)->get();
		else
			$products = Product::orderByRaw("RAND()")->where('title','<>',$product->title)->take(4)->get();

		$count = $products->count();
		$limit = 4 - 2;

		if($limit > 0)
		*/
			$products = Product::orderByRaw("RAND()")->where('title','<>',$product->title)->take(8)->get();

		return View::make('frontend.product')
				->with('product', Product::find($id))
					->with('producatori', Producator::all())
				->with('categories',PCategory::all())
				->with('subcategories',PSubcategory::all())
				->with('minicategories',Sscat::all())
				->with('products',$products)
				->with('produse_adaugate',Product::orderBy('created_at','desc')->limit(10)->get())
       		 	->with('produse_vizualizate',Product::orderBy('views','desc')->limit(10)->get())
       		 	->with('postari_blog',Post::orderBy('created_at','desc')->limit(10)->get());
	}

}
