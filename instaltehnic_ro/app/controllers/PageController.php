<?php

class PageController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$pages = Page::paginate(10);
		return View::make('admin.pagelist')->with('pages', $pages)
			->with('categories', Page::all());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.pagecreate')
					->with('categories', Page::all());
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Page::$rules);
		if($validator->passes()){
				$page = new Page;
				$page->title = Input::get('title');
				$page->content = Input::get('content');
				$page->parent_id = Input::get('parent_id');
				$folders = array(
				                    'original' => '735-425',
				                    'small'   => '268-140',
				                    'medium'  => '460-320',
				                    'large'   => '735-425',
				            );

				$image = Input::file('image');
				if (isset($image)){
				foreach($folders as $key => $data) {
				            $sizes = explode('-', $data);
				            $filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
				       	 	$save_img = $this->makeImage($filename, 'img/pages/' . $key .'/', $sizes[0], $sizes[1], $image);
							$page->image = $filename;
				           }
					   }
				$page->save();
				return Redirect::to("admin/pages")
								->with('message',"The page: <b> {$page->title} </b>,  was created.");
			}
			return Redirect::to('admin/pages/new')
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('admin.pageshow')
		            ->with('page', Page::find($id))
					->with('seo', Seo::where('page_id',$id)->first())
					->with('categories', Page::all())
					->with('imgpages', Imgpost::all());
	} 


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$validator = Validator::make(Input::all(), Page::$rulesupdate);
		if($validator->passes()){
            	$page = Page::find($id);
				$page->title = Input::get('title');
				$page->content = Input::get('content');
				$page->parent_id = Input::get('parent_id');
				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '735-425',
				            );

				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$page->image = $filename;
					foreach($folders as $key => $data) {
						$sizes = explode('-', $data);
				    	$this->makeImage($filename, 'img/pages/' . $key .'/', $sizes[0], $sizes[1], $image);
					}
				}
				$page->save();
				return Redirect::to("admin/pages")
								->with('message',"The page: <b> {$page->title} </b>,  was updated.");
			}
			return Redirect::to('admin/pages/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
            if($id <= 9){
                return Redirect::to('admin/pages')
		                ->with('messagedenger',"This page is part of the main cms and it cannot be deleted.");
            }
            else {
                
		$page = Page::find($id);

		        if($page){
		            $page->delete();
		            return Redirect::to('admin/pages')
		                ->with('message',"The page: <b>$page->title</b> was deleted");
		        }
		        return Redirect::to('admin/pages')
		            ->with('messagedenger','Something went wrong');
            }
	}
	private function makeImage($a, $b ,$c, $d, $e){
	            $image = $e;
	            $path = public_path($b . $a);
				if($b =="img/pages/original/"){
					Image::make($image->getRealPath())->save($path);
				}else{
					Image::make($image->getRealPath())->resize($c, $d)->save($path);
				}
	            
	    }


}
