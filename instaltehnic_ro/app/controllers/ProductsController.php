<?php

class ProductsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	 public function search()
 	{
 		$search = Input::get('search');
 		return View::make('admin.productlist')->with('products',Product::Where('title','Like','%'.$search.'%')->paginate(20))
 										->with('search','')
										->with('producatori', Producator::all());
 	}

 	public function getProducatori(){
 		return View::make('admin.producatori_list')->with('producatori',Producator::all());

 	}

 	public function editProducator($id){
 		$producator = Producator::find($id);

 		return View::make('admin.producatori_show')->with('producator',$producator);
 	}

 	public function updateProducator($id){
 		$producator = Producator::find($id);


 		$producator->discount = Input::get('discount');
		$folders = array(
		                'original' =>'735-425',
		                'small'   => '268-140',
		                'medium'  => '460-320',
		                'large'   => '735-425',
		            );
		$image = Input::file('image');
		if(isset($image)){
			$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
			$producator->image = $filename;
			foreach($folders as $key => $data) {
				$sizes = explode('-', $data);
		    	$this->makeImage2($filename, 'img/logos/' . $key .'/', $sizes[0], $sizes[1], $image);
			}
		}
 		$producator->save();

 		return Redirect::to("admin/producatori")
								->with('message',"Producatorul a fost modificat.");
 	}

	public function get()
	{
		return View::make('admin.productlist')
			->with('products', Product::paginate(10))
			->with('producatori', Producator::all());
	}
	public function newProduct(){
		return View::make('admin.newproduct')
			->with('categories', PCategory::all())
			->with('subcategories', PSubcategory::all())
			->with('sscats', Sscat::all())
			->with('producatori', Producator::all());
	}
	public function saveProduct(){
		$validator = Validator::make(Input::all(), Product::$rules);
		if($validator->passes()){
				$product = new Product;
				$product->title = Input::get('title');
				$product->content = Input::get('content');
				$product->details = Input::get('details');
				$product->price = Input::get('price');
				$product->featured = 0;
				$product->category_id = Input::get('category_id');
				$product->sscat_id = Input::get('sscat_id');
				$product->subcategory_id = Input::get('subcategory_id');
				$product->producator = Input::get('producator_id');
				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '735-425',
				            );

				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$product->image = $filename;
					foreach($folders as $key => $data) {
						$sizes = explode('-', $data);
				    	$this->makeImage($filename, 'img/products/' . $key .'/', $sizes[0], $sizes[1], $image);
					}
				}
				$product->save();
				return Redirect::to("admin/products")
								->with('message',"Produs adaugat.");
			}
			return Redirect::to('admin/products/new')
			           ->withErrors($validator)
			           ->withInput();
	}

	public function editProduct($id){
		return View::make('admin.editproduct')
			->with('post', Product::find($id))
			->with('categories', PCategory::all())
			->with('subcategories', PSubcategory::all())
			->with('sscats', Sscat::all())
			->with('imgposts', Imgpost::all())
			->with('producatori', Producator::all());
	}
	public function updateProduct($id){
		$validator = Validator::make(Input::all(), Product::$rules);
		if($validator->passes()){
				$product = Product::find($id);
				$product->title = Input::get('title');
				$product->content = Input::get('content');
				$product->details = Input::get('details');
				$product->price = Input::get('price');
				$product->featured = 0;
				$product->category_id = Input::get('category_id');
				$product->sscat_id = Input::get('sscat_id');
				$product->subcategory_id = Input::get('subcategory_id');
				$product->producator = Input::get('producator_id');
				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '735-425',
				            );

				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$product->image = $filename;
					foreach($folders as $key => $data) {
						$sizes = explode('-', $data);
				    	$this->makeImage($filename, 'img/products/' . $key .'/', $sizes[0], $sizes[1], $image);
					}
				}
				$product->save();
				return Redirect::to("admin/products")
								->with('message',"Produs modificat.");
			}
			return Redirect::to('admin/products/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
	}
	public function editstatus($id)
	{
   		  $product = Product::find($id);
            $product->featured = Input::get('status');
            $product->save();

            return Redirect::to("admin/products")->with('message',"Status schimbat.");
    }
	public function destroyProduct($id){
			$product = Product::find($id);

		        if($product){
		            $product->delete();
		            return Redirect::to('admin/products')
		                ->with('message',"Produsul a fost stears.");
		        }
		        return Redirect::to('admin/products')
		            ->with('messagedenger','Ceva nu a functionat corect.');
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	public function getPcats(){
		return View::make('admin.pcatlist')
			->with('categories', PCategory::paginate(10));
	}

		public function newPcats(){
		return View::make('admin.pcatnew');
	}

		public function savePcats()
		{
			$validator = Validator::make(Input::all(), PCategory::$rules);
		if($validator->passes()){
				$offer = new PCategory;
				$offer->name = Input::get('name');
				$offer->discount = Input::get('discount');
				$offer->save();
				return Redirect::to("admin/pcategories")
								->with('message',"Categorie adaugata.");
			}
			return Redirect::to('admin/pcategories/new')
			           ->withErrors($validator)
			           ->withInput();
		}

		public function editPcats($id)
		{

		return View::make('admin.pcatedit')
		->with('category', PCategory::find($id));

		}

		public function updatePcats($id)
		{
			$validator = Validator::make(Input::all(), PCategory::$rulesupdate);
		if($validator->passes()){
            	$offer = PCategory::find($id);
				$offer->name = Input::get('name');
				$offer->discount = Input::get('discount');
				$offer->save();
				return Redirect::to("admin/pcategories")
								->with('message',"Oferta a fost actualizata.");
			}
			return Redirect::to('admin/pcategories/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
		}

		public function destroyPcats($id)
		{
				$offer = PCategory::find($id);

		        if($offer){
		            $offer->delete();
		            return Redirect::to('admin/pcategories')
		                ->with('message',"Categoria a fost stearsa.");
		        }
		        return Redirect::to('admin/pcategories')
		            ->with('messagedenger','Ceva nu a functionat corect.');

		}

	/////////////////////////////////////////////////////////////////////////////////////////Subcategory functions

			public function getPscat(){
		return View::make('admin.pscatlist')
			->with('subcategories', PSubcategory::paginate(10))
			->with('categories', PCategory::all());
	}

		public function newPscat(){
		return View::make('admin.pscatn')
		->with('categories', PCategory::all());
	}
		public function savePscat()
		{
			$validator = Validator::make(Input::all(), PSubcategory::$rules);
		if($validator->passes()){
				$offer = new PSubcategory;
				$offer->name = Input::get('name');
				$offer->category_id = Input::get('cat_id');
				$offer->discount = Input::get('discount');
				$offer->save();
				return Redirect::to("admin/psubcategories")
								->with('message',"Subcategorie adaugata.");
			}
			return Redirect::to('admin/psubcategories/new')
			           ->withErrors($validator)
			           ->withInput();
		}

		public function editPscat($id)
		{
			return View::make('admin.pscatedit')
		->with('subcategory', PSubcategory::find($id))
		->with('category', PCategory::all());
		}

		public function updatePscat($id)
		{
				$validator = Validator::make(Input::all(), PSubcategory::$rulesupdate);
		if($validator->passes()){
            	$offer = PSubcategory::find($id);
				$offer->name = Input::get('name');
				$offer->category_id= Input::get('cat_id');
				$offer->discount = Input::get('discount');
				$offer->save();
				return Redirect::to("admin/psubcategories")
								->with('message',"Subcategoria a fost actualizata.");
			}
			return Redirect::to('admin/psubcategories/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
		}

		public function destroyPscat($id)
		{
				$offer = PSubcategory::find($id);

		        if($offer){
		            $offer->delete();
		            return Redirect::to('admin/psubcategories')
		                ->with('message',"Subcategoria a fost stearsa.");
		        }
		        return Redirect::to('admin/psubcategories')
		            ->with('messagedenger','Ceva nu a functionat corect.');

		}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////


	public function getSscat(){
		return View::make('admin.sscatlist')
			->with('subcategories', Sscat::paginate(10))
			->with('categories', PSubcategory::all());
	}

		public function newSscat(){
		return View::make('admin.sscatnew')
		->with('categories', PSubcategory::all());
	}
		public function saveSscat()
		{
			$validator = Validator::make(Input::all(), Sscat::$rules);
			if($validator->passes()){
				$offer = new Sscat;
				$offer->name = Input::get('name');
				$offer->subcategory_id = Input::get('cat_id');
				$offer->discount = Input::get('discount');
				$offer->save();
				return Redirect::to("admin/sscat")
								->with('message',"Subcategorie adaugata.");
			}
			return Redirect::to('admin/sscat/new')
			           ->withErrors($validator)
			           ->withInput();
		}

		public function editSscat($id)
		{
			return View::make('admin.sscatedit')
			->with('subcategory', Sscat::find($id))
			->with('category', PSubcategory::all());
		}

		public function updateSscat($id)
		{
				$validator = Validator::make(Input::all(), Sscat::$rulesupdate);
		if($validator->passes()){
        			$offer = Sscat::find($id);
				$offer->name = Input::get('name');
				$offer->subcategory_id= Input::get('cat_id');
				$offer->discount = Input::get('discount');
				$offer->save();
				return Redirect::to("admin/sscat")
								->with('message',"Subcategoria a fost actualizata.");
			}
			return Redirect::to('admin/sscat/update/'.$id)
			           ->withErrors($validator)
			           ->withInput();
		}

		public function destroySscat($id)
		{
				$offer = Sscat::find($id);

		        if($offer){
		            $offer->delete();
		            return Redirect::to('admin/sscat')
		                ->with('message',"Subcategoria a fost stearsa.");
		        }
		        return Redirect::to('admin/sscat')
		            ->with('messagedenger','Ceva nu a functionat corect.');

		}




	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function getWeb(){
			set_time_limit(0);
			$html = new Htmldom('http://www.bosch-professional.com/ro/ro/professional-power-tools-131398.html');
			if (ob_get_level() == 0) ob_start();
			$res = $html->find('div[class=floatBox]');
				foreach($res as $r){
					$res2  = $r->find('div[class=h3]');
					foreach($res2 as $r2){
						$res3 = $r2->find('a');
						foreach($res3 as $r3){
							$link = $r3->href;
							$html = new Htmldom($link);
							$res = $html->find('div[class=floatBox]');
								foreach($res as $r){
									$res2  = $r->find('div[class=h3]');
									foreach($res2 as $r2){
										$res3 = $r2->find('a');
										foreach($res3 as $r3){
											$link = $r3->href;
											
											$html = new Htmldom($link);
											$res = $html->find('div[class=pDBoxLContent]');
												foreach($res as $r){
													$res2  = $r->find('div[class=h3]');
													foreach($res2 as $r2){
														$res3 = $r2->find('a');
														foreach($res3 as $r3){
															$link = $r3->href;
															
															$html = new Htmldom($link);
															$res = $html->find('div[class=item]');
															foreach($res as $r){
																$res2 = $r->find('a');
																foreach($res2 as $r2){
																	$link = $r2->href;
																	
																	$product = new Product;
																	$html = new Htmldom($link);
																	$ssk = $html->find('meta[name=WT.pn_sku]');
																	foreach($ssk as $s){
																		$sku = $s->content;
																		$ss = Product::where('sku',$sku)->get();
																		if($ss->first()){
																			continue;
																		}
																		$product->sku = $sku;
																	}
																	$res = $html->find('div[class=prodImgStage]');
																	foreach($res as $r){
																		$img = $r->find('img');
																		foreach($img as $i){
																			$imagelink = $i->src;
																			$folders = array(
																			                'original' =>'735-425',
																			                'small'   => '268-140',
																			                'medium'  => '460-320',
																			                'large'   => '735-425',
																			            );

																			$image = file_get_contents($imagelink);
																			if(isset($image)){
																				$filename = date('Y-m-d-H-i-s') . "-" .basename($imagelink);
																				$product->image = $filename;
																				foreach($folders as $key => $data) {
																					$sizes = explode('-', $data);
																			    	$this->makeImage2($filename, 'img/products/' . $key .'/', $sizes[0], $sizes[1], $image);
																				}
																			}
																			
																			
																		}
																	}
																	$res2 = $html->find('div[class=detail-headlines]');	
																	foreach ($res2 as $r2){
																		$res4 = $r2->find('h1');
																		foreach($res4 as $r){
																			$title = $r->plaintext;
																			$product->title=$title;
																			
																		}
																		
																	}
																	$res4 = $html->find('section[id=be-detail-tool-features]');
																	
																	foreach($res4 as $r){
																		$content = $r->outertext;
																		$product->content = $content;
																		
																	}
																	$res3 = $html->find('section[id=be-detail-technical-data]');
																	
																	foreach($res3 as $r){
																		$detalii = $r->outertext;
																		$product->details = $detalii;
																	}
																	$product->subcategory_id = 20;
																	if($product->save()){
																		print_r("Product added to the database!</br>");
																	}
																	ob_flush();
															        flush();
															        sleep(1);	
																	
																		
																	
																}
															}
														
														}
						
													}
												}

										}
						
									}
								}
								
						}
						
					}
				}
				ob_end_flush();		
			/*$html2 = new Htmldom('http://www.makita.ro/grup-masini/26374/programul-makita-profesional.html');
			if (ob_get_level() == 0) ob_start();
			$res = $html2->find('div[class=product_group_title]');
			foreach($res as $r){
				$res2 = $html2->find('a');
				foreach($res2 as $a){
					$link = $a->href;
					$link = ltrim($link, '/');
					if($link == 'http://www.makita.biz/'){
						continue 1;
					}
					$html3 = new Htmldom('http://makita.ro/'.$link);
					$res = $html3->find('div[class=product_group_title]');
					foreach($res as $r){
						$res2 = $r->find('a');
						foreach($res2 as $a){
							$link = $a->href;
							$link = ltrim($link, '/');
							if($link == 'http://www.makita.biz/'){
								continue 1;
							}
							$html4 = new Htmldom('http://makita.ro/'.$link);
							$res = $html4->find('div[class=product_item_title]');
							$ca = $html4->find('div[class=current_group_title]');
							foreach($ca as $t){
								$cat = $a->plaintext;
								$category = $this->normalizeChars($cat);
							}
							if(empty($res)){
								$res = $html4->find('div[class=product_group_title]');
								
								foreach($res as $r){
									$res2 = $r->find('a');
									foreach($res2 as $a){
										$link = $a->href;
										$link = ltrim($link, '/');
										if($link == 'http://www.makita.biz/'){
											continue 1;
										}
										$html5 = new Htmldom('http://makita.ro/'.$link);
										$res = $html5->find('div[class=product_item_title]');
										foreach($ca as $t){
											$cat = $a->plaintext;
											$category = $this->normalizeChars($cat);
										}
										if(empty($res)){
											$res = $html5->find('div[class=product_group_title]');
											foreach($res as $r){
												$res2 = $r->find('a');
												foreach($res2 as $a){
													$link = $a->href;
													$link = ltrim($link, '/');
													if($link == 'http://www.makita.biz/'){
														continue 1;
													}
													$html6 = new Htmldom('http://makita.ro/'.$link);
													$res = $html6->find('div[class=product_item_title]');
													foreach($ca as $t){
														$cat = $a->plaintext;
														$category = $this->normalizeChars($cat);
													}
														foreach($res as $r){
															$res2 = $r->find('a');
															foreach($res2 as $a){
																$link = $a->href;
																$link = ltrim($link, '/');
																if($link == 'http://www.makita.biz/'){
																	continue 1;
																}
																print_r($link."</br>");
																$htmlf = new Htmldom('http://makita.ro/'.$link);
																$product = new Product;
																$t1 = $htmlf->find('div[class=product_item_info_content_title]');
																foreach($t1 as $t){
																	$product->title = ($t->plaintext);
																}
																$t2 = $htmlf->find('h4');

																foreach($t2 as $t){

																	$product->sku = ($t->plaintext);
																	$sku = ($t->plaintext);
																}
																$ss = Product::where('sku',$sku)->get();
																if($ss->first()){
																	continue;
																}
																$product->subcategory_id = 32;
																$t4 = $htmlf->find('div[id=tab_content_general]');
																foreach($t4 as $t){
																		$product->content = ($t->innertext);
																}
																$t5 = $htmlf->find('ul[id=tab_content_techspecs]');
																foreach($t5 as $t){
																		$product->details = ($t->innertext);
																}
																$t6 = $htmlf->find('div[class=product_item_info_image]');
																foreach($t6 as $t){
																	$tl = $t->find('a');
																	foreach($tl as $tt){
																		$folders = array(
																		                'original' =>'735-425',
																		                'small'   => '268-140',
																		                'medium'  => '460-320',
																		                'large'   => '735-425',
																		            );

																		$image = file_get_contents($tt->href);
																		if(isset($image)){
																			$filename = date('Y-m-d-H-i-s') . "-" .basename($tt->src);
																			$product->image = $filename;
																			foreach($folders as $key => $data) {
																				$sizes = explode('-', $data);
																		    	$this->makeImage2($filename, 'img/products/' . $key .'/', $sizes[0], $sizes[1], $image);
																			}
																		}


																	}

																}
																		if($product->save()){
																			print_r( "Product added to database. </br>");
																		}
																		ob_flush();
																        flush();
																        sleep(1);
											
															}
												
														
														}
													
											
												}
												
											}
										}else{
											foreach($res as $r){
												$res2 = $r->find('a');
												foreach($res2 as $a){
													$link = $a->href;
													$link = ltrim($link, '/');
													if($link == 'http://www.makita.biz/'){
														continue 1;
													}
													print_r($link."</br>");
													$htmlf = new Htmldom('http://makita.ro/'.$link);
													$product = new Product;
													$t1 = $htmlf->find('div[class=product_item_info_content_title]');
													foreach($t1 as $t){
														$product->title = ($t->plaintext);
													}
													$t2 = $htmlf->find('h4');

													foreach($t2 as $t){

														$product->sku = ($t->plaintext);
														$sku = ($t->plaintext);
													}
													$ss = Product::where('sku',$sku)->get();
													if($ss->first()){
														continue;
													}
													
													$product->subcategory_id = 32;
													$t4 = $htmlf->find('div[id=tab_content_general]');
													foreach($t4 as $t){
															$product->content = ($t->innertext);
													}
													$t5 = $htmlf->find('ul[id=tab_content_techspecs]');
													foreach($t5 as $t){
															$product->details = ($t->innertext);
													}
													$t6 = $htmlf->find('div[class=product_item_info_image]');
													foreach($t6 as $t){
														$tl = $t->find('a');
														foreach($tl as $tt){
															$folders = array(
															                'original' =>'735-425',
															                'small'   => '268-140',
															                'medium'  => '460-320',
															                'large'   => '735-425',
															            );

															$image = file_get_contents($tt->href);
															if(isset($image)){
																$filename = date('Y-m-d-H-i-s') . "-" .basename($tt->src);
																$product->image = $filename;
																foreach($folders as $key => $data) {
																	$sizes = explode('-', $data);
															    	$this->makeImage2($filename, 'img/products/' . $key .'/', $sizes[0], $sizes[1], $image);
																}
															}


														}

													}
															if($product->save()){
																print_r( "Product added to database. </br>");
															}
															ob_flush();
													        flush();
													        sleep(1);
															
											
												}
												
											}
										}
									}
								}
							}else{
											foreach($res as $r){
												$res2 = $r->find('a');
												foreach($res2 as $a){
													$link = $a->href;
													$link = ltrim($link, '/');
													if($link == 'http://www.makita.biz/'){
														continue 1;
													}
													print_r($link."</br>");
													$htmlf = new Htmldom('http://makita.ro/'.$link);
													$product = new Product;
													$t1 = $htmlf->find('div[class=product_item_info_content_title]');
													foreach($t1 as $t){
														$product->title = ($t->plaintext);
													}
													$t2 = $htmlf->find('h4');

													foreach($t2 as $t){

														$product->sku = ($t->plaintext);
														$sku = ($t->plaintext);
													}
													$ss = Product::where('sku',$sku)->get();
													if($ss->first()){
														continue;
													}
													
													$product->subcategory_id = 32;
													$t4 = $htmlf->find('div[id=tab_content_general]');
													foreach($t4 as $t){
															$product->content = ($t->innertext);
													}
													$t5 = $htmlf->find('ul[id=tab_content_techspecs]');
													foreach($t5 as $t){
															$product->details = ($t->innertext);
													}
													$t6 = $htmlf->find('div[class=product_item_info_image]');
													foreach($t6 as $t){
														$tl = $t->find('a');
														foreach($tl as $tt){
															$folders = array(
															                'original' =>'735-425',
															                'small'   => '268-140',
															                'medium'  => '460-320',
															                'large'   => '735-425',
															            );

															$image = file_get_contents($tt->href);
															if(isset($image)){
																$filename = date('Y-m-d-H-i-s') . "-" .basename($tt->src);
																$product->image = $filename;
																foreach($folders as $key => $data) {
																	$sizes = explode('-', $data);
															    	$this->makeImage2($filename, 'img/products/' . $key .'/', $sizes[0], $sizes[1], $image);
																}
															}


														}

													}
															if($product->save()){
																print_r( "Product added to database. </br>");
															}
															ob_flush();
													        flush();
													        sleep(1);
														
											
												}
												
											}
										}
							
										
						}
					}
				}
				
			}*/
			/*$html = new Htmldom('http://catalog.proenerg.com.ro/catalog-de-produse--c106');
			if (ob_get_level() == 0) ob_start();
			$res = $html->find('li[class=sfHoverForce]');
			foreach($res as $r){
				$res2 = $r->find('ul li ul li');
				foreach($res2 as $e){
					$res3 = $e->find('a');
					foreach($res3 as $a){
						$link = $a->href;
						$cat = $a->plaintext;
						$category = $this->normalizeChars($cat);
						$html2 = new htmldom($link);
						$rez = $html2->find('div[class=product-image-container]');
						foreach($rez as $prod){
								$href = $prod->find('a');
								foreach($href as $h){
										$prodl = $h->href;
										$html3 = new Htmldom($prodl);
										$product = new Product;
										$t1 = $html3->find('h1[itemprop=name]');
										foreach($t1 as $t){
											$product->title = ($t->plaintext);
										}
										$t2 = $html3->find('span[itemprop=sku]');

										foreach($t2 as $t){

											$product->sku = ($t->plaintext);
											$sku = ($t->plaintext);
										}
										$ss = Product::where('sku',$sku)->get();
										if($ss->first()){
											continue;
										}

										$categories = PCategory::all();
										foreach($categories as $cc){
											$ccv = strtolower($category);
											$cmp = strtolower($cc->name);
											if($ccv == $cmp){
												$product->category_id = $cc->id;
											}
										}
										$subcategories = PSubcategory::all();
										foreach($subcategories as $sc){
											$ccv = strtolower($category);
											$cmp = strtolower($sc->name);
											if($ccv == $cmp){
												$product->subcategory_id = $sc->id;
											}
										}
										$minicategories = Sscat::all();
										foreach($minicategories as $sc){

											$ccv = strtolower($category);
											$cmp = strtolower($sc->name);
											if($ccv == $cmp){
												$product->sscat_id = $sc->id;
											}
										}

										$t3 = $html3->find('span[itemprop=price]');
										foreach($t3 as $t){
											$product->price = ($t->plaintext);
										}
										$t4 = $html3->find('div[id=idTab1]');
										foreach($t4 as $t){
												$product->content = ($t->innertext);
										}
										$t5 = $html3->find('ul[id=idTab2]');
										foreach($t5 as $t){
												$product->details = ($t->innertext);
										}
										$t6 = $html3->find('span[id=view_full_size]');
										foreach($t6 as $t){
											$tl = $t->find('img');
											foreach($tl as $tt){
												$folders = array(
												                'original' =>'735-425',
												                'small'   => '268-140',
												                'medium'  => '460-320',
												                'large'   => '735-425',
												            );

												$image = file_get_contents($tt->src);
												if(isset($image)){
													$filename = date('Y-m-d-H-i-s') . "-" .basename($tt->src);
													$product->image = $filename;
													foreach($folders as $key => $data) {
														$sizes = explode('-', $data);
												    	$this->makeImage2($filename, 'img/products/' . $key .'/', $sizes[0], $sizes[1], $image);
													}
												}


											}

										}
												if($product->save()){
													print_r( "Product added to database. </br>");
												}
												ob_flush();
										        flush();
										        sleep(1);
								}
						}
					}
				}
			}
			ob_end_flush();*/
	}
	public function newProducator(){
		return View::make('admin.producatori_new');
	}
	public function storeProducator(){
		$validator = Validator::make(Input::all(), Producator::$rules);
		if($validator->passes()){
				$product = new Producator;
				$product->title = Input::get('title');

				$folders = array(
				                'original' =>'735-425',
				                'small'   => '268-140',
				                'medium'  => '460-320',
				                'large'   => '735-425',
				            );

				$image = Input::file('image');
				if(isset($image)){
					$filename = date('Y-m-d-H-i-s') . "-" . $image->getClientOriginalName();
					$product->image = $filename;
					foreach($folders as $key => $data) {
						$sizes = explode('-', $data);
				    	$this->makeImage2($filename, 'img/logos/' . $key .'/', $sizes[0], $sizes[1], $image);
					}
				}
				$product->save();
				return Redirect::to("admin/producatori")
								->with('message',"Produs adaugat.");
			}
			return Redirect::to('admin/producatori/new')
			           ->withErrors($validator)
			           ->withInput();
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		private function makeImage($a, $b ,$c, $d, $e){
	            $image = $e;
	            $path = public_path($b . $a);
				if($b =="img/products/original/"){
					Image::make($image->getRealPath())->save($path);
				}else{
					Image::make($image->getRealPath())->resize($c, $d)->save($path);
				}

	}
	private function makeImage2($a, $b ,$c, $d, $e){
            $image = $e;
            $path = public_path($b . $a);
			if($b =="img/products/original/"){
				Image::make($image)->save($path);
			}elseif($b =="img/logos/original/"){
				Image::make($image)->save($path);
			}else{
				Image::make($image)->resize($c, $d)->save($path);
			}

}
	private function normalizeChars($s) {
    $replace = array(
        'ъ'=>'-', 'Ь'=>'-', 'Ъ'=>'-', 'ь'=>'-','Ă'=>'A', 'Ą'=>'A', 'À'=>'A', 'Ã'=>'A', 'Á'=>'A', 'Æ'=>'A', 'Â'=>'A', 'Å'=>'A', 'Ä'=>'Ae','Þ'=>'B','Ć'=>'C', 'ץ'=>'C', 'Ç'=>'C','È'=>'E', 'Ę'=>'E', 'É'=>'E', 'Ë'=>'E', 'Ê'=>'E','Ğ'=>'G','İ'=>'I', 'Ï'=>'I', 'Î'=>'I', 'Í'=>'I', 'Ì'=>'I','Ł'=>'L','Ñ'=>'N', 'Ń'=>'N','Ø'=>'O','Ş'=>'S', 'Ś'=>'S', 'Ș'=>'S', 'Š'=>'S','Ț'=>'T','Ù'=>'U', 'Û'=>'U', 'Ú'=>'U', 'Ü'=>'Ue','Ý'=>'Y','Ź'=>'Z', 'Ž'=>'Z', 'Ż'=>'Z','â'=>'a', 'ǎ'=>'a', 'ą'=>'a', 'á'=>'a', 'ă'=>'a', 'ã'=>'a', 'Ǎ'=>'a', 'а'=>'a', 'А'=>'a', 'å'=>'a', 'à'=>'a', 'א'=>'a', 'Ǻ'=>'a', 'Ā'=>'a', 'ǻ'=>'a', 'ā'=>'a', 'ä'=>'ae', 'æ'=>'ae', 'Ǽ'=>'ae', 'ǽ'=>'ae','б'=>'b', 'ב'=>'b', 'Б'=>'b', 'þ'=>'b',
        'ĉ'=>'c', 'Ĉ'=>'c', 'Ċ'=>'c', 'ć'=>'c', 'ç'=>'c', 'ц'=>'c', 'צ'=>'c', 'ċ'=>'c', 'Ц'=>'c', 'Č'=>'c', 'č'=>'c', 'Ч'=>'ch', 'ч'=>'ch','ד'=>'d', 'ď'=>'d', 'Đ'=>'d', 'Ď'=>'d', 'đ'=>'d', 'д'=>'d', 'Д'=>'D', 'ð'=>'d',
        'є'=>'e', 'ע'=>'e', 'е'=>'e', 'Е'=>'e', 'Ə'=>'e', 'ę'=>'e', 'ĕ'=>'e', 'ē'=>'e', 'Ē'=>'e', 'Ė'=>'e', 'ė'=>'e', 'ě'=>'e', 'Ě'=>'e', 'Є'=>'e', 'Ĕ'=>'e', 'ê'=>'e', 'ə'=>'e', 'è'=>'e', 'ë'=>'e', 'é'=>'e',
        'ф'=>'f', 'ƒ'=>'f', 'Ф'=>'f',
        'ġ'=>'g', 'Ģ'=>'g', 'Ġ'=>'g', 'Ĝ'=>'g', 'Г'=>'g', 'г'=>'g', 'ĝ'=>'g', 'ğ'=>'g', 'ג'=>'g', 'Ґ'=>'g', 'ґ'=>'g', 'ģ'=>'g',
        'ח'=>'h', 'ħ'=>'h', 'Х'=>'h', 'Ħ'=>'h', 'Ĥ'=>'h', 'ĥ'=>'h', 'х'=>'h', 'ה'=>'h',
        'î'=>'i', 'ï'=>'i', 'í'=>'i', 'ì'=>'i', 'į'=>'i', 'ĭ'=>'i', 'ı'=>'i', 'Ĭ'=>'i', 'И'=>'i', 'ĩ'=>'i', 'ǐ'=>'i', 'Ĩ'=>'i', 'Ǐ'=>'i', 'и'=>'i', 'Į'=>'i', 'י'=>'i', 'Ї'=>'i', 'Ī'=>'i', 'І'=>'i', 'ї'=>'i', 'і'=>'i', 'ī'=>'i', 'ĳ'=>'ij', 'Ĳ'=>'ij',
        'й'=>'j', 'Й'=>'j', 'Ĵ'=>'j', 'ĵ'=>'j', 'я'=>'ja', 'Я'=>'ja', 'Э'=>'je', 'э'=>'je', 'ё'=>'jo', 'Ё'=>'jo', 'ю'=>'ju', 'Ю'=>'ju',
        'ĸ'=>'k', 'כ'=>'k', 'Ķ'=>'k', 'К'=>'k', 'к'=>'k', 'ķ'=>'k', 'ך'=>'k',
        'Ŀ'=>'l', 'ŀ'=>'l', 'Л'=>'l', 'ł'=>'l', 'ļ'=>'l', 'ĺ'=>'l', 'Ĺ'=>'l', 'Ļ'=>'l', 'л'=>'l', 'Ľ'=>'l', 'ľ'=>'l', 'ל'=>'l',
        'מ'=>'m', 'М'=>'m', 'ם'=>'m', 'м'=>'m',
        'ñ'=>'n', 'н'=>'n', 'Ņ'=>'n', 'ן'=>'n', 'ŋ'=>'n', 'נ'=>'n', 'Н'=>'n', 'ń'=>'n', 'Ŋ'=>'n', 'ņ'=>'n', 'ŉ'=>'n', 'Ň'=>'n', 'ň'=>'n',
        'о'=>'o', 'О'=>'o', 'ő'=>'o', 'õ'=>'o', 'ô'=>'o', 'Ő'=>'o', 'ŏ'=>'o', 'Ŏ'=>'o', 'Ō'=>'o', 'ō'=>'o', 'ø'=>'o', 'ǿ'=>'o', 'ǒ'=>'o', 'ò'=>'o', 'Ǿ'=>'o', 'Ǒ'=>'o', 'ơ'=>'o', 'ó'=>'o', 'Ơ'=>'o', 'œ'=>'oe', 'Œ'=>'oe', 'ö'=>'oe','פ'=>'p', 'ף'=>'p', 'п'=>'p', 'П'=>'p',
        'ק'=>'q',
        'ŕ'=>'r', 'ř'=>'r', 'Ř'=>'r', 'ŗ'=>'r', 'Ŗ'=>'r', 'ר'=>'r', 'Ŕ'=>'r', 'Р'=>'r', 'р'=>'r',
        'ș'=>'s', 'с'=>'s', 'Ŝ'=>'s', 'š'=>'s', 'ś'=>'s', 'ס'=>'s', 'ş'=>'s', 'С'=>'s', 'ŝ'=>'s', 'Щ'=>'sch', 'щ'=>'sch', 'ш'=>'sh', 'Ш'=>'sh', 'ß'=>'ss',
        'т'=>'t', 'ט'=>'t', 'ŧ'=>'t', 'ת'=>'t', 'ť'=>'t', 'ţ'=>'t', 'Ţ'=>'t', 'Т'=>'t', 'ț'=>'t', 'Ŧ'=>'t', 'Ť'=>'t', '™'=>'tm',
        'ū'=>'u', 'у'=>'u', 'Ũ'=>'u', 'ũ'=>'u', 'Ư'=>'u', 'ư'=>'u', 'Ū'=>'u', 'Ǔ'=>'u', 'ų'=>'u', 'Ų'=>'u', 'ŭ'=>'u', 'Ŭ'=>'u', 'Ů'=>'u', 'ů'=>'u', 'ű'=>'u', 'Ű'=>'u', 'Ǖ'=>'u', 'ǔ'=>'u', 'Ǜ'=>'u', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'У'=>'u', 'ǚ'=>'u', 'ǜ'=>'u', 'Ǚ'=>'u', 'Ǘ'=>'u', 'ǖ'=>'u', 'ǘ'=>'u', 'ü'=>'ue',
        'в'=>'v', 'ו'=>'v', 'В'=>'v',
        'ש'=>'w', 'ŵ'=>'w', 'Ŵ'=>'w',
        'ы'=>'y', 'ŷ'=>'y', 'ý'=>'y', 'ÿ'=>'y', 'Ÿ'=>'y', 'Ŷ'=>'y',
        'Ы'=>'y', 'ž'=>'z', 'З'=>'z', 'з'=>'z', 'ź'=>'z', 'ז'=>'z', 'ż'=>'z', 'ſ'=>'z', 'Ж'=>'zh', 'ж'=>'zh'
    );
    return strtr($s, $replace);
}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
}
