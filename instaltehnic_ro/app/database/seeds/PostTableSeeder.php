<?php

class PostTableSeeder extends Seeder
{

public function run()
{
    DB::table('posts')->delete();
    Post::create(array(
        'category_id'     => '1',
        'title' => 'Post',
        'content'    => 'PostContentHere',
    ));
}

}
