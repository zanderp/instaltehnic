<?php

class UserTableSeeder extends Seeder
{

public function run()
{
    DB::table('users')->delete();
    User::create(array(
        'name'     => 'Developer Ace COM',
        'username' => 'admin',
        'email'    => 'office@developerace.com',
        'password' => Hash::make('Testing!23'),
    ));
}

}
