<?php
class Cart extends Eloquent {

    protected $table = 'carts';
	
		public function user()
		    {
		        return $this->belongsTo('User');
		    }
 
			public function cartItems()
		    {
		        return $this->hasMany('CartItem');
		    }

}

