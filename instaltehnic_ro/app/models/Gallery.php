<?php
class Gallery extends Eloquent {

    protected $table = 'galleries';
	protected $fillable = array(
                'name',
	        'gcat_id');

	    public static $rules = array(
	        'name' => 'required|min:4'
	    );
	    public static $rulesupdate = array(
	        'name' => 'required|min:4'
	    );
	    

}
