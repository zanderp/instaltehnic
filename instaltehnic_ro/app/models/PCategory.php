<?php
class PCategory extends Eloquent {

    protected $table = 'products_categories';
	protected $fillable = array(
	        'name');

	    public static $rules = array(
	        'name' => 'required|min:3'
	    );
	    public static $rulesupdate = array(
	        'name' => 'required|min:3'
	    );
	    public function products(){
	        return $this->hasMany('Product');
	    }
	    public function subcategory(){
	        return $this->hasMany('PSubcategory');
	    }

}
