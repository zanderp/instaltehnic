<?php
class Nav extends Eloquent {

    protected $table = 'nav';
	protected $fillable = array('type',
	        'page');

	    public static $rules = array(
	        'type' => 'required',
	        'page'=>'required'
	    );
	  
	    

}
