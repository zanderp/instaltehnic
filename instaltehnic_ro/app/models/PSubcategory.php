<?php
class PSubcategory extends Eloquent {

    protected $table = 'products_subcategories';
	protected $fillable = array('category_id',
	        'name');

	    public static $rules = array(
	        'name' => 'min:2'
	        
	    );
	    public static $rulesupdate = array(
	       	'name' => 'min:2'
	    );
	    public function category(){
	        return $this->belongsTo('PCategory');
	    }

}
