<?php
class Menu extends Eloquent {

    protected $table = 'menus';
	protected $fillable = array(
                'title',
	        'content');

	    public static $rules = array(
	        'title' => 'required|min:4',
	        'content'=>'required|min:4',
			'image'=>'image|mimes:jpeg,bmp,png,gif'
	    );
	    public static $rulesupdate = array(
	        'title' => 'required|min:4',
	        'content'=>'required|min:4',
			'image'=>'image|mimes:jpeg,bmp,png,gif'
	    );
	    

}
