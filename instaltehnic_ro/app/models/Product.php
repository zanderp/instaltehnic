<?php
class Product extends Eloquent {

    protected $table = 'products';
	protected $fillable = array('category_id',
	        'subcategory_id',
	        'title');

	    public static $rules = array(
	        'title' => 'required|min:3'
	    );
	    public static $rulesupdate = array(
	        'title' => 'required|min:3'
	    );
	    public function category(){
	        return $this->belongsTo('PCategory');
	    }
	    public function subcategory(){
	        return $this->belongsTo('PSubcategory');
	    }

}
