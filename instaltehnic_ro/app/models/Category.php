<?php
class Category extends Eloquent {

    protected $table = 'categories';
    public static $rules = array(
        'name'=>'required|min:5'
    );
    public static $rulesupdate = array(
        'name' => 'required|min:5'
    );
	 public function posts()
    {
        return $this->hasMany('Post');
    }


}
