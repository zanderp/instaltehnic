<?php
class Sscat extends Eloquent {

    protected $table = 'products_minicategories';
	protected $fillable = array('subcategory_id',
	        'name');

	    public static $rules = array(
	        'name' => 'min:2'
	        
	    );
	    public static $rulesupdate = array(
	       	'name' => 'min:2'
	    );
	    public function category(){
	        return $this->belongsTo('PSubcategory');
	    }

}
