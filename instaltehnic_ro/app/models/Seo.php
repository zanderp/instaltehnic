<?php
class Seo extends Eloquent {

    protected $table = 'seos';
    public static $rules = array(
        'title'=>'required|min:5',
		'description'=>'min:10',
		'keywords'=>'min:3',
		'author'=>'min:4',
		'post_id'=>'required',
                'page_id'=>'required'
    );
    public static $rulesupdate = array(
        'title'=>'required|min:5',
		'description'=>'min:10',
		'keywords'=>'min:3',
		'author'=>'min:4',
		'post_id'=>'required',
                'page_id'=>'required'
    );
	 public function posts()
    {
        return $this->hasMany('Post');
    }


}
