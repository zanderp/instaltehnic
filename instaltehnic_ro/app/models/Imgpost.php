<?php

class Imgpost extends Eloquent {

    protected $fillable = array('page_id','post_id','offer_id','menu_id','event_id','image');

    public static $rules = array(
        'page_id' => 'integer',
	'post_id' => 'integer',
        'offer_id' => 'integer',
        'menu_id' => 'integer',
        'event_id' => 'integer',
        'file'=>'image|mimes:jpeg,jpg,bmp,png,gif|max:300000'
    );

    public function posts() {
        return $this->hasMany('Post');
    }
}
