<?php
class Slider extends Eloquent {

    protected $table = 'sliders';

	    public static $rules = array(
	     'name' => 'required',
		   'image'=>'image|mimes:jpeg,bmp,png,gif',
		   'pdf'=>'max:30000'
	    );
	    public static $rulesupdate = array(
	     'name' => 'required',
		   'image'=>'image|mimes:jpeg,bmp,png,gif',
		   'pdf'=>'max:30000'
	    );


}
