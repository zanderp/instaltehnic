<?php
class Post extends Eloquent {

    protected $table = 'posts';
	protected $fillable = array('category_id',
	        'title',
	        'content');

	    public static $rules = array(
	        'category_id' => 'required',
	        'title' => 'required|min:5',
	        'content'=>'required|min:5',
			'image'=>'image|mimes:jpeg,bmp,png,gif'
	    );
	    public static $rulesupdate = array(
	        'category_id' => 'required',
	        'title' => 'required|min:5',
	        'content'=>'required|min:5',
			'image'=>'image|mimes:jpeg,bmp,png,gif'
	    );
	    public function category(){
	        return $this->belongsTo('Category');
	    }

}
