<?php
class Page extends Eloquent {

    protected $table = 'pages';
	protected $fillable = array('parent_id',
	        'title',
	        'content');

	    public static $rules = array(
	        'parent_id' => 'required',
	        'title' => 'required|min:4',
	        'content'=>'required|min:4',
			'image'=>'image|mimes:jpeg,bmp,png,gif'
	    );
	    public static $rulesupdate = array(
	        'parent_id' => 'required',
	        'title' => 'required|min:4',
	        'content'=>'required|min:4',
			'image'=>'image|mimes:jpeg,bmp,png,gif'
	    );
	    

}
