<?php
class Settings extends Eloquent {

    protected $table = 'settings';
	protected $fillable = array('punct_lucru',
	        'telefon','mobil','email','lat','longi');

	    public static $rules = array(
                'punct_lucru' => 'required',
	       	 'telefon'=>'required',
                'mobil' => 'required',
                'email'=>'required'
	    );

            public static $rulesupdate = array(
                'punct_lucru' => 'required',
        		 'telefon'=>'required',
                'mobil'=>'required',
                'email'=>'required'
	    );



}
