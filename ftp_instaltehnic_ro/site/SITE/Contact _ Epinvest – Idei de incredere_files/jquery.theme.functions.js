// Accordion
ddaccordion.init({
	headerclass: "ask", 
	contentclass: "question", 
	revealtype: "click", 
	mouseoverdelay: 200, 
	collapseprev: true, 
	defaultexpanded: [], 
	onemustopen: true, 
	animatedefault: false,
	persiststate: false, 
	toggleclass: ["closedquestion", "openquestion"], 
	togglehtml: ["prefix", "<img src='wp-content/themes/hyperion/images/faq-closed.png' alt='' style='width:10px; height:10px; margin:3px 3px 0px 0px; float:right;' /> ", "<img src='wp-content/themes/hyperion/images/faq-open.png' alt='' style='width:10px; height:10px; margin:3px 3px 0px 0px; float:right;' /> "], 
	animatespeed: "fast", 
	oninit:function(expandedindices){ 
	},
	onopenclose:function(header, index, state, isuseractivated){ 		
	}
})



//Tooltip
jQuery( document ).ready( function()
{
	var targets = jQuery( '[class=tooltip]' ),
		target	= false,
		tooltip = false,
		title	= false;

	targets.bind( 'mouseenter', function()
	{
		target	= jQuery( this );
		tip		= target.attr( 'title' );
		tooltip	= jQuery( '<div id="tooltip"></div>' );

		if( !tip || tip == '' )
			return false;

		target.removeAttr( 'title' );
		tooltip.css( 'opacity', 0 )
			   .html( tip )
			   .appendTo( 'body' );

		var init_tooltip = function()
		{
			if( jQuery( window ).width() < tooltip.outerWidth() * 1.5 )
				tooltip.css( 'max-width', jQuery( window ).width() / 2 );
			else
				tooltip.css( 'max-width', 340 );

			var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
				pos_top	 = target.offset().top - tooltip.outerHeight() - 20;

			if( pos_left < 0 )
			{
				pos_left = target.offset().left + target.outerWidth() / 2 - 20;
				tooltip.addClass( 'left' );
			}
			else
				tooltip.removeClass( 'left' );

			if( pos_left + tooltip.outerWidth() > jQuery( window ).width() )
			{
				pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
				tooltip.addClass( 'right' );
			}
			else
				tooltip.removeClass( 'right' );

			if( pos_top < 0 )
			{
				var pos_top	 = target.offset().top + target.outerHeight();
				tooltip.addClass( 'top' );
			}
			else
				tooltip.removeClass( 'top' );

			tooltip.css( { left: pos_left, top: pos_top } )
				   .animate( { top: '+=10', opacity: 1 }, 50 );
		};

		init_tooltip();
		jQuery( window ).resize( init_tooltip );

		var remove_tooltip = function()
		{
			tooltip.animate( { top: '-=10', opacity: 0 }, 50, function()
			{
				jQuery( this ).remove();
			});

			target.attr( 'title', tip );
		};

		target.bind( 'mouseleave', remove_tooltip );
		tooltip.bind( 'click', remove_tooltip );
	});
});	



jQuery(document).ready(function() {
	//Tab Jquery
	jQuery(".tab_content").hide(); 
	jQuery("ul.tabs li:first").addClass("active").show(); 
	jQuery(".tab_content:first").show(); 
	jQuery("ul.tabs li").click(function() {
		jQuery("ul.tabs li").removeClass("active");
		jQuery(this).addClass("active"); 
		jQuery(".tab_content").hide(); 
		var activeTab = jQuery(this).find("a").attr("href"); 
		jQuery(activeTab).fadeIn(); 
		return false;
	});	
	
	
    //$('.toggle_content:first').show();
    jQuery(".toggle_title").toggle(
    	function(){
    		jQuery(this).addClass('toggle_active');
    		jQuery(this).siblings('.toggle_content').slideDown("fast");
    	},
    	function(){
    		jQuery(this).removeClass('toggle_active');
    		jQuery(this).siblings('.toggle_content').slideUp("fast");
    	}
    );
	
	//Fancybox Jquery
	jQuery(".fancybox").fancybox({
		padding: 0,
		openEffect : 'elastic',
		openSpeed  : 250,
		closeEffect : 'elastic',
		closeSpeed  : 250,
		closeClick : true,
		helpers : {
			overlay : {opacity : 0.65},
			media : {}
		}
	});	
	
	//TinyNav Jquery
    jQuery("#menu").tinyNav({
	active: 'current_page_item', // Set the "active" class for default menu
	label: '', // String: Sets the <label> text for the <select> (if not set, no label will be added)
    header: '' // String: Specify text for "header" and show header instead of the active item
	});

	// Responsive Menu (Selectbox)
	jQuery(function () {
		jQuery(".tinynav").selectbox();
	});
	
	//Social Panel
	jQuery(".trigger").click(function(){
		jQuery(".social-panel").toggle("fast");
		jQuery(this).toggleClass("active");
		return false;
	});
	
	
	//To top Jquery
	jQuery().UItoTop({ easingType: 'easeOutQuart' });					
	
});


//Detect Video
(function($){
	function detectVideo(){
		var flash_object = '<object style="z-index:0;" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="585" height="345"><param name="wmode" value="transparent" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="{path}" /><embed src="{path}" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="585" height="345" wmode="transparent"></embed></object>';
		var quicktime_object = '<object classid="clsid:02bf25d5-8c17-4b23-bc80-d3488abddc6b" codebase="http://www.apple.com/qtactivex/qtplugin.cab#version=6,0,2,0" height="345" width="585"><param name="src" value="{path}"><param name="autoplay" value="false"><param name="scale" value="tofit"><param name="type" value="video/quicktime"><embed src="{path}" scale="tofit" height="345" width="585" autoplay="false" type="video/quicktime" pluginspage="http://www.apple.com/quicktime/download/"></embed></object>';
		var toInject = "";
		
		var divWrapper = $('.pf_video_container');
		var ObjectP = divWrapper.find('a');
		
		switch(ObjectP.attr('rel')){
		  
					case 'youtube':
						movie = 'http://www.youtube.com/v/'+igrab_param('v', ObjectP.attr('href'));
						toInject = flash_object.replace(/{path}/g,movie);
					break;
					
					case 'vimeo':
						movie_id = ObjectP.attr('href');
						movie = "http://vimeo.com/moogaloop.swf?clip_id="+ movie_id.replace('http://vimeo.com/','');
					  toInject = flash_object.replace(/{path}/g,movie);
					break;
					
          case 'flash':
						movie = ObjectP.attr('href');
						toInject = flash_object.replace(/{path}/g,movie);
					break;
					
					case 'quicktime':
						movie = ObjectP.attr('href');
						toInject = quicktime_object.replace(/{path}/g,movie);
					break;
		}
		
		divWrapper.html(toInject);
		
	function igrab_param(name,url){
	  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	  var regexS = "[\\?&]"+name+"=([^&#]*)";
	  var regex = new RegExp( regexS );
	  var results = regex.exec( url );
	  if( results == null )
	    return "";
	  else
	    return results[1];
	}	
	
  }
  $(document).ready(function(){detectVideo();});
	
})(jQuery); 